import os

from flask import Flask, request, jsonify, abort, send_from_directory
from flask_login import login_required, logout_user
from sqlalchemy.exc import IntegrityError
from werkzeug.utils import redirect, secure_filename
import uuid
from orm import Users, session, Activitylog, UsersSchema, BreastPatient, BPatientSchema, ActivitySchema, \
    RheumatologyPatient, RheumatologyPatientSchema, RheumatologyFollowup, RheumatologyFollowupSchema, RAttachments, \
    RFollowAttachments, BAttachments, Rheumatologyattachmentschema, Breastattachmentschema, \
    RheumatologyFollowattachmentschema, RheumatologyPatientExamination, RheumatologyPatientExaminationSchema, \
    RheumatologyPatientInvestigation, RheumatologyPatientInvestigationSchema
import configparser

app = Flask(__name__)

UPLOAD_DIRECTORY = "./static/files"
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'mp4'}

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)

config = configparser.ConfigParser()
config.read('config.ini')

headerkey = config['SERVER']['X-AUTH-KEY']


@app.before_request
def before_request():
    # logger.info("Incoming request from IP:" + request.remote_addr + ".Route:" + request.url)
    key = request.headers.get('X-AUTH', None)
    if key is None or key != headerkey:
        # logger.info("Invalid auth header. Aborting request")
        if request.endpoint != 'uploaded_file':
            abort(403)


@app.route("/adduser", methods=['GET', 'POST'])
def adduser():
    if request.method == 'POST':
        username = request.json.get("uname", False)
        email = request.json.get("email", False)
        phone = request.json.get("phone", False)
        password = request.json.get("password", False)
        role = request.json.get("role", False)
        speciality = request.json.get("speciality", False)
        try:
            dbadd = Users(username=username, email=email, phone=phone, role=role, password=password,
                          speciality=speciality)
            session.add(dbadd)
            session.commit()
            return "Success"
        except:
            return "Fail"
    return "get request made"


@app.route("/userupdate", methods=['GET', 'POST'])
def userupdate():
    if request.method == 'POST':
        user_id = request.json.get("user_id", False)
        username = request.json.get("uname", False)
        email = request.json.get("email", False)
        phone = request.json.get("phone", False)
        password = request.json.get("password", False)
        role = request.json.get("role", False)
        speciality = request.json.get("speciality", False)
        try:
            dbedit = session.query(Users).filter_by(id=user_id).update(
                dict(username=username, email=email, phone=phone, role=role, password=password, speciality=speciality))
            session.commit()
            print(dbedit)
            # sending back updated data of the user
            userdata = session.query(Users).filter_by(id=user_id).first()
            session.commit()
            user_schema = UsersSchema()
            output = user_schema.dump(userdata)
            return jsonify({'user': output})
        except:
            return "Fail"
    return "get request made"


@app.route("/userlogin", methods=['GET', 'POST'])
def Ulogin():
    if request.method == 'POST':
        email = request.json.get("email", False)
        password = request.json.get("password", False)
        try:
            users = session.query(Users).filter_by(email=email, password=password).first()
            session.commit()
            if str(users) != "None":
                user_schema = UsersSchema()
                output = user_schema.dump(users)
                return jsonify({'user': output})
            else:
                return "Fail"
        except:
            return "Fail"
    return "make post request"


@app.route('/userlogout')
@login_required
def logout():
    logout_user()
    return "Success"


# Profile and views of user
@app.route("/userdetails", methods=['GET', 'POST'])
def Udetails():
    uid = request.args['uid']
    users = session.query(Users).filter_by(id=uid).first()
    session.commit()
    user_schema = UsersSchema()
    output = user_schema.dump(users)
    return jsonify({'user': output})


# for usermanagement
@app.route("/alluserdetails", methods=['GET', 'POST'])
def AllUdetails():
    users = session.query(Users).all()
    session.commit()
    user_schema = UsersSchema(many=True)
    output = user_schema.dump(users)
    return jsonify({'user': output})


# for patientMangement
@app.route("/allbpatientdetails", methods=['GET', 'POST'])
def AllBdetails():
    bpatient = session.query(BreastPatient).all()
    session.commit()
    user_schema = BPatientSchema(many=True)
    output = user_schema.dump(bpatient)
    return jsonify({'breast_patient': output})


# for patientMangement
@app.route("/allrpatientdetails", methods=['GET', 'POST'])
def AllRdetails():
    rpatient = session.query(RheumatologyPatient).all()
    session.commit()
    user_schema = RheumatologyPatientSchema(many=True)
    output = user_schema.dump(rpatient)
    return jsonify({'rheumatology_patient': output})


@app.route("/userdelete", methods=['GET', 'POST'])
def userdelete():
    global name
    uid = request.args['uid']
    try:
        users = session.query(Users).filter_by(id=uid).first()
        if users:
            name = users.username
        session.delete(users)
        session.commit()
        return "Success"
    except:
        return "Fail"


@app.route("/addactivity", methods=['GET', 'POST'])
def addactivity():
    if request.method == 'POST':
        user_id = request.json.get("user_id", False)
        activity = request.json.get("activity", False)

        try:
            dbadd = Activitylog(user_id=user_id, activity=activity)
            session.add(dbadd)
            session.commit()
            return "Success"
        except:
            return "Fail"
    return "get request made"


# For notifications in navbar

@app.route("/activitydetails", methods=['GET', 'POST'])
def activitydetails():
    activity = session.query(Activitylog).order_by(Activitylog.id.desc()).all()
    session.commit()
    activity_schema = ActivitySchema(many=True)
    output = activity_schema.dump(activity)
    return jsonify({'activity': output})


# Breast Patient

@app.route("/addbpatient", methods=['GET', 'POST'])
def addbpatient():
    if request.method == 'POST':
        pid = request.json.get("pid", False)
        date = request.json.get("date")
        code = request.json.get("code", False)
        place = request.json.get("place", False)
        pname = request.json.get("pname", False)
        fhname = request.json.get("fhname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        education = request.json.get("education", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        district = request.json.get("district", False)
        state = request.json.get("state", False)
        area = request.json.get("area", False)
        referred_by = request.json.get("referred_by", False)
        occupation = request.json.get("occupation", False)
        ethnic = request.json.get("ethnic", False)
        marital_status = request.json.get("marital_status", False)
        marital_status_years = request.json.get("marital_status_years", False)
        menses_frequency = request.json.get("menses_frequency", False)
        menses_loss = request.json.get("menses_loss", False)
        menarche_years = request.json.get("menarche_years", False)
        hystrectomy = request.json.get("hystrectomy", False)
        hystrectomy_years = request.json.get("hystrectomy_years", False)
        menopause = request.json.get("menopause", False)
        menopause_years = request.json.get("menopause_years", False)
        children = request.json.get("children", False)
        children_male = request.json.get("children_male", False)
        children_female = request.json.get("children_female", False)
        abortions = request.json.get("abortions", False)
        abortions_number = request.json.get("abortions_number", False)
        abortions_cause = request.json.get("abortions_cause", False)
        breastfed = request.json.get("breastfed", False)
        breastfed_years = request.json.get("breastfed_years", False)
        current_lactation = request.json.get("current_lactation", False)
        contraception_methods = request.json.get("contraception_methods", False)
        contraception_methods_type = request.json.get("contraception_methods_type", False)
        hormone_treatment = request.json.get("hormone_treatment", False)
        addiction = request.json.get("addiction", False)
        tobacco = request.json.get("tobacco", False)
        tobacco_years = request.json.get("tobacco_years", False)
        smoking = request.json.get("smoking", False)
        smoking_years = request.json.get("smoking_years", False)
        alcohol = request.json.get("alcohol", False)
        alcohol_years = request.json.get("alcohol_years", False)
        family_history = request.json.get("family_history", False)
        comorbidities = request.json.get("comorbidities", False)
        breastlump = request.json.get("breastlump", False)
        breastlump_location = request.json.get("breastlump_location", False)
        breastlump_size = request.json.get("breastlump_size", False)
        overlying_skin = request.json.get("overlying_skin", False)
        axillarylump = request.json.get("axillarylump", False)
        axillarylump_side = request.json.get("axillarylump_side", False)
        matted = request.json.get("matted", False)
        axillarylump_size = request.json.get("axillarylump_size", False)
        nipple_discharge = request.json.get("nipple_discharge", False)
        nipple_discharge_frequency = request.json.get("nipple_discharge_frequency", False)
        nipple_discharge_colour = request.json.get("nipple_discharge_colour", False)
        mastalgia = request.json.get("mastalgia", False)
        mastitis = request.json.get("mastitis", False)
        ulcer = request.json.get("ulcer", False)
        nipple_inversion = request.json.get("nipple_inversion", False)
        others = request.json.get("others", False)
        duration = request.json.get("duration", False)
        pasthistory = request.json.get("pasthistory", False)
        surgical_history = request.json.get("surgical_history", False)
        drug_history = request.json.get("drug_history", False)
        drug_allergy = request.json.get("drug_allergy", False)
        drug_allergy_type = request.json.get("drug_allergy_type", False)
        bowelhabit = request.json.get("bowelhabit", False)
        bladderhabit = request.json.get("bladderhabit", False)
        sleep = request.json.get("sleep", False)
        appetite = request.json.get("appetite", False)
        # examination
        weight = request.json.get("weight", False)
        height = request.json.get("height", False)
        bmi = request.json.get("bmi", False)
        bp = request.json.get("bp", False)
        pulse = request.json.get("pulse", False)
        temp = request.json.get("temp", False)
        respiratory_rate = request.json.get("respiratory_rate", False)
        health_condition = request.json.get("health_condition", False)
        examination_remarks = request.json.get("examination_remarks", False)
        bra_size = request.json.get("bra_size", False)
        # investigations
        usg = request.json.get("usg", False)
        mmg = request.json.get("mmg", False)
        mri = request.json.get("mri", False)
        fnac = request.json.get("fnac", False)
        core_biopsy = request.json.get("core_biopsy", False)
        incision_biopsy = request.json.get("incision_biopsy", False)
        investigation_remarks = request.json.get("investigation_remarks", False)
        blood_investigation = request.json.get("blood_investigation", False)
        diagnosis = request.json.get("diagnosis", False)
        treatment_plan = request.json.get("treatment_plan", False)
        # connection with doctor table
        user_id = request.json.get("user_id", False)

        try:
            dbadd = BreastPatient(diagnosis=diagnosis, treatment_plan=treatment_plan, user_id=user_id,
                                  incision_biopsy=incision_biopsy, investigation_remarks=investigation_remarks,
                                  blood_investigation=blood_investigation, examination_remarks=examination_remarks,
                                  bra_size=bra_size, usg=usg, mmg=mmg, mri=mri, fnac=fnac, core_biopsy=core_biopsy,
                                  weight=weight, height=height, bmi=bmi, bp=bp, pulse=pulse, temp=temp,
                                  respiratory_rate=respiratory_rate, health_condition=health_condition, pid=pid,
                                  date=date, code=code, place=place, pname=pname, fhname=fhname, age=age, sex=sex,
                                  education=education, address=address, phone=phone, district=district, state=state,
                                  area=area, referred_by=referred_by, occupation=occupation, ethnic=ethnic,
                                  marital_status=marital_status, marital_status_years=marital_status_years,
                                  menses_frequency=menses_frequency, menses_loss=menses_loss,
                                  menarche_years=menarche_years, hystrectomy=hystrectomy,
                                  hystrectomy_years=hystrectomy_years, menopause=menopause,
                                  menopause_years=menopause_years, children=children, children_male=children_male,
                                  children_female=children_female, abortions=abortions,
                                  abortions_number=abortions_number, abortions_cause=abortions_cause,
                                  breastfed=breastfed, breastfed_years=breastfed_years,
                                  current_lactation=current_lactation, contraception_methods=contraception_methods,
                                  contraception_methods_type=contraception_methods_type,
                                  hormone_treatment=hormone_treatment, addiction=addiction, tobacco=tobacco,
                                  tobacco_years=tobacco_years, smoking=smoking, smoking_years=smoking_years,
                                  alcohol=alcohol, alcohol_years=alcohol_years, family_history=family_history,
                                  comorbidities=comorbidities, breastlump=breastlump,
                                  breastlump_location=breastlump_location, breastlump_size=breastlump_size,
                                  overlying_skin=overlying_skin, axillarylump=axillarylump,
                                  axillarylump_side=axillarylump_side, matted=matted,
                                  axillarylump_size=axillarylump_size, nipple_discharge=nipple_discharge,
                                  nipple_discharge_frequency=nipple_discharge_frequency,
                                  nipple_discharge_colour=nipple_discharge_colour, mastalgia=mastalgia,
                                  mastitis=mastitis, ulcer=ulcer, nipple_inversion=nipple_inversion, others=others,
                                  duration=duration, pasthistory=pasthistory, surgical_history=surgical_history,
                                  drug_history=drug_history, drug_allergy=drug_allergy,
                                  drug_allergy_type=drug_allergy_type, bowelhabit=bowelhabit, bladderhabit=bladderhabit,
                                  sleep=sleep, appetite=appetite, )
            session.add(dbadd)
            session.commit()
            return "Success"
        except:
            return "Fail"
    return "get request made"


# view patientdetails
@app.route("/bpatientdetails", methods=['GET', 'POST'])
def Bpatientdetails():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        bpatient = session.query(BreastPatient).filter_by(id=pid).first()
        session.commit()
        print(bpatient.pname)
        bpatientschema = BPatientSchema()
        output = bpatientschema.dump(bpatient)
        return jsonify({'bpatient': output})
    except:
        return "Wrong Pid"


@app.route("/updatebpatient", methods=['GET', 'POST'])
def updatebpatient():
    if request.method == 'POST':
        pid = request.json.get("pid", False)
        # pid is must
        code = request.json.get("code", False)
        place = request.json.get("place", False)
        pname = request.json.get("pname", False)
        fhname = request.json.get("fhname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        education = request.json.get("education", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        district = request.json.get("district", False)
        state = request.json.get("state", False)
        area = request.json.get("area", False)
        referred_by = request.json.get("referred_by", False)
        occupation = request.json.get("occupation", False)
        ethnic = request.json.get("ethnic", False)
        marital_status = request.json.get("marital_status", False)
        marital_status_years = request.json.get("marital_status_years", False)
        menses_frequency = request.json.get("menses_frequency", False)
        menses_loss = request.json.get("menses_loss", False)
        menarche_years = request.json.get("menarche_years", False)
        hystrectomy = request.json.get("hystrectomy", False)
        hystrectomy_years = request.json.get("hystrectomy_years", False)
        menopause = request.json.get("menopause", False)
        menopause_years = request.json.get("menopause_years", False)
        children = request.json.get("children", False)
        children_male = request.json.get("children_male", False)
        children_female = request.json.get("children_female", False)
        abortions = request.json.get("abortions", False)
        abortions_number = request.json.get("abortions_number", False)
        abortions_cause = request.json.get("abortions_cause", False)
        breastfed = request.json.get("breastfed", False)
        breastfed_years = request.json.get("breastfed_years", False)
        current_lactation = request.json.get("current_lactation", False)
        contraception_methods = request.json.get("contraception_methods", False)
        contraception_methods_type = request.json.get("contraception_methods_type", False)
        hormone_treatment = request.json.get("hormone_treatment", False)
        addiction = request.json.get("addiction", False)
        tobacco = request.json.get("tobacco", False)
        tobacco_years = request.json.get("tobacco_years", False)
        smoking = request.json.get("smoking", False)
        smoking_years = request.json.get("smoking_years", False)
        alcohol = request.json.get("alcohol", False)
        alcohol_years = request.json.get("alcohol_years", False)
        family_history = request.json.get("family_history", False)
        comorbidities = request.json.get("comorbidities", False)
        breastlump = request.json.get("breastlump", False)
        breastlump_location = request.json.get("breastlump_location", False)
        breastlump_size = request.json.get("breastlump_size", False)
        overlying_skin = request.json.get("overlying_skin", False)
        axillarylump = request.json.get("axillarylump", False)
        axillarylump_side = request.json.get("axillarylump_side", False)
        matted = request.json.get("matted", False)
        axillarylump_size = request.json.get("axillarylump_size", False)
        nipple_discharge = request.json.get("nipple_discharge", False)
        nipple_discharge_frequency = request.json.get("nipple_discharge_frequency", False)
        nipple_discharge_colour = request.json.get("nipple_discharge_colour", False)
        mastalgia = request.json.get("mastalgia", False)
        mastitis = request.json.get("mastitis", False)
        ulcer = request.json.get("ulcer", False)
        nipple_inversion = request.json.get("nipple_inversion", False)
        others = request.json.get("others", False)
        duration = request.json.get("duration", False)
        pasthistory = request.json.get("pasthistory", False)
        surgical_history = request.json.get("surgical_history", False)
        drug_history = request.json.get("drug_history", False)
        drug_allergy = request.json.get("drug_allergy", False)
        drug_allergy_type = request.json.get("drug_allergy_type", False)
        bowelhabit = request.json.get("bowelhabit", False)
        bladderhabit = request.json.get("bladderhabit", False)
        sleep = request.json.get("sleep", False)
        appetite = request.json.get("appetite", False)
        # examination
        weight = request.json.get("weight", False)
        height = request.json.get("height", False)
        bmi = request.json.get("bmi", False)
        bp = request.json.get("bp", False)
        pulse = request.json.get("pulse", False)
        temp = request.json.get("temp", False)
        respiratory_rate = request.json.get("respiratory_rate", False)
        health_condition = request.json.get("health_condition", False)
        examination_remarks = request.json.get("examination_remarks", False)
        bra_size = request.json.get("bra_size", False)
        # investigations
        usg = request.json.get("usg", False)
        mmg = request.json.get("mmg", False)
        mri = request.json.get("mri", False)
        fnac = request.json.get("fnac", False)
        core_biopsy = request.json.get("core_biopsy", False)
        incision_biopsy = request.json.get("incision_biopsy", False)
        investigation_remarks = request.json.get("investigation_remarks", False)
        blood_investigation = request.json.get("blood_investigation", False)
        diagnosis = request.json.get("diagnosis", False)
        treatment_plan = request.json.get("treatment_plan", False)
        # connection with doctor table
        user_id = request.json.get("user_id", False)

        # return "send all parameters"
        try:
            dbedit = session.query(BreastPatient).filter_by(pid=pid).update(
                dict(diagnosis=diagnosis, treatment_plan=treatment_plan, user_id=user_id,
                     incision_biopsy=incision_biopsy, investigation_remarks=investigation_remarks,
                     blood_investigation=blood_investigation, examination_remarks=examination_remarks,
                     bra_size=bra_size, usg=usg, mmg=mmg, mri=mri, fnac=fnac, core_biopsy=core_biopsy, weight=weight,
                     height=height, bmi=bmi, bp=bp, pulse=pulse, temp=temp, respiratory_rate=respiratory_rate,
                     health_condition=health_condition, code=code, place=place, pname=pname, fhname=fhname, age=age,
                     sex=sex, education=education, address=address, phone=phone, district=district, state=state,
                     area=area, referred_by=referred_by, occupation=occupation, ethnic=ethnic,
                     marital_status=marital_status, marital_status_years=marital_status_years,
                     menses_frequency=menses_frequency, menses_loss=menses_loss, menarche_years=menarche_years,
                     hystrectomy=hystrectomy, hystrectomy_years=hystrectomy_years, menopause=menopause,
                     menopause_years=menopause_years, children=children, children_male=children_male,
                     children_female=children_female, abortions=abortions, abortions_number=abortions_number,
                     abortions_cause=abortions_cause, breastfed=breastfed, breastfed_years=breastfed_years,
                     current_lactation=current_lactation, contraception_methods=contraception_methods,
                     contraception_methods_type=contraception_methods_type, hormone_treatment=hormone_treatment,
                     addiction=addiction, tobacco=tobacco, tobacco_years=tobacco_years, smoking=smoking,
                     smoking_years=smoking_years, alcohol=alcohol, alcohol_years=alcohol_years,
                     family_history=family_history, comorbidities=comorbidities, breastlump=breastlump,
                     breastlump_location=breastlump_location, breastlump_size=breastlump_size,
                     overlying_skin=overlying_skin, axillarylump=axillarylump, axillarylump_side=axillarylump_side,
                     matted=matted, axillarylump_size=axillarylump_size, nipple_discharge=nipple_discharge,
                     nipple_discharge_frequency=nipple_discharge_frequency,
                     nipple_discharge_colour=nipple_discharge_colour, mastalgia=mastalgia, mastitis=mastitis,
                     ulcer=ulcer, nipple_inversion=nipple_inversion, others=others, duration=duration,
                     pasthistory=pasthistory, surgical_history=surgical_history, drug_history=drug_history,
                     drug_allergy=drug_allergy, drug_allergy_type=drug_allergy_type, bowelhabit=bowelhabit,
                     bladderhabit=bladderhabit, sleep=sleep, appetite=appetite, ))
            session.commit()

            # sending back updated data of the patient
            patientdata = session.query(BreastPatient).filter_by(pid=pid).first()
            session.commit()
            bpatient_schema = BPatientSchema()
            output = bpatient_schema.dump(patientdata)
            return jsonify({'bpatient': output})
        except:
            return "Send all Parameters"
    return "get request made"


@app.route("/bpatientdelete", methods=['GET', 'POST'])
def Bpatientdelete():
    global name
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        bpatient = session.query(BreastPatient).filter_by(pid=pid).first()
        if bpatient:
            name = bpatient.pname
        session.delete(bpatient)
        session.commit()
        return "Success"
    except:
        return "Fail"


# Rheumatology History starts here
# view details
@app.route("/rheumapatientdetails", methods=['GET', 'POST'])
def rheumapatientdetails():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyPatient).filter_by(pid=pid).first()
        session.commit()
        RheumatologyPSchema = RheumatologyPatientSchema()
        output = RheumatologyPSchema.dump(RheumatologyPat)
        return jsonify({'RheumatologyPatientHistory': output})
    except:
        return "Wrong Pid"


#deleteDetails
@app.route("/rheumapatientdelete", methods=['GET','POST'])
def rheumapatientdelete():
    global name
    try:
        pid = request.args['pid']
        RheumatologyPat = session.query(RheumatologyPatient).filter_by(pid=pid).all()
        if not RheumatologyPat:
            return "patient doesnot exsit", 203
    except:
        return "no pid sent"
    print(pid)

    try:
        RheumatologyPat = session.query(RAttachments).filter_by(pid=pid).all()
        print(RheumatologyPat)
        if RheumatologyPat:
            num_rows_deleted = session.query(RAttachments).filter_by(pid=pid).delete()
            print(num_rows_deleted)
            session.commit()
            print("attachment found")

    except:
        session.rollback()
        return "attachment entry found"
    try:
        RheumatologyPat = session.query(RheumatologyFollowup).filter_by(pid=pid).all()
        print(RheumatologyPat)
        if RheumatologyPat:
            num_rows_deleted = session.query(RheumatologyFollowup).filter_by(pid=pid).delete()
            print(num_rows_deleted)
            session.commit()
            print("followup found")

    except:
        session.rollback()
        return "followup entry availabel"
    try:
        RheumatologyPat = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).all()
        print(RheumatologyPat)
        if RheumatologyPat:
            num_rows_deleted = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).delete()
            print(num_rows_deleted)
            session.commit()
            print("followup found")

    except:
        session.rollback()
        return "Investigation entry availabel"
    try:
        RheumatologyPat = session.query(RheumatologyPatientExamination).filter_by(pid=pid).all()
        print(RheumatologyPat)
        if RheumatologyPat:
            num_rows_deleted = session.query(RheumatologyPatientExamination).filter_by(pid=pid).delete()
            print(num_rows_deleted)
            session.commit()
            print("followup found")
    except:
        session.rollback()
        return "Examination entry availabel"
    try:
        RheumatologyPat = session.query(RheumatologyPatient).filter_by(pid=pid).first()
        if RheumatologyPat:
            name = RheumatologyPat.pname
            session.delete(RheumatologyPat)
            session.commit()
            return "Success"
    except:
        session.rollback()
        return "fail"
    return "fail"


@app.route("/addrheumapatient", methods=['GET', 'POST'])
def addrheumapatient():
    if request.method == 'POST':
        pid = request.json.get("pid", False)
        date = request.json.get("date")
        code = request.json.get("code", False)
        place = request.json.get("place", False)
        pname = request.json.get("pname", False)
        fhname = request.json.get("fhname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        education = request.json.get("education", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        district = request.json.get("district", False)
        state = request.json.get("state", False)
        area = request.json.get("area", False)
        referred_by = request.json.get("referred_by", False)
        occupation = request.json.get("occupation", False)
        ethnic = request.json.get("ethnic", False)
        marital_status = request.json.get("marital_status", False)
        marital_status_years = request.json.get("marital_status_years", False)
        menses_frequency = request.json.get("menses_frequency", False)
        menses_loss = request.json.get("menses_loss", False)
        menarche_years = request.json.get("menarche_years", False)
        hysterectomy = request.json.get("hysterectomy", False)
        hysterectomy_years = request.json.get("hysterectomy_years", False)
        menopause = request.json.get("menopause", False)
        menopause_years = request.json.get("menopause_years", False)
        children = request.json.get("children", False)
        children_male = request.json.get("children_male", False)
        children_female = request.json.get("children_female", False)
        abortions = request.json.get("abortions", False)
        abortions_number = request.json.get("abortions_number", False)
        abortions_cause = request.json.get("abortions_cause", False)
        current_lactation = request.json.get("current_lactation", False)
        contraception_methods = request.json.get("contraception_methods", False)
        contraception_methods_type = request.json.get("contraception_methods_type", False)
        hormone_treatment = request.json.get("hormone_treatment", False)
        addiction = request.json.get("addiction", False)
        tobacco = request.json.get("tobacco", False)
        tobacco_years = request.json.get("tobacco_years", False)
        smoking = request.json.get("smoking", False)
        smoking_years = request.json.get("smoking_years", False)
        alcohol = request.json.get("alcohol", False)
        alcohol_years = request.json.get("alcohol_years", False)
        family_history = request.json.get("family_history", False)
        health_condition = request.json.get("health_condition", False)
        remarks = request.json.get("remarks", False)
        comorbidities = request.json.get("comorbidities", False)
        # COMPLAINTS
        onset = request.json.get("onset", False)
        onset_duration = request.json.get("onset_duration", False)
        onset_duration_type = request.json.get("onset_duration_type", False)
        first_symptom = request.json.get("first_symptom", False)
        initial_site_joint = request.json.get("initial_site_joint", False)
        soft_tissue = request.json.get("soft_tissue", False)
        others = request.json.get("others", False)
        course1 = request.json.get("course1", False)
        course2 = request.json.get("course2", False)
        pattern1 = request.json.get("pattern1", False)
        pattern2 = request.json.get("pattern2", False)
        current_relapse = request.json.get("current_relapse", False)
        current_relapse_type = request.json.get("current_relapse_type", False)
        detailed_history = request.json.get("detailed_history", False)
        pasthistory = request.json.get("pasthistory", False)
        surgical_history = request.json.get("surgical_history", False)
        drug_history = request.json.get("drug_history", False)
        drug_allergy = request.json.get("drug_allergy", False)
        drug_allergy_type = request.json.get("drug_allergy_type", False)
        bowelhabit = request.json.get("bowelhabit", False)
        sleep = request.json.get("sleep", False)
        user_id = request.json.get("user_id", False)

        try:
            dbadd = RheumatologyPatient(pid=pid, date=date, code=code, place=place, pname=pname, fhname=fhname, age=age,
                                        sex=sex, education=education, address=address, phone=phone, district=district,
                                        state=state, area=area, referred_by=referred_by, occupation=occupation,
                                        ethnic=ethnic, marital_status=marital_status,
                                        marital_status_years=marital_status_years, menses_frequency=menses_frequency,
                                        menses_loss=menses_loss, menarche_years=menarche_years,
                                        hysterectomy=hysterectomy, hysterectomy_years=hysterectomy_years,
                                        menopause=menopause, menopause_years=menopause_years, children=children,
                                        children_male=children_male, children_female=children_female,
                                        abortions=abortions, abortions_number=abortions_number,
                                        abortions_cause=abortions_cause, current_lactation=current_lactation,
                                        contraception_methods=contraception_methods,
                                        contraception_methods_type=contraception_methods_type,
                                        hormone_treatment=hormone_treatment, addiction=addiction, tobacco=tobacco,
                                        tobacco_years=tobacco_years, smoking=smoking, smoking_years=smoking_years,
                                        alcohol=alcohol, alcohol_years=alcohol_years, family_history=family_history,
                                        health_condition=health_condition, remarks=remarks, comorbidities=comorbidities,
                                        onset=onset, onset_duration=onset_duration,
                                        onset_duration_type=onset_duration_type, first_symptom=first_symptom,
                                        initial_site_joint=initial_site_joint, soft_tissue=soft_tissue, others=others,
                                        course1=course1, course2=course2, pattern1=pattern1, pattern2=pattern2,
                                        current_relapse=current_relapse, current_relapse_type=current_relapse_type,
                                        detailed_history=detailed_history, pasthistory=pasthistory,
                                        surgical_history=surgical_history, drug_history=drug_history,
                                        drug_allergy=drug_allergy, drug_allergy_type=drug_allergy_type,
                                        bowelhabit=bowelhabit, sleep=sleep, user_id=user_id)
            session.add(dbadd)
            session.commit()
            return "Success"
        except IntegrityError:
            return "Same PID given"

    return "get request made"


@app.route("/updaterheumapatient", methods=['GET', 'POST'])
def updaterheumapatient():
    if request.method == 'POST':
        # id = request.json.get("id", False)
        pid = request.json.get("pid", False)

        code = request.json.get("code", False)
        place = request.json.get("place", False)
        pname = request.json.get("pname", False)
        fhname = request.json.get("fhname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        education = request.json.get("education", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        district = request.json.get("district", False)
        state = request.json.get("state", False)
        area = request.json.get("area", False)
        referred_by = request.json.get("referred_by", False)
        occupation = request.json.get("occupation", False)
        ethnic = request.json.get("ethnic", False)
        marital_status = request.json.get("marital_status", False)
        marital_status_years = request.json.get("marital_status_years", False)
        menses_frequency = request.json.get("menses_frequency", False)
        menses_loss = request.json.get("menses_loss", False)
        menarche_years = request.json.get("menarche_years", False)
        hysterectomy = request.json.get("hysterectomy", False)
        hysterectomy_years = request.json.get("hysterectomy_years", False)
        menopause = request.json.get("menopause", False)
        menopause_years = request.json.get("menopause_years", False)
        children = request.json.get("children", False)
        children_male = request.json.get("children_male", False)
        children_female = request.json.get("children_female", False)
        abortions = request.json.get("abortions", False)
        abortions_number = request.json.get("abortions_number", False)
        abortions_cause = request.json.get("abortions_cause", False)
        current_lactation = request.json.get("current_lactation", False)
        contraception_methods = request.json.get("contraception_methods", False)
        contraception_methods_type = request.json.get("contraception_methods_type", False)
        hormone_treatment = request.json.get("hormone_treatment", False)
        addiction = request.json.get("addiction", False)
        tobacco = request.json.get("tobacco", False)
        tobacco_years = request.json.get("tobacco_years", False)
        smoking = request.json.get("smoking", False)
        smoking_years = request.json.get("smoking_years", False)
        alcohol = request.json.get("alcohol", False)
        alcohol_years = request.json.get("alcohol_years", False)
        family_history = request.json.get("family_history", False)
        health_condition = request.json.get("health_condition", False)
        remarks = request.json.get("remarks", False)
        comorbidities = request.json.get("comorbidities", False)
        # COMPLAINTS
        onset = request.json.get("onset", False)
        onset_duration = request.json.get("onset_duration", False)
        onset_duration_type = request.json.get("onset_duration_type", False)
        first_symptom = request.json.get("first_symptom", False)
        initial_site_joint = request.json.get("initial_site_joint", False)
        soft_tissue = request.json.get("soft_tissue", False)
        others = request.json.get("others", False)
        course1 = request.json.get("course1", False)
        course2 = request.json.get("course2", False)
        pattern1 = request.json.get("pattern1", False)
        pattern2 = request.json.get("pattern2", False)
        current_relapse = request.json.get("current_relapse", False)
        current_relapse_type = request.json.get("current_relapse_type", False)
        detailed_history = request.json.get("detailed_history", False)
        pasthistory = request.json.get("pasthistory", False)
        surgical_history = request.json.get("surgical_history", False)
        drug_history = request.json.get("drug_history", False)
        drug_allergy = request.json.get("drug_allergy", False)
        drug_allergy_type = request.json.get("drug_allergy_type", False)
        bowelhabit = request.json.get("bowelhabit", False)
        sleep = request.json.get("sleep", False)

        user_id = request.json.get("user_id", False)

        try:
            dbedit = session.query(RheumatologyPatient).filter_by(pid=pid).update(
                dict(code=code, place=place, pname=pname, fhname=fhname, age=age, sex=sex, education=education,
                     address=address, phone=phone, district=district, state=state, area=area, referred_by=referred_by,
                     occupation=occupation, ethnic=ethnic, marital_status=marital_status,
                     marital_status_years=marital_status_years, menses_frequency=menses_frequency,
                     menses_loss=menses_loss, menarche_years=menarche_years, hysterectomy=hysterectomy,
                     hysterectomy_years=hysterectomy_years, menopause=menopause, menopause_years=menopause_years,
                     children=children, children_male=children_male, children_female=children_female,
                     abortions=abortions, abortions_number=abortions_number, abortions_cause=abortions_cause,
                     current_lactation=current_lactation, contraception_methods=contraception_methods,
                     contraception_methods_type=contraception_methods_type, hormone_treatment=hormone_treatment,
                     addiction=addiction, tobacco=tobacco, tobacco_years=tobacco_years, smoking=smoking,
                     smoking_years=smoking_years, alcohol=alcohol, alcohol_years=alcohol_years,
                     family_history=family_history, health_condition=health_condition, remarks=remarks,
                     comorbidities=comorbidities, onset=onset, onset_duration=onset_duration,
                     onset_duration_type=onset_duration_type, first_symptom=first_symptom,
                     initial_site_joint=initial_site_joint, soft_tissue=soft_tissue, others=others, course1=course1,
                     course2=course2, pattern1=pattern1, pattern2=pattern2, current_relapse=current_relapse,
                     current_relapse_type=current_relapse_type, detailed_history=detailed_history,
                     pasthistory=pasthistory, surgical_history=surgical_history, drug_history=drug_history,
                     drug_allergy=drug_allergy, drug_allergy_type=drug_allergy_type, bowelhabit=bowelhabit, sleep=sleep,
                     user_id=user_id))
            session.commit()
            # sending back updated data of the user
            patientdata = session.query(RheumatologyPatient).filter_by(pid=pid).first()
            session.commit()
            patientschema = RheumatologyPatientSchema()
            output = patientschema.dump(patientdata)
            return jsonify({'RheumatologyPatientHistory': output})
        except:
            return "Fail"
    return "get request made"

    # Rheumatology Examinations starts here
    # view details


@app.route("/rheumapatientexaminationdetails", methods=['GET', 'POST'])
def rheumapatientexaminationdetails():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyPatientExamination).filter_by(pid=pid).first()
        session.commit()
        RheumatologyPSchema = RheumatologyPatientExaminationSchema()
        output = RheumatologyPSchema.dump(RheumatologyPat)
        return jsonify({'RheumatologyPatientExamination': output})
    except:
        return "Wrong Pid"


# deleteDetails
@app.route("/rheumapatientexaminationdelete", methods=['GET', 'POST'])
def rheumapatientexaminationdelete():
    global name
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    print(pid)
    try:
        RheumatologyPat = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).all()
        print(RheumatologyPat)
        if RheumatologyPat:
            num_rows_deleted = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).delete()
            print(num_rows_deleted)
            session.commit()
            print("followup found")

    except:
        session.rollback()
        return "Investigation entry availabel"
    try:
        RheumatologyPat = session.query(RheumatologyPatientExamination).filter_by(pid=pid).first()
        print(RheumatologyPat)
        if RheumatologyPat:
            session.delete(RheumatologyPat)
            session.commit()
            return "Success"
        return "pid does not exist"
    except:
        return "Fail"


@app.route("/addrheumapatientexamination", methods=['GET', 'POST'])
def addrheumapatientexamination():
    if request.method == 'POST':
        pid = request.json.get("pid", False)
        user_id = request.json.get("user_id", False)

        # EXAMINATION
        general_condition = request.json.get("general_condition", False)
        weight = request.json.get("weight", False)
        height = request.json.get("height", False)
        bmi = request.json.get("bmi", False)
        # vitals
        bp = request.json.get("bp", False)
        pulse = request.json.get("pulse", False)
        temp = request.json.get("temp", False)
        respiratory_rate = request.json.get("respiratory_rate", False)
        ex_health_condition = request.json.get("ex_health_condition", False)
        ex_remarks = request.json.get("ex_remarks", False)
        eyes = request.json.get("eyes", False)
        skin_morphology = request.json.get("skin_morphology", False)
        skin_pattern = request.json.get("skin_pattern", False)
        skin_color = request.json.get("skin_color", False)
        distribution = request.json.get("distribution", False)
        other_affects = request.json.get("other_affects", False)
        mucosa = request.json.get("mucosa", False)
        hair_loss = request.json.get("hair_loss", False)
        dandruff = request.json.get("dandruff", False)
        nail = request.json.get("nail", False)
        lymphnodes = request.json.get("lymphnodes", False)
        lymphnodes_number = request.json.get("lymphnodes_number", False)
        tender = request.json.get("tender", False)
        tender_type = request.json.get("tender_type", False)
        lymphnodes_tender_others = request.json.get("lymphnodes_tender_others", False)
        vascular = request.json.get("vascular", False)
        cns_examination = request.json.get("cns_examination", False)
        cvs_examination = request.json.get("cvs_examination", False)
        chest_examination = request.json.get("chest_examination", False)
        abdomen_examination = request.json.get("abdomen_examination", False)
        # MUSCULOSKELETAL_EXAMINATIONS
        # joint_evaluation:
        # row1
        tm_t1 = request.json.get("tm_t1")
        tm_s1 = request.json.get("tm_s1", False)
        tm_t2 = request.json.get("tm_t2", False)
        tm_s2 = request.json.get("tm_s2", False)
        scl_t1 = request.json.get("scl_t1", False)
        scl_s1 = request.json.get("scl_s1", False)
        scl_t2 = request.json.get("scl_t2", False)
        scl_s2 = request.json.get("scl_s2", False)
        acl_t1 = request.json.get("acl_t1", False)
        acl_s1 = request.json.get("acl_s1", False)
        acl_t2 = request.json.get("acl_t2", False)
        acl_s2 = request.json.get("acl_s2", False)
        sh_t1 = request.json.get("sh_t1", False)
        sh_s1 = request.json.get("sh_s1", False)
        sh_t2 = request.json.get("sh_t2", False)
        sh_s2 = request.json.get("sh_s2", False)
        elbow_t1 = request.json.get("elbow_t1", False)
        elbow_s1 = request.json.get("elbow_s1", False)
        elbow_t2 = request.json.get("elbow_t2", False)
        elbow_s2 = request.json.get("elbow_s2", False)
        infru_t1 = request.json.get("infru_t1", False)
        infru_s1 = request.json.get("infru_s1", False)
        infru_t2 = request.json.get("infru_t2", False)
        infru_s2 = request.json.get("infru_s2", False)
        cmc1_t1 = request.json.get("cmc1_t1", False)
        cmc1_s1 = request.json.get("cmc1_s1", False)
        cmc1_t2 = request.json.get("cmc1_t2", False)
        cmc1_s2 = request.json.get("cmc1_s2", False)
        wrist_t1 = request.json.get("wrist_t1", False)
        wrist_s1 = request.json.get("wrist_s1", False)
        wrist_t2 = request.json.get("wrist_t2", False)
        wrist_s2 = request.json.get("wrist_s2", False)
        dip2_t1 = request.json.get("dip2_t1", False)
        dip2_s1 = request.json.get("dip2_s1", False)
        dip2_t2 = request.json.get("dip2_t2", False)
        dip2_s2 = request.json.get("dip2_s2", False)
        dip3_t1 = request.json.get("dip3_t1", False)
        dip3_s1 = request.json.get("dip3_s1", False)
        dip3_t2 = request.json.get("dip3_t2", False)
        dip3_s2 = request.json.get("dip3_s2", False)
        dip4_t1 = request.json.get("dip4_t1", False)
        dip4_s1 = request.json.get("dip4_s1", False)
        dip4_t2 = request.json.get("dip4_t2", False)
        dip4_s2 = request.json.get("dip4_s2", False)
        dip5_t1 = request.json.get("dip5_t1", False)
        dip5_s1 = request.json.get("dip5_s1", False)
        dip5_t2 = request.json.get("dip5_t2", False)
        dip5_s2 = request.json.get("dip5_s2", False)
        ip1_t1 = request.json.get("ip1_t1", False)
        ip1_s1 = request.json.get("ip1_s1", False)
        ip1_t2 = request.json.get("ip1_t2", False)
        ip1_s2 = request.json.get("ip1_s2", False)
        ip2_t1 = request.json.get("ip2_t1", False)
        ip2_s1 = request.json.get("ip2_s1", False)
        ip2_t2 = request.json.get("ip2_t2", False)
        ip2_s2 = request.json.get("ip2_s2", False)
        pip3_t1 = request.json.get("pip3_t1", False)
        pip3_s1 = request.json.get("pip3_s1", False)
        pip3_t2 = request.json.get("pip3_t2", False)
        pip3_s2 = request.json.get("pip3_s2", False)
        pip4_t1 = request.json.get("pip4_t1", False)
        pip4_s1 = request.json.get("pip4_s1", False)
        pip4_t2 = request.json.get("pip4_t2", False)
        pip4_s2 = request.json.get("pip4_s2", False)
        pip5_t1 = request.json.get("pip5_t1", False)
        pip5_s1 = request.json.get("pip5_s1", False)
        pip5_t2 = request.json.get("pip5_t2", False)
        pip5_s2 = request.json.get("pip5_s2", False)
        mcp1_t1 = request.json.get("mcp1_t1", False)
        mcp1_s1 = request.json.get("mcp1_s1", False)
        mcp1_t2 = request.json.get("mcp1_t2", False)
        mcp1_s2 = request.json.get("mcp1_s2", False)
        mcp2_t1 = request.json.get("mcp2_t1", False)
        mcp2_s1 = request.json.get("mcp2_s1", False)
        mcp2_t2 = request.json.get("mcp2_t2", False)
        mcp2_s2 = request.json.get("mcp2_s2", False)

        # row2
        mcp3_t1 = request.json.get("mcp3_t1", False)
        mcp3_s1 = request.json.get("mcp3_s1", False)
        mcp3_t2 = request.json.get("mcp3_t2", False)
        mcp3_s2 = request.json.get("mcp3_s2", False)
        mcp4_t1 = request.json.get("mcp4_t1", False)
        mcp4_s1 = request.json.get("mcp4_s1", False)
        mcp4_t2 = request.json.get("mcp4_t2", False)
        mcp4_s2 = request.json.get("mcp4_s2", False)
        mcp5_t1 = request.json.get("mcp5_t1", False)
        mcp5_s1 = request.json.get("mcp5_s1", False)
        mcp5_t2 = request.json.get("mcp5_t2", False)
        mcp5_s2 = request.json.get("mcp5_s2", False)
        hip_t1 = request.json.get("hip_t1", False)
        hip_s1 = request.json.get("hip_s1", False)
        hip_t2 = request.json.get("hip_t2", False)
        hip_s2 = request.json.get("hip_s2", False)
        knee_t1 = request.json.get("knee_t1", False)
        knee_s1 = request.json.get("knee_s1", False)
        knee_t2 = request.json.get("knee_t2", False)
        knee_s2 = request.json.get("knee_s2", False)
        a_tt_t1 = request.json.get("a_tt_t1", False)
        a_tt_s1 = request.json.get("a_tt_s1", False)
        a_tt_t2 = request.json.get("a_tt_t2", False)
        a_tt_s2 = request.json.get("a_tt_s2", False)
        a_tc_t1 = request.json.get("a_tc_t1", False)
        a_tc_s1 = request.json.get("a_tc_s1", False)
        a_tc_t2 = request.json.get("a_tc_t2", False)
        a_tc_s2 = request.json.get("a_tc_s2", False)
        mtl_t1 = request.json.get("mtl_t1", False)
        mtl_s1 = request.json.get("mtl_s1", False)
        mtl_t2 = request.json.get("mtl_t2", False)
        mtl_s2 = request.json.get("mtl_s2", False)
        mtp1_t1 = request.json.get("mtp1_t1", False)
        mtp1_s1 = request.json.get("mtp1_s1", False)
        mtp1_t2 = request.json.get("mtp1_t2", False)
        mtp1_s2 = request.json.get("mtp1_s2", False)
        mtp2_t1 = request.json.get("mtp2_t1", False)
        mtp2_s1 = request.json.get("mtp2_s1", False)
        mtp2_t2 = request.json.get("mtp2_t2", False)
        mtp2_s2 = request.json.get("mtp2_s2", False)
        mtp3_t1 = request.json.get("mtp3_t1", False)
        mtp3_s1 = request.json.get("mtp3_s1", False)
        mtp3_t2 = request.json.get("mtp3_t2", False)
        mtp3_s2 = request.json.get("mtp3_s2", False)
        mtp4_t1 = request.json.get("mtp4_t1", False)
        mtp4_s1 = request.json.get("mtp4_s1", False)
        mtp4_t2 = request.json.get("mtp4_t2", False)
        mtp4_s2 = request.json.get("mtp4_s2", False)
        mtp5_t1 = request.json.get("mtp5_t1", False)
        mtp5_s1 = request.json.get("mtp5_s1", False)
        mtp5_t2 = request.json.get("mtp5_t2", False)
        mtp5_s2 = request.json.get("mtp5_s2", False)
        ip1b_t1 = request.json.get("ip1b_t1", False)
        ip1b_s1 = request.json.get("ip1b_s1", False)
        ip1b_t2 = request.json.get("ip1b_t2", False)
        ip1b_s2 = request.json.get("ip1b_s2", False)
        ip2b_t1 = request.json.get("ip2b_t1", False)
        ip2b_s1 = request.json.get("ip2b_s1", False)
        ip2b_t2 = request.json.get("ip2b_t2", False)
        ip2b_s2 = request.json.get("ip2b_s2", False)
        ip3b_t1 = request.json.get("ip3b_t1", False)
        ip3b_s1 = request.json.get("ip3b_s1", False)
        ip3b_t2 = request.json.get("ip3b_t2", False)
        ip3b_s2 = request.json.get("ip3b_s2", False)
        ip4b_t1 = request.json.get("ip4b_t1", False)
        ip4b_s1 = request.json.get("ip4b_s1", False)
        ip4b_t2 = request.json.get("ip4b_t2", False)
        ip4b_s2 = request.json.get("ip4b_s2", False)
        ip5b_t1 = request.json.get("ip5b_t1", False)
        ip5b_s1 = request.json.get("ip5b_s1", False)
        ip5b_t2 = request.json.get("ip5b_t2", False)
        ip5b_s2 = request.json.get("ip5b_s2", False)
        s1_t1 = request.json.get("s1_t1", False)
        s1_s1 = request.json.get("s1_s1", False)
        s1_t2 = request.json.get("s1_t2", False)
        s1_s2 = request.json.get("s1_s2", False)
        # ROM
        cervical_anky = request.json.get("cervical_anky", False)
        cervical_flex = request.json.get("cervical_flex", False)
        cervical_ext = request.json.get("cervical_ext", False)
        cervical_rtrot = request.json.get("cervical_rtrot", False)
        cervical_ltrot = request.json.get("cervical_ltrot", False)
        cervical_rtfl = request.json.get("cervical_rtfl", False)
        cervical_ltfl = request.json.get("cervical_ltfl", False)
        cervical_pt = request.json.get("cervical_pt", False)
        thorasic_anky = request.json.get("thorasic_anky", False)
        thorasic_flex = request.json.get("thorasic_flex", False)
        thorasic_ext = request.json.get("thorasic_ext", False)
        thorasic_rtrot = request.json.get("thorasic_rtrot", False)
        thorasic_ltrot = request.json.get("thorasic_ltrot", False)
        thorasic_rtfl = request.json.get("thorasic_rtfl", False)
        thorasic_ltfl = request.json.get("thorasic_ltfl", False)
        thorasic_pt = request.json.get("thorasic_pt", False)
        lumbar_anky = request.json.get("lumbar_anky", False)
        lumbar_flex = request.json.get("lumbar_flex", False)
        lumbar_ext = request.json.get("lumbar_ext", False)
        lumbar_rtrot = request.json.get("lumbar_rtrot", False)
        lumbar_ltrot = request.json.get("lumbar_ltrot", False)
        lumbar_rtfl = request.json.get("lumbar_rtfl", False)
        lumbar_ltfl = request.json.get("lumbar_ltfl", False)
        lumbar_pt = request.json.get("lumbar_pt", False)
        # SOFT TISSUE RHEUMATISM
        opt_rt = request.json.get("opt_rt", False)
        opt_lt = request.json.get("opt_lt", False)
        lcer_rt = request.json.get("lcer_rt", False)
        lcer_lt = request.json.get("lcer_lt", False)
        trpz_rt = request.json.get("trpz_rt", False)
        trpz_lt = request.json.get("trpz_lt", False)
        scap_rt = request.json.get("scap_rt", False)
        scap_lt = request.json.get("scap_lt", False)
        zcst_rt = request.json.get("zcst_rt", False)
        zcst_lt = request.json.get("zcst_lt", False)
        epdl_rt = request.json.get("epdl_rt", False)
        epdl_lt = request.json.get("epdl_lt", False)
        glut_rt = request.json.get("glut_rt", False)
        glut_lt = request.json.get("glut_lt", False)
        trcr_rt = request.json.get("trcr_rt", False)
        trcr_lt = request.json.get("trcr_lt", False)
        knee_rt = request.json.get("knee_rt", False)
        knee_lt = request.json.get("knee_lt", False)
        ta_rt = request.json.get("ta_rt", False)
        ta_lt = request.json.get("ta_lt", False)
        calf_rt = request.json.get("calf_rt", False)
        calf_lt = request.json.get("calf_lt", False)
        sole_rt = request.json.get("sole_rt", False)
        sole_lt = request.json.get("sole_lt", False)
        # DEFORMITIES
        # joints  #last column is missing from image
        rhand_fl = request.json.get("rhand_fl", False)
        rhand_ex = request.json.get("rhand_ex", False)
        rhand_ab = request.json.get("rhand_ab", False)
        rhand_add = request.json.get("rhand_add", False)
        rhand_slx = request.json.get("rhand_slx", False)
        lhand_fl = request.json.get("lhand_fl", False)
        lhand_ex = request.json.get("lhand_ex", False)
        lhand_ab = request.json.get("lhand_ab", False)
        lhand_add = request.json.get("lhand_add", False)
        lhand_slx = request.json.get("lhand_slx", False)
        rwrist_fl = request.json.get("rwrist_fl", False)
        rwrist_ex = request.json.get("rwrist_ex", False)
        rwrist_ab = request.json.get("rwrist_ab", False)
        rwrist_add = request.json.get("rwrist_add", False)
        rwrist_slx = request.json.get("rwrist_slx", False)
        lwrist_fl = request.json.get("lwrist_fl", False)
        lwrist_ex = request.json.get("lwrist_ex", False)
        lwrist_ab = request.json.get("lwrist_ab", False)
        lwrist_add = request.json.get("lwrist_add", False)
        lwrist_slx = request.json.get("lwrist_slx", False)
        relb_fl = request.json.get("relb_fl", False)
        relb_ex = request.json.get("relb_ex", False)
        relb_ab = request.json.get("relb_ab", False)
        relb_add = request.json.get("relb_add", False)
        relb_slx = request.json.get("relb_slx", False)

        lelb_fl = request.json.get("lelb_fl", False)
        lelb_ex = request.json.get("lelb_ex", False)
        lelb_ab = request.json.get("lelb_ab", False)
        lelb_add = request.json.get("lelb_add", False)
        lelb_slx = request.json.get("lelb_slx", False)

        shrt_fl = request.json.get("shrt_fl", False)
        shrt_ex = request.json.get("shrt_ex", False)
        shrt_ab = request.json.get("shrt_ab", False)
        shrt_add = request.json.get("shrt_add", False)
        shrt_slx = request.json.get("shrt_slx", False)
        shlt_fl = request.json.get("shlt_fl", False)
        shlt_ex = request.json.get("shlt_ex", False)
        shlt_ab = request.json.get("shlt_ab", False)
        shlt_add = request.json.get("shlt_add", False)
        shlt_slx = request.json.get("shlt_slx", False)
        kneert_fl = request.json.get("kneert_fl", False)
        kneert_ex = request.json.get("kneert_ex", False)
        kneert_ab = request.json.get("kneert_ab", False)
        kneert_add = request.json.get("kneert_add", False)
        kneert_slx = request.json.get("kneert_slx", False)
        kneelt_fl = request.json.get("kneelt_fl", False)
        kneelt_ex = request.json.get("kneelt_ex", False)
        kneelt_ab = request.json.get("kneelt_ab", False)
        kneelt_add = request.json.get("kneelt_add", False)
        kneelt_slx = request.json.get("kneelt_slx", False)
        footrt_fl = request.json.get("footrt_fl", False)
        footrt_ex = request.json.get("footrt_ex", False)
        footrt_ab = request.json.get("footrt_ab", False)
        footrt_add = request.json.get("footrt_add", False)
        footrt_slx = request.json.get("footrt_slx", False)
        footlt_fl = request.json.get("footlt_fl", False)
        footlt_ex = request.json.get("footlt_ex", False)
        footlt_ab = request.json.get("footlt_ab", False)
        footlt_add = request.json.get("footlt_add", False)
        footlt_slx = request.json.get("footlt_slx", False)
        # HYPERMOBILITY
        thumb_rt = request.json.get("thumb_rt", False)
        thumb_lt = request.json.get("thumb_lt", False)
        finger_rt = request.json.get("finger_rt", False)
        finger_lt = request.json.get("finger_lt", False)
        palm_rt = request.json.get("palm_rt", False)
        palm_lt = request.json.get("palm_lt", False)
        elbow_rt = request.json.get("elbow_rt", False)
        elbow_lt = request.json.get("elbow_lt", False)
        hy_knee_rt = request.json.get("hy_knee_rt", False)
        hy_knee_lt = request.json.get("hy_knee_lt", False)
        ankle_rt = request.json.get("ankle_rt", False)
        ankle_lt = request.json.get("ankle_lt", False)
        other_rt = request.json.get("other_rt", False)
        other_lt = request.json.get("other_lt", False)
        spine_rt = request.json.get("spine_rt", False)
        spine_lt = request.json.get("spine_lt", False)

        try:
            dbadd = RheumatologyPatientExamination(pid=pid, user_id=user_id, general_condition=general_condition,
                                                   weight=weight, height=height, bmi=bmi, bp=bp, pulse=pulse,
                                                   temp=temp, respiratory_rate=respiratory_rate,
                                                   ex_health_condition=ex_health_condition, ex_remarks=ex_remarks,
                                                   eyes=eyes, skin_morphology=skin_morphology,
                                                   skin_pattern=skin_pattern, skin_color=skin_color,
                                                   distribution=distribution, other_affects=other_affects,
                                                   mucosa=mucosa, hair_loss=hair_loss, dandruff=dandruff, nail=nail,
                                                   lymphnodes=lymphnodes, lymphnodes_number=lymphnodes_number,
                                                   tender=tender, tender_type=tender_type,
                                                   lymphnodes_tender_others=lymphnodes_tender_others,
                                                   vascular=vascular, cns_examination=cns_examination,
                                                   cvs_examination=cvs_examination,
                                                   chest_examination=chest_examination,
                                                   abdomen_examination=abdomen_examination, tm_t1=tm_t1,
                                                   tm_s1=tm_s1, tm_t2=tm_t2, tm_s2=tm_s2, scl_t1=scl_t1,
                                                   scl_s1=scl_s1, scl_t2=scl_t2, scl_s2=scl_s2, acl_t1=acl_t1,
                                                   acl_s1=acl_s1, acl_t2=acl_t2, acl_s2=acl_s2, sh_t1=sh_t1,
                                                   sh_s1=sh_s1, sh_t2=sh_t2, sh_s2=sh_s2, elbow_t1=elbow_t1,
                                                   elbow_s1=elbow_s1, elbow_t2=elbow_t2, elbow_s2=elbow_s2,
                                                   infru_t1=infru_t1, infru_s1=infru_s1, infru_t2=infru_t2,
                                                   infru_s2=infru_s2, cmc1_t1=cmc1_t1, cmc1_s1=cmc1_s1,
                                                   cmc1_t2=cmc1_t2, cmc1_s2=cmc1_s2, wrist_t1=wrist_t1,
                                                   wrist_s1=wrist_s1, wrist_t2=wrist_t2, wrist_s2=wrist_s2,
                                                   dip2_t1=dip2_t1, dip2_s1=dip2_s1, dip2_t2=dip2_t2,
                                                   dip2_s2=dip2_s2, dip3_t1=dip3_t1, dip3_s1=dip3_s1,
                                                   dip3_t2=dip3_t2, dip3_s2=dip3_s2, dip4_t1=dip4_t1,
                                                   dip4_s1=dip4_s1, dip4_t2=dip4_t2, dip4_s2=dip4_s2,
                                                   dip5_t1=dip5_t1, dip5_s1=dip5_s1, dip5_t2=dip5_t2,
                                                   dip5_s2=dip5_s2, ip1_t1=ip1_t1, ip1_s1=ip1_s1, ip1_t2=ip1_t2,
                                                   ip1_s2=ip1_s2, ip2_t1=ip2_t1, ip2_s1=ip2_s1, ip2_t2=ip2_t2,
                                                   ip2_s2=ip2_s2, pip3_t1=pip3_t1, pip3_s1=pip3_s1, pip3_t2=pip3_t2,
                                                   pip3_s2=pip3_s2, pip4_t1=pip4_t1, pip4_s1=pip4_s1,
                                                   pip4_t2=pip4_t2, pip4_s2=pip4_s2, pip5_t1=pip5_t1,
                                                   pip5_s1=pip5_s1, pip5_t2=pip5_t2, pip5_s2=pip5_s2,
                                                   mcp1_t1=mcp1_t1, mcp1_s1=mcp1_s1, mcp1_t2=mcp1_t2,
                                                   mcp1_s2=mcp1_s2, mcp2_t1=mcp2_t1, mcp2_s1=mcp2_s1,
                                                   mcp2_t2=mcp2_t2, mcp2_s2=mcp2_s2, mcp3_t1=mcp3_t1,
                                                   mcp3_s1=mcp3_s1, mcp3_t2=mcp3_t2, mcp3_s2=mcp3_s2,
                                                   mcp4_t1=mcp4_t1, mcp4_s1=mcp4_s1, mcp4_t2=mcp4_t2,
                                                   mcp4_s2=mcp4_s2, mcp5_t1=mcp5_t1, mcp5_s1=mcp5_s1,
                                                   mcp5_t2=mcp5_t2, mcp5_s2=mcp5_s2, hip_t1=hip_t1, hip_s1=hip_s1,
                                                   hip_t2=hip_t2, hip_s2=hip_s2, knee_t1=knee_t1, knee_s1=knee_s1,
                                                   knee_t2=knee_t2, knee_s2=knee_s2, a_tt_t1=a_tt_t1,
                                                   a_tt_s1=a_tt_s1, a_tt_t2=a_tt_t2, a_tt_s2=a_tt_s2,
                                                   a_tc_t1=a_tc_t1, a_tc_s1=a_tc_s1, a_tc_t2=a_tc_t2,
                                                   a_tc_s2=a_tc_s2, mtl_t1=mtl_t1, mtl_s1=mtl_s1, mtl_t2=mtl_t2,
                                                   mtl_s2=mtl_s2, mtp1_t1=mtp1_t1, mtp1_s1=mtp1_s1, mtp1_t2=mtp1_t2,
                                                   mtp1_s2=mtp1_s2, mtp2_t1=mtp2_t1, mtp2_s1=mtp2_s1,
                                                   mtp2_t2=mtp2_t2, mtp2_s2=mtp2_s2, mtp3_t1=mtp3_t1,
                                                   mtp3_s1=mtp3_s1, mtp3_t2=mtp3_t2, mtp3_s2=mtp3_s2,
                                                   mtp4_t1=mtp4_t1, mtp4_s1=mtp4_s1, mtp4_t2=mtp4_t2,
                                                   mtp4_s2=mtp4_s2, mtp5_t1=mtp5_t1, mtp5_s1=mtp5_s1,
                                                   mtp5_t2=mtp5_t2, mtp5_s2=mtp5_s2, ip1b_t1=ip1b_t1,
                                                   ip1b_s1=ip1b_s1, ip1b_t2=ip1b_t2, ip1b_s2=ip1b_s2,
                                                   ip2b_t1=ip2b_t1, ip2b_s1=ip2b_s1, ip2b_t2=ip2b_t2,
                                                   ip2b_s2=ip2b_s2, ip3b_t1=ip3b_t1, ip3b_s1=ip3b_s1,
                                                   ip3b_t2=ip3b_t2, ip3b_s2=ip3b_s2, ip4b_t1=ip4b_t1,
                                                   ip4b_s1=ip4b_s1, ip4b_t2=ip4b_t2, ip4b_s2=ip4b_s2,
                                                   ip5b_t1=ip5b_t1, ip5b_s1=ip5b_s1, ip5b_t2=ip5b_t2,
                                                   ip5b_s2=ip5b_s2, s1_t1=s1_t1, s1_s1=s1_s1, s1_t2=s1_t2,
                                                   s1_s2=s1_s2, cervical_anky=cervical_anky,
                                                   cervical_flex=cervical_flex, cervical_ext=cervical_ext,
                                                   cervical_rtrot=cervical_rtrot, cervical_ltrot=cervical_ltrot,
                                                   cervical_rtfl=cervical_rtfl, cervical_ltfl=cervical_ltfl,
                                                   cervical_pt=cervical_pt, thorasic_anky=thorasic_anky,
                                                   thorasic_flex=thorasic_flex, thorasic_ext=thorasic_ext,
                                                   thorasic_rtrot=thorasic_rtrot, thorasic_ltrot=thorasic_ltrot,
                                                   thorasic_rtfl=thorasic_rtfl, thorasic_ltfl=thorasic_ltfl,
                                                   thorasic_pt=thorasic_pt, lumbar_anky=lumbar_anky,
                                                   lumbar_flex=lumbar_flex, lumbar_ext=lumbar_ext,
                                                   lumbar_rtrot=lumbar_rtrot, lumbar_ltrot=lumbar_ltrot,
                                                   lumbar_rtfl=lumbar_rtfl, lumbar_ltfl=lumbar_ltfl,
                                                   lumbar_pt=lumbar_pt, opt_rt=opt_rt, opt_lt=opt_lt,
                                                   lcer_rt=lcer_rt, lcer_lt=lcer_lt, trpz_rt=trpz_rt,
                                                   trpz_lt=trpz_lt, scap_rt=scap_rt, scap_lt=scap_lt,
                                                   zcst_rt=zcst_rt, zcst_lt=zcst_lt, epdl_rt=epdl_rt,
                                                   epdl_lt=epdl_lt, glut_rt=glut_rt, glut_lt=glut_lt,
                                                   trcr_rt=trcr_rt, trcr_lt=trcr_lt, knee_rt=knee_rt,
                                                   knee_lt=knee_lt, ta_rt=ta_rt, ta_lt=ta_lt, calf_rt=calf_rt,
                                                   calf_lt=calf_lt, sole_rt=sole_rt, sole_lt=sole_lt,
                                                   rhand_fl=rhand_fl, rhand_ex=rhand_ex, rhand_ab=rhand_ab,
                                                   rhand_add=rhand_add, rhand_slx=rhand_slx, lhand_fl=lhand_fl,
                                                   lhand_ex=lhand_ex, lhand_ab=lhand_ab, lhand_add=lhand_add,
                                                   lhand_slx=lhand_slx, rwrist_fl=rwrist_fl, rwrist_ex=rwrist_ex,
                                                   rwrist_ab=rwrist_ab, rwrist_add=rwrist_add,
                                                   rwrist_slx=rwrist_slx, lwrist_fl=lwrist_fl, lwrist_ex=lwrist_ex,
                                                   lwrist_ab=lwrist_ab, lwrist_add=lwrist_add,
                                                   lwrist_slx=lwrist_slx, relb_fl=relb_fl, relb_ex=relb_ex,
                                                   relb_ab=relb_ab, relb_add=relb_add, relb_slx=relb_slx,
                                                   lelb_fl=lelb_fl, lelb_ex=lelb_ex, lelb_ab=lelb_ab,
                                                   lelb_add=lelb_add, lelb_slx=lelb_slx, shrt_fl=shrt_fl,
                                                   shrt_ex=shrt_ex, shrt_ab=shrt_ab, shrt_add=shrt_add,
                                                   shrt_slx=shrt_slx, shlt_fl=shlt_fl, shlt_ex=shlt_ex,
                                                   shlt_ab=shlt_ab, shlt_add=shlt_add, shlt_slx=shlt_slx,
                                                   kneert_fl=kneert_fl, kneert_ex=kneert_ex, kneert_ab=kneert_ab,
                                                   kneert_add=kneert_add, kneert_slx=kneert_slx,
                                                   kneelt_fl=kneelt_fl, kneelt_ex=kneelt_ex, kneelt_ab=kneelt_ab,
                                                   kneelt_add=kneelt_add, kneelt_slx=kneelt_slx,
                                                   footrt_fl=footrt_fl, footrt_ex=footrt_ex, footrt_ab=footrt_ab,
                                                   footrt_add=footrt_add, footrt_slx=footrt_slx,
                                                   footlt_fl=footlt_fl, footlt_ex=footlt_ex, footlt_ab=footlt_ab,
                                                   footlt_add=footlt_add, footlt_slx=footlt_slx, thumb_rt=thumb_rt,
                                                   thumb_lt=thumb_lt, finger_rt=finger_rt, finger_lt=finger_lt,
                                                   palm_rt=palm_rt, palm_lt=palm_lt, elbow_rt=elbow_rt,
                                                   elbow_lt=elbow_lt, hy_knee_rt=hy_knee_rt, hy_knee_lt=hy_knee_lt,
                                                   ankle_rt=ankle_rt, ankle_lt=ankle_lt, other_rt=other_rt,
                                                   other_lt=other_lt, spine_rt=spine_rt, spine_lt=spine_lt)
            session.add(dbadd)
            session.commit()
            return "Success"
        except IntegrityError:
            return "Same PID given"

    return "get request made"


@app.route("/updaterheumapatientexamination", methods=['GET', 'POST'])
def updaterheumapatientexamination():
    if request.method == 'POST':
        id = request.json.get("id", False)
        pid = request.json.get("pid", False)
        user_id = request.json.get("user_id", False)
        # EXAMINATION
        general_condition = request.json.get("general_condition", False)
        weight = request.json.get("weight", False)
        height = request.json.get("height", False)
        bmi = request.json.get("bmi", False)
        # vitals
        bp = request.json.get("bp", False)
        pulse = request.json.get("pulse", False)
        temp = request.json.get("temp", False)
        respiratory_rate = request.json.get("respiratory_rate", False)
        ex_health_condition = request.json.get("ex_health_condition", False)
        ex_remarks = request.json.get("ex_remarks", False)
        eyes = request.json.get("eyes", False)
        skin_morphology = request.json.get("skin_morphology", False)
        skin_pattern = request.json.get("skin_pattern", False)
        skin_color = request.json.get("skin_color", False)
        distribution = request.json.get("distribution", False)
        other_affects = request.json.get("other_affects", False)
        mucosa = request.json.get("mucosa", False)
        hair_loss = request.json.get("hair_loss", False)
        dandruff = request.json.get("dandruff", False)
        nail = request.json.get("nail", False)
        lymphnodes = request.json.get("lymphnodes", False)
        lymphnodes_number = request.json.get("lymphnodes_number", False)
        tender = request.json.get("tender", False)
        tender_type = request.json.get("tender_type", False)
        lymphnodes_tender_others = request.json.get("lymphnodes_tender_others", False)
        vascular = request.json.get("vascular", False)
        cns_examination = request.json.get("cns_examination", False)
        cvs_examination = request.json.get("cvs_examination", False)
        chest_examination = request.json.get("chest_examination", False)
        abdomen_examination = request.json.get("abdomen_examination", False)
        # MUSCULOSKELETAL_EXAMINATIONS
        # joint_evaluation:
        # row1
        tm_t1 = request.json.get("tm_t1")
        tm_s1 = request.json.get("tm_s1", False)
        tm_t2 = request.json.get("tm_t2", False)
        tm_s2 = request.json.get("tm_s2", False)
        scl_t1 = request.json.get("scl_t1", False)
        scl_s1 = request.json.get("scl_s1", False)
        scl_t2 = request.json.get("scl_t2", False)
        scl_s2 = request.json.get("scl_s2", False)
        acl_t1 = request.json.get("acl_t1", False)
        acl_s1 = request.json.get("acl_s1", False)
        acl_t2 = request.json.get("acl_t2", False)
        acl_s2 = request.json.get("acl_s2", False)
        sh_t1 = request.json.get("sh_t1", False)
        sh_s1 = request.json.get("sh_s1", False)
        sh_t2 = request.json.get("sh_t2", False)
        sh_s2 = request.json.get("sh_s2", False)
        elbow_t1 = request.json.get("elbow_t1", False)
        elbow_s1 = request.json.get("elbow_s1", False)
        elbow_t2 = request.json.get("elbow_t2", False)
        elbow_s2 = request.json.get("elbow_s2", False)
        infru_t1 = request.json.get("infru_t1", False)
        infru_s1 = request.json.get("infru_s1", False)
        infru_t2 = request.json.get("infru_t2", False)
        infru_s2 = request.json.get("infru_s2", False)
        cmc1_t1 = request.json.get("cmc1_t1", False)
        cmc1_s1 = request.json.get("cmc1_s1", False)
        cmc1_t2 = request.json.get("cmc1_t2", False)
        cmc1_s2 = request.json.get("cmc1_s2", False)
        wrist_t1 = request.json.get("wrist_t1", False)
        wrist_s1 = request.json.get("wrist_s1", False)
        wrist_t2 = request.json.get("wrist_t2", False)
        wrist_s2 = request.json.get("wrist_s2", False)
        dip2_t1 = request.json.get("dip2_t1", False)
        dip2_s1 = request.json.get("dip2_s1", False)
        dip2_t2 = request.json.get("dip2_t2", False)
        dip2_s2 = request.json.get("dip2_s2", False)
        dip3_t1 = request.json.get("dip3_t1", False)
        dip3_s1 = request.json.get("dip3_s1", False)
        dip3_t2 = request.json.get("dip3_t2", False)
        dip3_s2 = request.json.get("dip3_s2", False)
        dip4_t1 = request.json.get("dip4_t1", False)
        dip4_s1 = request.json.get("dip4_s1", False)
        dip4_t2 = request.json.get("dip4_t2", False)
        dip4_s2 = request.json.get("dip4_s2", False)
        dip5_t1 = request.json.get("dip5_t1", False)
        dip5_s1 = request.json.get("dip5_s1", False)
        dip5_t2 = request.json.get("dip5_t2", False)
        dip5_s2 = request.json.get("dip5_s2", False)
        ip1_t1 = request.json.get("ip1_t1", False)
        ip1_s1 = request.json.get("ip1_s1", False)
        ip1_t2 = request.json.get("ip1_t2", False)
        ip1_s2 = request.json.get("ip1_s2", False)
        ip2_t1 = request.json.get("ip2_t1", False)
        ip2_s1 = request.json.get("ip2_s1", False)
        ip2_t2 = request.json.get("ip2_t2", False)
        ip2_s2 = request.json.get("ip2_s2", False)
        pip3_t1 = request.json.get("pip3_t1", False)
        pip3_s1 = request.json.get("pip3_s1", False)
        pip3_t2 = request.json.get("pip3_t2", False)
        pip3_s2 = request.json.get("pip3_s2", False)
        pip4_t1 = request.json.get("pip4_t1", False)
        pip4_s1 = request.json.get("pip4_s1", False)
        pip4_t2 = request.json.get("pip4_t2", False)
        pip4_s2 = request.json.get("pip4_s2", False)
        pip5_t1 = request.json.get("pip5_t1", False)
        pip5_s1 = request.json.get("pip5_s1", False)
        pip5_t2 = request.json.get("pip5_t2", False)
        pip5_s2 = request.json.get("pip5_s2", False)
        mcp1_t1 = request.json.get("mcp1_t1", False)
        mcp1_s1 = request.json.get("mcp1_s1", False)
        mcp1_t2 = request.json.get("mcp1_t2", False)
        mcp1_s2 = request.json.get("mcp1_s2", False)
        mcp2_t1 = request.json.get("mcp2_t1", False)
        mcp2_s1 = request.json.get("mcp2_s1", False)
        mcp2_t2 = request.json.get("mcp2_t2", False)
        mcp2_s2 = request.json.get("mcp2_s2", False)

        # row2
        mcp3_t1 = request.json.get("mcp3_t1", False)
        mcp3_s1 = request.json.get("mcp3_s1", False)
        mcp3_t2 = request.json.get("mcp3_t2", False)
        mcp3_s2 = request.json.get("mcp3_s2", False)
        mcp4_t1 = request.json.get("mcp4_t1", False)
        mcp4_s1 = request.json.get("mcp4_s1", False)
        mcp4_t2 = request.json.get("mcp4_t2", False)
        mcp4_s2 = request.json.get("mcp4_s2", False)
        mcp5_t1 = request.json.get("mcp5_t1", False)
        mcp5_s1 = request.json.get("mcp5_s1", False)
        mcp5_t2 = request.json.get("mcp5_t2", False)
        mcp5_s2 = request.json.get("mcp5_s2", False)
        hip_t1 = request.json.get("hip_t1", False)
        hip_s1 = request.json.get("hip_s1", False)
        hip_t2 = request.json.get("hip_t2", False)
        hip_s2 = request.json.get("hip_s2", False)
        knee_t1 = request.json.get("knee_t1", False)
        knee_s1 = request.json.get("knee_s1", False)
        knee_t2 = request.json.get("knee_t2", False)
        knee_s2 = request.json.get("knee_s2", False)
        a_tt_t1 = request.json.get("a_tt_t1", False)
        a_tt_s1 = request.json.get("a_tt_s1", False)
        a_tt_t2 = request.json.get("a_tt_t2", False)
        a_tt_s2 = request.json.get("a_tt_s2", False)
        a_tc_t1 = request.json.get("a_tc_t1", False)
        a_tc_s1 = request.json.get("a_tc_s1", False)
        a_tc_t2 = request.json.get("a_tc_t2", False)
        a_tc_s2 = request.json.get("a_tc_s2", False)
        mtl_t1 = request.json.get("mtl_t1", False)
        mtl_s1 = request.json.get("mtl_s1", False)
        mtl_t2 = request.json.get("mtl_t2", False)
        mtl_s2 = request.json.get("mtl_s2", False)
        mtp1_t1 = request.json.get("mtp1_t1", False)
        mtp1_s1 = request.json.get("mtp1_s1", False)
        mtp1_t2 = request.json.get("mtp1_t2", False)
        mtp1_s2 = request.json.get("mtp1_s2", False)
        mtp2_t1 = request.json.get("mtp2_t1", False)
        mtp2_s1 = request.json.get("mtp2_s1", False)
        mtp2_t2 = request.json.get("mtp2_t2", False)
        mtp2_s2 = request.json.get("mtp2_s2", False)
        mtp3_t1 = request.json.get("mtp3_t1", False)
        mtp3_s1 = request.json.get("mtp3_s1", False)
        mtp3_t2 = request.json.get("mtp3_t2", False)
        mtp3_s2 = request.json.get("mtp3_s2", False)
        mtp4_t1 = request.json.get("mtp4_t1", False)
        mtp4_s1 = request.json.get("mtp4_s1", False)
        mtp4_t2 = request.json.get("mtp4_t2", False)
        mtp4_s2 = request.json.get("mtp4_s2", False)
        mtp5_t1 = request.json.get("mtp5_t1", False)
        mtp5_s1 = request.json.get("mtp5_s1", False)
        mtp5_t2 = request.json.get("mtp5_t2", False)
        mtp5_s2 = request.json.get("mtp5_s2", False)
        ip1b_t1 = request.json.get("ip1b_t1", False)
        ip1b_s1 = request.json.get("ip1b_s1", False)
        ip1b_t2 = request.json.get("ip1b_t2", False)
        ip1b_s2 = request.json.get("ip1b_s2", False)
        ip2b_t1 = request.json.get("ip2b_t1", False)
        ip2b_s1 = request.json.get("ip2b_s1", False)
        ip2b_t2 = request.json.get("ip2b_t2", False)
        ip2b_s2 = request.json.get("ip2b_s2", False)
        ip3b_t1 = request.json.get("ip3b_t1", False)
        ip3b_s1 = request.json.get("ip3b_s1", False)
        ip3b_t2 = request.json.get("ip3b_t2", False)
        ip3b_s2 = request.json.get("ip3b_s2", False)
        ip4b_t1 = request.json.get("ip4b_t1", False)
        ip4b_s1 = request.json.get("ip4b_s1", False)
        ip4b_t2 = request.json.get("ip4b_t2", False)
        ip4b_s2 = request.json.get("ip4b_s2", False)
        ip5b_t1 = request.json.get("ip5b_t1", False)
        ip5b_s1 = request.json.get("ip5b_s1", False)
        ip5b_t2 = request.json.get("ip5b_t2", False)
        ip5b_s2 = request.json.get("ip5b_s2", False)
        s1_t1 = request.json.get("s1_t1", False)
        s1_s1 = request.json.get("s1_s1", False)
        s1_t2 = request.json.get("s1_t2", False)
        s1_s2 = request.json.get("s1_s2", False)
        # ROM
        cervical_anky = request.json.get("cervical_anky", False)
        cervical_flex = request.json.get("cervical_flex", False)
        cervical_ext = request.json.get("cervical_ext", False)
        cervical_rtrot = request.json.get("cervical_rtrot", False)
        cervical_ltrot = request.json.get("cervical_ltrot", False)
        cervical_rtfl = request.json.get("cervical_rtfl", False)
        cervical_ltfl = request.json.get("cervical_ltfl", False)
        cervical_pt = request.json.get("cervical_pt", False)
        thorasic_anky = request.json.get("thorasic_anky", False)
        thorasic_flex = request.json.get("thorasic_flex", False)
        thorasic_ext = request.json.get("thorasic_ext", False)
        thorasic_rtrot = request.json.get("thorasic_rtrot", False)
        thorasic_ltrot = request.json.get("thorasic_ltrot", False)
        thorasic_rtfl = request.json.get("thorasic_rtfl", False)
        thorasic_ltfl = request.json.get("thorasic_ltfl", False)
        thorasic_pt = request.json.get("thorasic_pt", False)
        lumbar_anky = request.json.get("lumbar_anky", False)
        lumbar_flex = request.json.get("lumbar_flex", False)
        lumbar_ext = request.json.get("lumbar_ext", False)
        lumbar_rtrot = request.json.get("lumbar_rtrot", False)
        lumbar_ltrot = request.json.get("lumbar_ltrot", False)
        lumbar_rtfl = request.json.get("lumbar_rtfl", False)
        lumbar_ltfl = request.json.get("lumbar_ltfl", False)
        lumbar_pt = request.json.get("lumbar_pt", False)
        # SOFT TISSUE RHEUMATISM
        opt_rt = request.json.get("opt_rt", False)
        opt_lt = request.json.get("opt_lt", False)
        lcer_rt = request.json.get("lcer_rt", False)
        lcer_lt = request.json.get("lcer_lt", False)
        trpz_rt = request.json.get("trpz_rt", False)
        trpz_lt = request.json.get("trpz_lt", False)
        scap_rt = request.json.get("scap_rt", False)
        scap_lt = request.json.get("scap_lt", False)
        zcst_rt = request.json.get("zcst_rt", False)
        zcst_lt = request.json.get("zcst_lt", False)
        epdl_rt = request.json.get("epdl_rt", False)
        epdl_lt = request.json.get("epdl_lt", False)
        glut_rt = request.json.get("glut_rt", False)
        glut_lt = request.json.get("glut_lt", False)
        trcr_rt = request.json.get("trcr_rt", False)
        trcr_lt = request.json.get("trcr_lt", False)
        knee_rt = request.json.get("knee_rt", False)
        knee_lt = request.json.get("knee_lt", False)
        ta_rt = request.json.get("ta_rt", False)
        ta_lt = request.json.get("ta_lt", False)
        calf_rt = request.json.get("calf_rt", False)
        calf_lt = request.json.get("calf_lt", False)
        sole_rt = request.json.get("sole_rt", False)
        sole_lt = request.json.get("sole_lt", False)
        # DEFORMITIES
        # joints  #last column is missing from image
        rhand_fl = request.json.get("rhand_fl", False)
        rhand_ex = request.json.get("rhand_ex", False)
        rhand_ab = request.json.get("rhand_ab", False)
        rhand_add = request.json.get("rhand_add", False)
        rhand_slx = request.json.get("rhand_slx", False)
        lhand_fl = request.json.get("lhand_fl", False)
        lhand_ex = request.json.get("lhand_ex", False)
        lhand_ab = request.json.get("lhand_ab", False)
        lhand_add = request.json.get("lhand_add", False)
        lhand_slx = request.json.get("lhand_slx", False)
        rwrist_fl = request.json.get("rwrist_fl", False)
        rwrist_ex = request.json.get("rwrist_ex", False)
        rwrist_ab = request.json.get("rwrist_ab", False)
        rwrist_add = request.json.get("rwrist_add", False)
        rwrist_slx = request.json.get("rwrist_slx", False)
        lwrist_fl = request.json.get("lwrist_fl", False)
        lwrist_ex = request.json.get("lwrist_ex", False)
        lwrist_ab = request.json.get("lwrist_ab", False)
        lwrist_add = request.json.get("lwrist_add", False)
        lwrist_slx = request.json.get("lwrist_slx", False)
        relb_fl = request.json.get("relb_fl", False)
        relb_ex = request.json.get("relb_ex", False)
        relb_ab = request.json.get("relb_ab", False)
        relb_add = request.json.get("relb_add", False)
        relb_slx = request.json.get("relb_slx", False)

        lelb_fl = request.json.get("lelb_fl", False)
        lelb_ex = request.json.get("lelb_ex", False)
        lelb_ab = request.json.get("lelb_ab", False)
        lelb_add = request.json.get("lelb_add", False)
        lelb_slx = request.json.get("lelb_slx", False)

        shrt_fl = request.json.get("shrt_fl", False)
        shrt_ex = request.json.get("shrt_ex", False)
        shrt_ab = request.json.get("shrt_ab", False)
        shrt_add = request.json.get("shrt_add", False)
        shrt_slx = request.json.get("shrt_slx", False)
        shlt_fl = request.json.get("shlt_fl", False)
        shlt_ex = request.json.get("shlt_ex", False)
        shlt_ab = request.json.get("shlt_ab", False)
        shlt_add = request.json.get("shlt_add", False)
        shlt_slx = request.json.get("shlt_slx", False)
        kneert_fl = request.json.get("kneert_fl", False)
        kneert_ex = request.json.get("kneert_ex", False)
        kneert_ab = request.json.get("kneert_ab", False)
        kneert_add = request.json.get("kneert_add", False)
        kneert_slx = request.json.get("kneert_slx", False)
        kneelt_fl = request.json.get("kneelt_fl", False)
        kneelt_ex = request.json.get("kneelt_ex", False)
        kneelt_ab = request.json.get("kneelt_ab", False)
        kneelt_add = request.json.get("kneelt_add", False)
        kneelt_slx = request.json.get("kneelt_slx", False)
        footrt_fl = request.json.get("footrt_fl", False)
        footrt_ex = request.json.get("footrt_ex", False)
        footrt_ab = request.json.get("footrt_ab", False)
        footrt_add = request.json.get("footrt_add", False)
        footrt_slx = request.json.get("footrt_slx", False)
        footlt_fl = request.json.get("footlt_fl", False)
        footlt_ex = request.json.get("footlt_ex", False)
        footlt_ab = request.json.get("footlt_ab", False)
        footlt_add = request.json.get("footlt_add", False)
        footlt_slx = request.json.get("footlt_slx", False)
        # HYPERMOBILITY
        thumb_rt = request.json.get("thumb_rt", False)
        thumb_lt = request.json.get("thumb_lt", False)
        finger_rt = request.json.get("finger_rt", False)
        finger_lt = request.json.get("finger_lt", False)
        palm_rt = request.json.get("palm_rt", False)
        palm_lt = request.json.get("palm_lt", False)
        elbow_rt = request.json.get("elbow_rt", False)
        elbow_lt = request.json.get("elbow_lt", False)
        hy_knee_rt = request.json.get("hy_knee_rt", False)
        hy_knee_lt = request.json.get("hy_knee_lt", False)
        ankle_rt = request.json.get("ankle_rt", False)
        ankle_lt = request.json.get("ankle_lt", False)
        other_rt = request.json.get("other_rt", False)
        other_lt = request.json.get("other_lt", False)
        spine_rt = request.json.get("spine_rt", False)
        spine_lt = request.json.get("spine_lt", False)

        try:
            dbedit = session.query(RheumatologyPatientExamination).filter_by(id=id).update(
                dict(pid=pid, user_id=user_id, general_condition=general_condition, weight=weight, height=height,
                     bmi=bmi, bp=bp, pulse=pulse, temp=temp, respiratory_rate=respiratory_rate,
                     ex_health_condition=ex_health_condition, ex_remarks=ex_remarks, eyes=eyes,
                     skin_morphology=skin_morphology, skin_pattern=skin_pattern, skin_color=skin_color,
                     distribution=distribution, other_affects=other_affects, mucosa=mucosa, hair_loss=hair_loss,
                     dandruff=dandruff, nail=nail, lymphnodes=lymphnodes, lymphnodes_number=lymphnodes_number,
                     tender=tender, tender_type=tender_type, lymphnodes_tender_others=lymphnodes_tender_others,
                     vascular=vascular, cns_examination=cns_examination, cvs_examination=cvs_examination,
                     chest_examination=chest_examination, abdomen_examination=abdomen_examination, tm_t1=tm_t1,
                     tm_s1=tm_s1, tm_t2=tm_t2, tm_s2=tm_s2, scl_t1=scl_t1, scl_s1=scl_s1, scl_t2=scl_t2,
                     scl_s2=scl_s2, acl_t1=acl_t1, acl_s1=acl_s1, acl_t2=acl_t2, acl_s2=acl_s2, sh_t1=sh_t1,
                     sh_s1=sh_s1, sh_t2=sh_t2, sh_s2=sh_s2, elbow_t1=elbow_t1, elbow_s1=elbow_s1, elbow_t2=elbow_t2,
                     elbow_s2=elbow_s2, infru_t1=infru_t1, infru_s1=infru_s1, infru_t2=infru_t2, infru_s2=infru_s2,
                     cmc1_t1=cmc1_t1, cmc1_s1=cmc1_s1, cmc1_t2=cmc1_t2, cmc1_s2=cmc1_s2, wrist_t1=wrist_t1,
                     wrist_s1=wrist_s1, wrist_t2=wrist_t2, wrist_s2=wrist_s2, dip2_t1=dip2_t1, dip2_s1=dip2_s1,
                     dip2_t2=dip2_t2, dip2_s2=dip2_s2, dip3_t1=dip3_t1, dip3_s1=dip3_s1, dip3_t2=dip3_t2,
                     dip3_s2=dip3_s2, dip4_t1=dip4_t1, dip4_s1=dip4_s1, dip4_t2=dip4_t2, dip4_s2=dip4_s2,
                     dip5_t1=dip5_t1, dip5_s1=dip5_s1, dip5_t2=dip5_t2, dip5_s2=dip5_s2, ip1_t1=ip1_t1,
                     ip1_s1=ip1_s1, ip1_t2=ip1_t2, ip1_s2=ip1_s2, ip2_t1=ip2_t1, ip2_s1=ip2_s1, ip2_t2=ip2_t2,
                     ip2_s2=ip2_s2, pip3_t1=pip3_t1, pip3_s1=pip3_s1, pip3_t2=pip3_t2, pip3_s2=pip3_s2,
                     pip4_t1=pip4_t1, pip4_s1=pip4_s1, pip4_t2=pip4_t2, pip4_s2=pip4_s2, pip5_t1=pip5_t1,
                     pip5_s1=pip5_s1, pip5_t2=pip5_t2, pip5_s2=pip5_s2, mcp1_t1=mcp1_t1, mcp1_s1=mcp1_s1,
                     mcp1_t2=mcp1_t2, mcp1_s2=mcp1_s2, mcp2_t1=mcp2_t1, mcp2_s1=mcp2_s1, mcp2_t2=mcp2_t2,
                     mcp2_s2=mcp2_s2, mcp3_t1=mcp3_t1, mcp3_s1=mcp3_s1, mcp3_t2=mcp3_t2, mcp3_s2=mcp3_s2,
                     mcp4_t1=mcp4_t1, mcp4_s1=mcp4_s1, mcp4_t2=mcp4_t2, mcp4_s2=mcp4_s2, mcp5_t1=mcp5_t1,
                     mcp5_s1=mcp5_s1, mcp5_t2=mcp5_t2, mcp5_s2=mcp5_s2, hip_t1=hip_t1, hip_s1=hip_s1, hip_t2=hip_t2,
                     hip_s2=hip_s2, knee_t1=knee_t1, knee_s1=knee_s1, knee_t2=knee_t2, knee_s2=knee_s2,
                     a_tt_t1=a_tt_t1, a_tt_s1=a_tt_s1, a_tt_t2=a_tt_t2, a_tt_s2=a_tt_s2, a_tc_t1=a_tc_t1,
                     a_tc_s1=a_tc_s1, a_tc_t2=a_tc_t2, a_tc_s2=a_tc_s2, mtl_t1=mtl_t1, mtl_s1=mtl_s1, mtl_t2=mtl_t2,
                     mtl_s2=mtl_s2, mtp1_t1=mtp1_t1, mtp1_s1=mtp1_s1, mtp1_t2=mtp1_t2, mtp1_s2=mtp1_s2,
                     mtp2_t1=mtp2_t1, mtp2_s1=mtp2_s1, mtp2_t2=mtp2_t2, mtp2_s2=mtp2_s2, mtp3_t1=mtp3_t1,
                     mtp3_s1=mtp3_s1, mtp3_t2=mtp3_t2, mtp3_s2=mtp3_s2, mtp4_t1=mtp4_t1, mtp4_s1=mtp4_s1,
                     mtp4_t2=mtp4_t2, mtp4_s2=mtp4_s2, mtp5_t1=mtp5_t1, mtp5_s1=mtp5_s1, mtp5_t2=mtp5_t2,
                     mtp5_s2=mtp5_s2, ip1b_t1=ip1b_t1, ip1b_s1=ip1b_s1, ip1b_t2=ip1b_t2, ip1b_s2=ip1b_s2,
                     ip2b_t1=ip2b_t1, ip2b_s1=ip2b_s1, ip2b_t2=ip2b_t2, ip2b_s2=ip2b_s2, ip3b_t1=ip3b_t1,
                     ip3b_s1=ip3b_s1, ip3b_t2=ip3b_t2, ip3b_s2=ip3b_s2, ip4b_t1=ip4b_t1, ip4b_s1=ip4b_s1,
                     ip4b_t2=ip4b_t2, ip4b_s2=ip4b_s2, ip5b_t1=ip5b_t1, ip5b_s1=ip5b_s1, ip5b_t2=ip5b_t2,
                     ip5b_s2=ip5b_s2, s1_t1=s1_t1, s1_s1=s1_s1, s1_t2=s1_t2, s1_s2=s1_s2,
                     cervical_anky=cervical_anky, cervical_flex=cervical_flex, cervical_ext=cervical_ext,
                     cervical_rtrot=cervical_rtrot, cervical_ltrot=cervical_ltrot, cervical_rtfl=cervical_rtfl,
                     cervical_ltfl=cervical_ltfl, cervical_pt=cervical_pt, thorasic_anky=thorasic_anky,
                     thorasic_flex=thorasic_flex, thorasic_ext=thorasic_ext, thorasic_rtrot=thorasic_rtrot,
                     thorasic_ltrot=thorasic_ltrot, thorasic_rtfl=thorasic_rtfl, thorasic_ltfl=thorasic_ltfl,
                     thorasic_pt=thorasic_pt, lumbar_anky=lumbar_anky, lumbar_flex=lumbar_flex,
                     lumbar_ext=lumbar_ext, lumbar_rtrot=lumbar_rtrot, lumbar_ltrot=lumbar_ltrot,
                     lumbar_rtfl=lumbar_rtfl, lumbar_ltfl=lumbar_ltfl, lumbar_pt=lumbar_pt, opt_rt=opt_rt,
                     opt_lt=opt_lt, lcer_rt=lcer_rt, lcer_lt=lcer_lt, trpz_rt=trpz_rt, trpz_lt=trpz_lt,
                     scap_rt=scap_rt, scap_lt=scap_lt, zcst_rt=zcst_rt, zcst_lt=zcst_lt, epdl_rt=epdl_rt,
                     epdl_lt=epdl_lt, glut_rt=glut_rt, glut_lt=glut_lt, trcr_rt=trcr_rt, trcr_lt=trcr_lt,
                     knee_rt=knee_rt, knee_lt=knee_lt, ta_rt=ta_rt, ta_lt=ta_lt, calf_rt=calf_rt, calf_lt=calf_lt,
                     sole_rt=sole_rt, sole_lt=sole_lt, rhand_fl=rhand_fl, rhand_ex=rhand_ex, rhand_ab=rhand_ab,
                     rhand_add=rhand_add, rhand_slx=rhand_slx, lhand_fl=lhand_fl, lhand_ex=lhand_ex,
                     lhand_ab=lhand_ab, lhand_add=lhand_add, lhand_slx=lhand_slx, rwrist_fl=rwrist_fl,
                     rwrist_ex=rwrist_ex, rwrist_ab=rwrist_ab, rwrist_add=rwrist_add, rwrist_slx=rwrist_slx,
                     lwrist_fl=lwrist_fl, lwrist_ex=lwrist_ex, lwrist_ab=lwrist_ab, lwrist_add=lwrist_add,
                     lwrist_slx=lwrist_slx, relb_fl=relb_fl, relb_ex=relb_ex, relb_ab=relb_ab, relb_add=relb_add,
                     relb_slx=relb_slx, lelb_fl=lelb_fl, lelb_ex=lelb_ex, lelb_ab=lelb_ab, lelb_add=lelb_add,
                     lelb_slx=lelb_slx, shrt_fl=shrt_fl, shrt_ex=shrt_ex, shrt_ab=shrt_ab, shrt_add=shrt_add,
                     shrt_slx=shrt_slx, shlt_fl=shlt_fl, shlt_ex=shlt_ex, shlt_ab=shlt_ab, shlt_add=shlt_add,
                     shlt_slx=shlt_slx, kneert_fl=kneert_fl, kneert_ex=kneert_ex, kneert_ab=kneert_ab,
                     kneert_add=kneert_add, kneert_slx=kneert_slx, kneelt_fl=kneelt_fl, kneelt_ex=kneelt_ex,
                     kneelt_ab=kneelt_ab, kneelt_add=kneelt_add, kneelt_slx=kneelt_slx, footrt_fl=footrt_fl,
                     footrt_ex=footrt_ex, footrt_ab=footrt_ab, footrt_add=footrt_add, footrt_slx=footrt_slx,
                     footlt_fl=footlt_fl, footlt_ex=footlt_ex, footlt_ab=footlt_ab, footlt_add=footlt_add,
                     footlt_slx=footlt_slx, thumb_rt=thumb_rt, thumb_lt=thumb_lt, finger_rt=finger_rt,
                     finger_lt=finger_lt, palm_rt=palm_rt, palm_lt=palm_lt, elbow_rt=elbow_rt, elbow_lt=elbow_lt,
                     hy_knee_rt=hy_knee_rt, hy_knee_lt=hy_knee_lt, ankle_rt=ankle_rt, ankle_lt=ankle_lt,
                     other_rt=other_rt, other_lt=other_lt, spine_rt=spine_rt, spine_lt=spine_lt))
            session.commit()
            # sending back updated data of the user
            patientdata = session.query(RheumatologyPatientExamination).filter_by(pid=pid).first()
            session.commit()
            patientschema = RheumatologyPatientExaminationSchema()
            output = patientschema.dump(patientdata)
            return jsonify({'RheumatologyPatientExamination': output})
        except:
            return "Fail"
    return "get request made"

    # Rheumatology Investigation starts here
    # view details


@app.route("/rheumapatientinvestigationdetails", methods=['GET', 'POST'])
def rheumapatientinveestigationdetails():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).first()
        session.commit()
        RheumatologyPSchema = RheumatologyPatientInvestigationSchema()
        output = RheumatologyPSchema.dump(RheumatologyPat)
        return jsonify({'RheumatologyPatientInvestigation': output})
    except:
        return "Wrong Pid"


# deleteDetails
@app.route("/rheumapatientinvestigationdelete", methods=['GET', 'POST'])
def rheumapatientinvestigationdelete():
    global name
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).first()
        if RheumatologyPat:
            session.delete(RheumatologyPat)
            session.commit()
            return "Success", 201
        return "wrong pid"
    except:
        return "wrong pid"


@app.route("/addrheumapatientinvestigation", methods=['GET', 'POST'])
def addrheumapatientinvestigation():
    if request.method == 'POST':
        pid = request.json.get("pid", False)
        user_id = request.json.get("user_id", False)
        peid = pid
        # INVESTIGATIONS #table
        hematological_date = request.json.get("hematological_date", False)
        hem_esr = request.json.get("hem_esr", False)
        hem_hb = request.json.get("hem_hb", False)
        hem_tlc = request.json.get("hem_tlc", False)
        hem_pleb = request.json.get("hem_pleb", False)
        hem_plat = request.json.get("hem_plat", False)
        hem_urine = request.json.get("hem_urine", False)
        hem_others = request.json.get("hem_others", False)
        # 2column investigation
        hematological_date2 = request.json.get("hematological_date2", False)
        hem_esr2 = request.json.get("hem_esr2", False)
        hem_hb2 = request.json.get("hem_hb2", False)
        hem_tlc2 = request.json.get("hem_tlc2", False)
        hem_pleb2 = request.json.get("hem_pleb2", False)
        hem_plat2 = request.json.get("hem_plat2", False)
        hem_urine2 = request.json.get("hem_urine2", False)
        hem_others2 = request.json.get("hem_others2", False)
        # 3column investigation
        hematological_date3 = request.json.get("hematological_date3", False)
        hem_esr3 = request.json.get("hem_esr3", False)
        hem_hb3 = request.json.get("hem_hb3", False)
        hem_tlc3 = request.json.get("hem_tlc3", False)
        hem_pleb3 = request.json.get("hem_pleb3", False)
        hem_plat3 = request.json.get("hem_plat3", False)
        hem_urine3 = request.json.get("hem_urine3", False)
        hem_others3 = request.json.get("hem_others3", False)
        # 4column investigation
        hematological_date4 = request.json.get("hematological_date4", False)
        hem_esr4 = request.json.get("hem_esr4", False)
        hem_hb4 = request.json.get("hem_hb4", False)
        hem_tlc4 = request.json.get("hem_tlc4", False)
        hem_pleb4 = request.json.get("hem_pleb4", False)
        hem_plat4 = request.json.get("hem_plat4", False)
        hem_urine4 = request.json.get("hem_urine4", False)
        hem_others4 = request.json.get("hem_others4", False)
        # 5column investigation
        hematological_date5 = request.json.get("hematological_date5", False)
        hem_esr5 = request.json.get("hem_esr5", False)
        hem_hb5 = request.json.get("hem_hb5", False)
        hem_tlc5 = request.json.get("hem_tlc5", False)
        hem_pleb5 = request.json.get("hem_pleb5", False)
        hem_plat5 = request.json.get("hem_plat5", False)
        hem_urine5 = request.json.get("hem_urine5", False)
        hem_others5 = request.json.get("hem_others5", False)
        # table2
        biochemical_date = request.json.get("biochemical_date", False)
        bio_lft = request.json.get("bio_lft", False)
        bio_rft = request.json.get("bio_rft", False)
        bio_crp = request.json.get("bio_crp", False)
        bio_bsl = request.json.get("bio_bsl", False)
        bio_ua = request.json.get("bio_ua", False)
        bio_others = request.json.get("bio_others", False)
        # 2column investigation biochemical
        biochemical_date2 = request.json.get("biochemical_date2", False)
        bio_lft2 = request.json.get("bio_lft2", False)
        bio_rft2 = request.json.get("bio_rft2", False)
        bio_crp2 = request.json.get("bio_crp2", False)
        bio_bsl2 = request.json.get("bio_bsl2", False)
        bio_ua2 = request.json.get("bio_ua2", False)
        bio_others2 = request.json.get("bio_others2", False)
        # 3 column investigation biochemical
        biochemical_date3 = request.json.get("biochemical_date3", False)
        bio_lft3 = request.json.get("bio_lft3", False)
        bio_rft3 = request.json.get("bio_rft3", False)
        bio_crp3 = request.json.get("bio_crp3", False)
        bio_bsl3 = request.json.get("bio_bsl3", False)
        bio_ua3 = request.json.get("bio_ua3", False)
        bio_others3 = request.json.get("bio_others3", False)
        # 4 column investigation biochemical
        biochemical_date4 = request.json.get("biochemical_date4", False)
        bio_lft4 = request.json.get("bio_lft4", False)
        bio_rft4 = request.json.get("bio_rft4", False)
        bio_crp4 = request.json.get("bio_crp4", False)
        bio_bsl4 = request.json.get("bio_bsl4", False)
        bio_ua4 = request.json.get("bio_ua4", False)
        bio_others4 = request.json.get("bio_others4", False)
        # 5 column investigation biochemical
        biochemical_date5 = request.json.get("biochemical_date5", False)
        bio_lft5 = request.json.get("bio_lft5", False)
        bio_rft5 = request.json.get("bio_rft5", False)
        bio_crp5 = request.json.get("bio_crp5", False)
        bio_bsl5 = request.json.get("bio_bsl5", False)
        bio_ua5 = request.json.get("bio_ua5", False)
        bio_others5 = request.json.get("bio_others5", False)

        # table3
        immunological_date = request.json.get("immunological_date", False)
        imm_rf = request.json.get("imm_rf", False)
        imm_accp = request.json.get("imm_accp", False)
        imm_ana = request.json.get("imm_ana", False)
        imm_anawb = request.json.get("imm_anawb", False)
        imm_ace = request.json.get("imm_ace", False)
        imm_dsdna = request.json.get("imm_dsdna", False)
        imm_hlab27 = request.json.get("imm_hlab27", False)
        imm_c3 = request.json.get("imm_c3", False)
        imm_c4 = request.json.get("imm_c4", False)
        imm_others = request.json.get("imm_others", False)
        # 2 column investigation immunological
        immunological_date2 = request.json.get("immunological_date2", False)
        imm_rf2 = request.json.get("imm_rf2", False)
        imm_accp2 = request.json.get("imm_accp2", False)
        imm_ana2 = request.json.get("imm_ana2", False)
        imm_anawb2 = request.json.get("imm_anawb2", False)
        imm_ace2 = request.json.get("imm_ace2", False)
        imm_dsdna2 = request.json.get("imm_dsdna2", False)
        imm_hlab272 = request.json.get("imm_hlab272", False)
        imm_c32 = request.json.get("imm_c32", False)
        imm_c42 = request.json.get("imm_c42", False)
        imm_others2 = request.json.get("imm_others2", False)

        # 3 column investigation immunological
        immunological_date3 = request.json.get("immunological_date3", False)
        imm_rf3 = request.json.get("imm_rf3", False)
        imm_accp3 = request.json.get("imm_accp3", False)
        imm_ana3 = request.json.get("imm_ana3", False)
        imm_anawb3 = request.json.get("imm_anawb3", False)
        imm_ace3 = request.json.get("imm_ace3", False)
        imm_dsdna3 = request.json.get("imm_dsdna3", False)
        imm_hlab273 = request.json.get("imm_hlab273", False)
        imm_c33 = request.json.get("imm_c33", False)
        imm_c43 = request.json.get("imm_c43", False)
        imm_others3 = request.json.get("imm_others3", False)

        # Radiological
        usg = request.json.get("usg", False)
        xray_chest = request.json.get("xray_chest", False)
        xray_joints = request.json.get("xray_joints", False)
        mri = request.json.get("mri", False)
        radio_remarks = request.json.get("radio_remarks", False)

        # Disease activity indices
        das28 = request.json.get("das28", False)
        sledai = request.json.get("sledai", False)
        basdai = request.json.get("basdai", False)
        pasi = request.json.get("pasi", False)
        bvas = request.json.get("bvas", False)
        mrss = request.json.get("mrss", False)
        sdai_cdai = request.json.get("sdai_cdai", False)
        asdas = request.json.get("asdas", False)
        dapsa = request.json.get("dapsa", False)
        vdi = request.json.get("vdi", False)
        essdai = request.json.get("essdai", False)

        # HEALTH_ASSESSMENT_QUESTIONNAIRE :
        # are you able to
        # dressing
        can_u_dress_urself = request.json.get("can_u_dress_urself", False)
        can_u_wash_ur_hair = request.json.get("can_u_wash_ur_hair", False)
        can_u_comb_ur_hair = request.json.get("can_u_comb_ur_hair", False)
        dressing_score = request.json.get("dressing_score", False)

        # arising
        can_u_stand_from_chair = request.json.get("can_u_stand_from_chair", False)
        can_u_get_inout_from_bed = request.json.get("can_u_get_inout_from_bed", False)
        can_u_sit_grossteg_onfloor = request.json.get("can_u_sit_grossteg_onfloor", False)
        arising_score = request.json.get("arising_score", False)

        # eating
        can_u_cut_vegetables = request.json.get("can_u_cut_vegetables", False)
        can_u_lift_glass = request.json.get("can_u_lift_glass", False)
        can_u_break_roti_from_1hand = request.json.get("can_u_break_roti_from_1hand", False)
        eating_score = request.json.get("eating_score", False)
        # walking
        can_u_walk = request.json.get("can_u_walk", False)
        can_u_climb_5steps = request.json.get("can_u_climb_5steps", False)
        walking_score = request.json.get("walking_score", False)

        # hygiene
        can_u_take_bath = request.json.get("can_u_take_bath", False)
        can_u_wash_dry_urbody = request.json.get("can_u_wash_dry_urbody", False)
        can_u_get_onoff_toilet = request.json.get("can_u_get_onoff_toilet", False)
        hygiene_score = request.json.get("hygiene_score", False)
        # reaching
        can_u_weigh_2kg = request.json.get("can_u_weigh_2kg", False)
        can_u_bend_and_pickcloths = request.json.get("can_u_bend_and_pickcloths", False)
        reaching_score = request.json.get("reaching_score", False)
        # grip
        can_u_open_bottle = request.json.get("can_u_open_bottle", False)
        can_u_turntaps_onoff = request.json.get("can_u_turntaps_onoff", False)
        can_u_open_latches = request.json.get("can_u_open_latches", False)
        grip_score = request.json.get("grip_score", False)
        # activities
        can_u_work_office_house = request.json.get("can_u_work_office_house", False)
        can_u_run_errands = request.json.get("can_u_run_errands", False)
        can_u_get_inout_of_bus = request.json.get("can_u_get_inout_of_bus", False)
        activities_score = request.json.get("activities_score", False)

        # modified CRD pune version HAQ score
        # on a scale of 10 cm
        patient_assessment_pain = request.json.get("patient_assessment_pain", False)
        grip_strength_rt = request.json.get("grip_strength_rt", False)
        grip_strength_hg = request.json.get("grip_strength_hg", False)
        grip_strength_lt = request.json.get("grip_strength_lt", False)
        early_mrng_stiffness = request.json.get("early_mrng_stiffness", False)
        assess_sleep = request.json.get("assess_sleep", False)
        general_health_assessment = request.json.get("general_health_assessment", False)
        # summary(history+examination+investigation)
        classification_criteria_followig = request.json.get("classification_criteria_followig", False)
        diagnosis = request.json.get("diagnosis", False)
        treatment_plan = request.json.get("treatment_plan", False)

        try:
            dbadd = RheumatologyPatientInvestigation(peid=peid, pid=pid, user_id=user_id,
                                                     hematological_date=hematological_date, hem_esr=hem_esr,
                                                     hem_hb=hem_hb, hem_tlc=hem_tlc, hem_pleb=hem_pleb,
                                                     hem_plat=hem_plat, hem_urine=hem_urine, hem_others=hem_others,
                                                     hematological_date2=hematological_date2, hem_esr2=hem_esr2,
                                                     hem_hb2=hem_hb2, hem_tlc2=hem_tlc2, hem_pleb2=hem_pleb2,
                                                     hem_plat2=hem_plat2, hem_urine2=hem_urine2,
                                                     hem_others2=hem_others2,
                                                     hematological_date3=hematological_date3, hem_esr3=hem_esr3,
                                                     hem_hb3=hem_hb3, hem_tlc3=hem_tlc3, hem_pleb3=hem_pleb3,
                                                     hem_plat3=hem_plat3, hem_urine3=hem_urine3,
                                                     hem_others3=hem_others3,
                                                     hematological_date4=hematological_date4, hem_esr4=hem_esr4,
                                                     hem_hb4=hem_hb4, hem_tlc4=hem_tlc4, hem_pleb4=hem_pleb4,
                                                     hem_plat4=hem_plat4, hem_urine4=hem_urine4,
                                                     hem_others4=hem_others4,
                                                     hematological_date5=hematological_date5, hem_esr5=hem_esr5,
                                                     hem_hb5=hem_hb5, hem_tlc5=hem_tlc5, hem_pleb5=hem_pleb5,
                                                     hem_plat5=hem_plat5, hem_urine5=hem_urine5,
                                                     hem_others5=hem_others5, biochemical_date=biochemical_date,
                                                     bio_lft=bio_lft, bio_rft=bio_rft, bio_crp=bio_crp,
                                                     bio_bsl=bio_bsl, bio_ua=bio_ua, bio_others=bio_others,
                                                     biochemical_date2=biochemical_date2, bio_lft2=bio_lft2,
                                                     bio_rft2=bio_rft2, bio_crp2=bio_crp2, bio_bsl2=bio_bsl2,
                                                     bio_ua2=bio_ua2, bio_others2=bio_others2,
                                                     biochemical_date3=biochemical_date3, bio_lft3=bio_lft3,
                                                     bio_rft3=bio_rft3, bio_crp3=bio_crp3, bio_bsl3=bio_bsl3,
                                                     bio_ua3=bio_ua3, bio_others3=bio_others3,
                                                     biochemical_date4=biochemical_date4, bio_lft4=bio_lft4,
                                                     bio_rft4=bio_rft4, bio_crp4=bio_crp4, bio_bsl4=bio_bsl4,
                                                     bio_ua4=bio_ua4, bio_others4=bio_others4,
                                                     biochemical_date5=biochemical_date5, bio_lft5=bio_lft5,
                                                     bio_rft5=bio_rft5, bio_crp5=bio_crp5, bio_bsl5=bio_bsl5,
                                                     bio_ua5=bio_ua5, bio_others5=bio_others5,
                                                     immunological_date=immunological_date, imm_rf=imm_rf,
                                                     imm_accp=imm_accp, imm_ana=imm_ana, imm_anawb=imm_anawb,
                                                     imm_ace=imm_ace, imm_dsdna=imm_dsdna, imm_hlab27=imm_hlab27,
                                                     imm_c3=imm_c3, imm_c4=imm_c4, imm_others=imm_others,
                                                     immunological_date2=immunological_date2, imm_rf2=imm_rf2,
                                                     imm_accp2=imm_accp2, imm_ana2=imm_ana2, imm_anawb2=imm_anawb2,
                                                     imm_ace2=imm_ace2, imm_dsdna2=imm_dsdna2,
                                                     imm_hlab272=imm_hlab272, imm_c32=imm_c32, imm_c42=imm_c42,
                                                     imm_others2=imm_others2,
                                                     immunological_date3=immunological_date3, imm_rf3=imm_rf3,
                                                     imm_accp3=imm_accp3, imm_ana3=imm_ana3, imm_anawb3=imm_anawb3,
                                                     imm_ace3=imm_ace3, imm_dsdna3=imm_dsdna3,
                                                     imm_hlab273=imm_hlab273, imm_c33=imm_c33, imm_c43=imm_c43,
                                                     imm_others3=imm_others3, usg=usg, xray_chest=xray_chest,
                                                     xray_joints=xray_joints, mri=mri, radio_remarks=radio_remarks,
                                                     das28=das28, sledai=sledai, basdai=basdai, pasi=pasi,
                                                     bvas=bvas, mrss=mrss, sdai_cdai=sdai_cdai, asdas=asdas,
                                                     dapsa=dapsa, vdi=vdi, essdai=essdai,
                                                     can_u_dress_urself=can_u_dress_urself,
                                                     can_u_wash_ur_hair=can_u_wash_ur_hair,
                                                     can_u_comb_ur_hair=can_u_comb_ur_hair,
                                                     dressing_score=dressing_score,
                                                     can_u_stand_from_chair=can_u_stand_from_chair,
                                                     can_u_get_inout_from_bed=can_u_get_inout_from_bed,
                                                     can_u_sit_grossteg_onfloor=can_u_sit_grossteg_onfloor,
                                                     arising_score=arising_score,
                                                     can_u_cut_vegetables=can_u_cut_vegetables,
                                                     can_u_lift_glass=can_u_lift_glass,
                                                     can_u_break_roti_from_1hand=can_u_break_roti_from_1hand,
                                                     eating_score=eating_score, can_u_walk=can_u_walk,
                                                     can_u_climb_5steps=can_u_climb_5steps,
                                                     walking_score=walking_score, can_u_take_bath=can_u_take_bath,
                                                     can_u_wash_dry_urbody=can_u_wash_dry_urbody,
                                                     can_u_get_onoff_toilet=can_u_get_onoff_toilet,
                                                     hygiene_score=hygiene_score, can_u_weigh_2kg=can_u_weigh_2kg,
                                                     can_u_bend_and_pickcloths=can_u_bend_and_pickcloths,
                                                     reaching_score=reaching_score,
                                                     can_u_open_bottle=can_u_open_bottle,
                                                     can_u_turntaps_onoff=can_u_turntaps_onoff,
                                                     can_u_open_latches=can_u_open_latches, grip_score=grip_score,
                                                     can_u_work_office_house=can_u_work_office_house,
                                                     can_u_run_errands=can_u_run_errands,
                                                     can_u_get_inout_of_bus=can_u_get_inout_of_bus,
                                                     activities_score=activities_score,
                                                     patient_assessment_pain=patient_assessment_pain,
                                                     grip_strength_rt=grip_strength_rt,
                                                     grip_strength_hg=grip_strength_hg,
                                                     grip_strength_lt=grip_strength_lt,
                                                     early_mrng_stiffness=early_mrng_stiffness,
                                                     assess_sleep=assess_sleep,
                                                     general_health_assessment=general_health_assessment,
                                                     classification_criteria_followig=classification_criteria_followig,
                                                     diagnosis=diagnosis, treatment_plan=treatment_plan)
            session.add(dbadd)
            session.commit()
            return "Success"
        except IntegrityError:
            return "Same PID given"

    return "get request made"


@app.route("/updaterheumapatientinvestigation", methods=['GET', 'POST'])
def updaterheumapatientinvestigation():
    if request.method == 'POST':
        id = request.json.get("id", False)
        pid = request.json.get("pid", False)
        user_id = request.json.get("user_id", False)
        # peid = pid
        hematological_date = request.json.get("hematological_date", False)
        hem_esr = request.json.get("hem_esr", False)
        hem_hb = request.json.get("hem_hb", False)
        hem_tlc = request.json.get("hem_tlc", False)
        hem_pleb = request.json.get("hem_pleb", False)
        hem_plat = request.json.get("hem_plat", False)
        hem_urine = request.json.get("hem_urine", False)
        hem_others = request.json.get("hem_others", False)
        # 2column investigation
        hematological_date2 = request.json.get("hematological_date2", False)
        hem_esr2 = request.json.get("hem_esr2", False)
        hem_hb2 = request.json.get("hem_hb2", False)
        hem_tlc2 = request.json.get("hem_tlc2", False)
        hem_pleb2 = request.json.get("hem_pleb2", False)
        hem_plat2 = request.json.get("hem_plat2", False)
        hem_urine2 = request.json.get("hem_urine2", False)
        hem_others2 = request.json.get("hem_others2", False)
        # 3column investigation
        hematological_date3 = request.json.get("hematological_date3", False)
        hem_esr3 = request.json.get("hem_esr3", False)
        hem_hb3 = request.json.get("hem_hb3", False)
        hem_tlc3 = request.json.get("hem_tlc3", False)
        hem_pleb3 = request.json.get("hem_pleb3", False)
        hem_plat3 = request.json.get("hem_plat3", False)
        hem_urine3 = request.json.get("hem_urine3", False)
        hem_others3 = request.json.get("hem_others3", False)
        # 4column investigation
        hematological_date4 = request.json.get("hematological_date4", False)
        hem_esr4 = request.json.get("hem_esr4", False)
        hem_hb4 = request.json.get("hem_hb4", False)
        hem_tlc4 = request.json.get("hem_tlc4", False)
        hem_pleb4 = request.json.get("hem_pleb4", False)
        hem_plat4 = request.json.get("hem_plat4", False)
        hem_urine4 = request.json.get("hem_urine4", False)
        hem_others4 = request.json.get("hem_others4", False)
        # 5column investigation
        hematological_date5 = request.json.get("hematological_date5", False)
        hem_esr5 = request.json.get("hem_esr5", False)
        hem_hb5 = request.json.get("hem_hb5", False)
        hem_tlc5 = request.json.get("hem_tlc5", False)
        hem_pleb5 = request.json.get("hem_pleb5", False)
        hem_plat5 = request.json.get("hem_plat5", False)
        hem_urine5 = request.json.get("hem_urine5", False)
        hem_others5 = request.json.get("hem_others5", False)
        # table2
        biochemical_date = request.json.get("biochemical_date", False)
        bio_lft = request.json.get("bio_lft", False)
        bio_rft = request.json.get("bio_rft", False)
        bio_crp = request.json.get("bio_crp", False)
        bio_bsl = request.json.get("bio_bsl", False)
        bio_ua = request.json.get("bio_ua", False)
        bio_others = request.json.get("bio_others", False)
        # 2column investigation biochemical
        biochemical_date2 = request.json.get("biochemical_date2", False)
        bio_lft2 = request.json.get("bio_lft2", False)
        bio_rft2 = request.json.get("bio_rft2", False)
        bio_crp2 = request.json.get("bio_crp2", False)
        bio_bsl2 = request.json.get("bio_bsl2", False)
        bio_ua2 = request.json.get("bio_ua2", False)
        bio_others2 = request.json.get("bio_others2", False)
        # 3 column investigation biochemical
        biochemical_date3 = request.json.get("biochemical_date3", False)
        bio_lft3 = request.json.get("bio_lft3", False)
        bio_rft3 = request.json.get("bio_rft3", False)
        bio_crp3 = request.json.get("bio_crp3", False)
        bio_bsl3 = request.json.get("bio_bsl3", False)
        bio_ua3 = request.json.get("bio_ua3", False)
        bio_others3 = request.json.get("bio_others3", False)
        # 4 column investigation biochemical
        biochemical_date4 = request.json.get("biochemical_date4", False)
        bio_lft4 = request.json.get("bio_lft4", False)
        bio_rft4 = request.json.get("bio_rft4", False)
        bio_crp4 = request.json.get("bio_crp4", False)
        bio_bsl4 = request.json.get("bio_bsl4", False)
        bio_ua4 = request.json.get("bio_ua4", False)
        bio_others4 = request.json.get("bio_others4", False)
        # 5 column investigation biochemical
        biochemical_date5 = request.json.get("biochemical_date5", False)
        bio_lft5 = request.json.get("bio_lft5", False)
        bio_rft5 = request.json.get("bio_rft5", False)
        bio_crp5 = request.json.get("bio_crp5", False)
        bio_bsl5 = request.json.get("bio_bsl5", False)
        bio_ua5 = request.json.get("bio_ua5", False)
        bio_others5 = request.json.get("bio_others5", False)

        # table3
        immunological_date = request.json.get("immunological_date", False)
        imm_rf = request.json.get("imm_rf", False)
        imm_accp = request.json.get("imm_accp", False)
        imm_ana = request.json.get("imm_ana", False)
        imm_anawb = request.json.get("imm_anawb", False)
        imm_ace = request.json.get("imm_ace", False)
        imm_dsdna = request.json.get("imm_dsdna", False)
        imm_hlab27 = request.json.get("imm_hlab27", False)
        imm_c3 = request.json.get("imm_c3", False)
        imm_c4 = request.json.get("imm_c4", False)
        imm_others = request.json.get("imm_others", False)
        # 2 column investigation immunological
        immunological_date2 = request.json.get("immunological_date2", False)
        imm_rf2 = request.json.get("imm_rf2", False)
        imm_accp2 = request.json.get("imm_accp2", False)
        imm_ana2 = request.json.get("imm_ana2", False)
        imm_anawb2 = request.json.get("imm_anawb2", False)
        imm_ace2 = request.json.get("imm_ace2", False)
        imm_dsdna2 = request.json.get("imm_dsdna2", False)
        imm_hlab272 = request.json.get("imm_hlab272", False)
        imm_c32 = request.json.get("imm_c32", False)
        imm_c42 = request.json.get("imm_c42", False)
        imm_others2 = request.json.get("imm_others2", False)

        # 3 column investigation immunological
        immunological_date3 = request.json.get("immunological_date3", False)
        imm_rf3 = request.json.get("imm_rf3", False)
        imm_accp3 = request.json.get("imm_accp3", False)
        imm_ana3 = request.json.get("imm_ana3", False)
        imm_anawb3 = request.json.get("imm_anawb3", False)
        imm_ace3 = request.json.get("imm_ace3", False)
        imm_dsdna3 = request.json.get("imm_dsdna3", False)
        imm_hlab273 = request.json.get("imm_hlab273", False)
        imm_c33 = request.json.get("imm_c33", False)
        imm_c43 = request.json.get("imm_c43", False)
        imm_others3 = request.json.get("imm_others3", False)

        # Radiological
        usg = request.json.get("usg", False)
        xray_chest = request.json.get("xray_chest", False)
        xray_joints = request.json.get("xray_joints", False)
        mri = request.json.get("mri", False)
        radio_remarks = request.json.get("radio_remarks", False)

        # Disease activity indices
        das28 = request.json.get("das28", False)
        sledai = request.json.get("sledai", False)
        basdai = request.json.get("basdai", False)
        pasi = request.json.get("pasi", False)
        bvas = request.json.get("bvas", False)
        mrss = request.json.get("mrss", False)
        sdai_cdai = request.json.get("sdai_cdai", False)
        asdas = request.json.get("asdas", False)
        dapsa = request.json.get("dapsa", False)
        vdi = request.json.get("vdi", False)
        essdai = request.json.get("essdai", False)
        # HEALTH_ASSESSMENT_QUESTIONNAIRE :
        # are you able to
        # dressing
        can_u_dress_urself = request.json.get("can_u_dress_urself", False)
        can_u_wash_ur_hair = request.json.get("can_u_wash_ur_hair", False)
        can_u_comb_ur_hair = request.json.get("can_u_comb_ur_hair", False)
        dressing_score = request.json.get("dressing_score", False)
        # arising
        can_u_stand_from_chair = request.json.get("can_u_stand_from_chair", False)
        can_u_get_inout_from_bed = request.json.get("can_u_get_inout_from_bed", False)
        can_u_sit_grossteg_onfloor = request.json.get("can_u_sit_grossteg_onfloor", False)
        arising_score = request.json.get("arising_score", False)
        # eating
        can_u_cut_vegetables = request.json.get("can_u_cut_vegetables", False)
        can_u_lift_glass = request.json.get("can_u_lift_glass", False)
        can_u_break_roti_from_1hand = request.json.get("can_u_break_roti_from_1hand", False)
        eating_score = request.json.get("eating_score", False)

        # walking
        can_u_walk = request.json.get("can_u_walk", False)
        can_u_climb_5steps = request.json.get("can_u_climb_5steps", False)
        walking_score = request.json.get("walking_score", False)

        # hygiene
        can_u_take_bath = request.json.get("can_u_take_bath", False)
        can_u_wash_dry_urbody = request.json.get("can_u_wash_dry_urbody", False)
        can_u_get_onoff_toilet = request.json.get("can_u_get_onoff_toilet", False)
        hygiene_score = request.json.get("hygiene_score", False)

        # reaching
        can_u_weigh_2kg = request.json.get("can_u_weigh_2kg", False)
        can_u_bend_and_pickcloths = request.json.get("can_u_bend_and_pickcloths", False)
        reaching_score = request.json.get("reaching_score", False)
        # grip
        can_u_open_bottle = request.json.get("can_u_open_bottle", False)
        can_u_turntaps_onoff = request.json.get("can_u_turntaps_onoff", False)
        can_u_open_latches = request.json.get("can_u_open_latches", False)
        grip_score = request.json.get("grip_score", False)
        # activities
        can_u_work_office_house = request.json.get("can_u_work_office_house", False)
        can_u_run_errands = request.json.get("can_u_run_errands", False)
        can_u_get_inout_of_bus = request.json.get("can_u_get_inout_of_bus", False)
        activities_score = request.json.get("activities_score", False)
        # modified CRD pune version HAQ score
        # on a scale of 10 cm
        patient_assessment_pain = request.json.get("patient_assessment_pain", False)
        grip_strength_rt = request.json.get("grip_strength_rt", False)
        grip_strength_hg = request.json.get("grip_strength_hg", False)
        grip_strength_lt = request.json.get("grip_strength_lt", False)
        early_mrng_stiffness = request.json.get("early_mrng_stiffness", False)
        assess_sleep = request.json.get("assess_sleep", False)
        general_health_assessment = request.json.get("general_health_assessment", False)
        # summary(history+examination+investigation)
        classification_criteria_followig = request.json.get("classification_criteria_followig", False)
        diagnosis = request.json.get("diagnosis", False)
        treatment_plan = request.json.get("treatment_plan", False)

        dbedit = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).update(
            dict(user_id=user_id, hematological_date=hematological_date, hem_esr=hem_esr,
                 hem_hb=hem_hb, hem_tlc=hem_tlc, hem_pleb=hem_pleb, hem_plat=hem_plat, hem_urine=hem_urine,
                 hem_others=hem_others, hematological_date2=hematological_date2, hem_esr2=hem_esr2,
                 hem_hb2=hem_hb2, hem_tlc2=hem_tlc2, hem_pleb2=hem_pleb2, hem_plat2=hem_plat2,
                 hem_urine2=hem_urine2, hem_others2=hem_others2, hematological_date3=hematological_date3,
                 hem_esr3=hem_esr3, hem_hb3=hem_hb3, hem_tlc3=hem_tlc3, hem_pleb3=hem_pleb3,
                 hem_plat3=hem_plat3, hem_urine3=hem_urine3, hem_others3=hem_others3,
                 hematological_date4=hematological_date4, hem_esr4=hem_esr4, hem_hb4=hem_hb4, hem_tlc4=hem_tlc4,
                 hem_pleb4=hem_pleb4, hem_plat4=hem_plat4, hem_urine4=hem_urine4, hem_others4=hem_others4,
                 hematological_date5=hematological_date5, hem_esr5=hem_esr5, hem_hb5=hem_hb5, hem_tlc5=hem_tlc5,
                 hem_pleb5=hem_pleb5, hem_plat5=hem_plat5, hem_urine5=hem_urine5, hem_others5=hem_others5,
                 biochemical_date=biochemical_date, bio_lft=bio_lft, bio_rft=bio_rft, bio_crp=bio_crp,
                 bio_bsl=bio_bsl, bio_ua=bio_ua, bio_others=bio_others, biochemical_date2=biochemical_date2,
                 bio_lft2=bio_lft2, bio_rft2=bio_rft2, bio_crp2=bio_crp2, bio_bsl2=bio_bsl2, bio_ua2=bio_ua2,
                 bio_others2=bio_others2, biochemical_date3=biochemical_date3, bio_lft3=bio_lft3,
                 bio_rft3=bio_rft3, bio_crp3=bio_crp3, bio_bsl3=bio_bsl3, bio_ua3=bio_ua3,
                 bio_others3=bio_others3, biochemical_date4=biochemical_date4, bio_lft4=bio_lft4,
                 bio_rft4=bio_rft4, bio_crp4=bio_crp4, bio_bsl4=bio_bsl4, bio_ua4=bio_ua4,
                 bio_others4=bio_others4, biochemical_date5=biochemical_date5, bio_lft5=bio_lft5,
                 bio_rft5=bio_rft5, bio_crp5=bio_crp5, bio_bsl5=bio_bsl5, bio_ua5=bio_ua5,
                 bio_others5=bio_others5, immunological_date=immunological_date, imm_rf=imm_rf,
                 imm_accp=imm_accp, imm_ana=imm_ana, imm_anawb=imm_anawb, imm_ace=imm_ace, imm_dsdna=imm_dsdna,
                 imm_hlab27=imm_hlab27, imm_c3=imm_c3, imm_c4=imm_c4, imm_others=imm_others,
                 immunological_date2=immunological_date2, imm_rf2=imm_rf2, imm_accp2=imm_accp2,
                 imm_ana2=imm_ana2, imm_anawb2=imm_anawb2, imm_ace2=imm_ace2, imm_dsdna2=imm_dsdna2,
                 imm_hlab272=imm_hlab272, imm_c32=imm_c32, imm_c42=imm_c42, imm_others2=imm_others2,
                 immunological_date3=immunological_date3, imm_rf3=imm_rf3, imm_accp3=imm_accp3,
                 imm_ana3=imm_ana3, imm_anawb3=imm_anawb3, imm_ace3=imm_ace3, imm_dsdna3=imm_dsdna3,
                 imm_hlab273=imm_hlab273, imm_c33=imm_c33, imm_c43=imm_c43, imm_others3=imm_others3, usg=usg,
                 xray_chest=xray_chest, xray_joints=xray_joints, mri=mri, radio_remarks=radio_remarks,
                 das28=das28, sledai=sledai, basdai=basdai, pasi=pasi, bvas=bvas, mrss=mrss,
                 sdai_cdai=sdai_cdai, asdas=asdas, dapsa=dapsa, vdi=vdi, essdai=essdai,
                 can_u_dress_urself=can_u_dress_urself, can_u_wash_ur_hair=can_u_wash_ur_hair,
                 can_u_comb_ur_hair=can_u_comb_ur_hair, dressing_score=dressing_score,
                 can_u_stand_from_chair=can_u_stand_from_chair,
                 can_u_get_inout_from_bed=can_u_get_inout_from_bed,
                 can_u_sit_grossteg_onfloor=can_u_sit_grossteg_onfloor, arising_score=arising_score,
                 can_u_cut_vegetables=can_u_cut_vegetables, can_u_lift_glass=can_u_lift_glass,
                 can_u_break_roti_from_1hand=can_u_break_roti_from_1hand, eating_score=eating_score,
                 can_u_walk=can_u_walk, can_u_climb_5steps=can_u_climb_5steps, walking_score=walking_score,
                 can_u_take_bath=can_u_take_bath, can_u_wash_dry_urbody=can_u_wash_dry_urbody,
                 can_u_get_onoff_toilet=can_u_get_onoff_toilet, hygiene_score=hygiene_score,
                 can_u_weigh_2kg=can_u_weigh_2kg, can_u_bend_and_pickcloths=can_u_bend_and_pickcloths,
                 reaching_score=reaching_score, can_u_open_bottle=can_u_open_bottle,
                 can_u_turntaps_onoff=can_u_turntaps_onoff, can_u_open_latches=can_u_open_latches,
                 grip_score=grip_score, can_u_work_office_house=can_u_work_office_house,
                 can_u_run_errands=can_u_run_errands, can_u_get_inout_of_bus=can_u_get_inout_of_bus,
                 activities_score=activities_score, patient_assessment_pain=patient_assessment_pain,
                 grip_strength_rt=grip_strength_rt, grip_strength_hg=grip_strength_hg,
                 grip_strength_lt=grip_strength_lt, early_mrng_stiffness=early_mrng_stiffness,
                 assess_sleep=assess_sleep, general_health_assessment=general_health_assessment,
                 classification_criteria_followig=classification_criteria_followig, diagnosis=diagnosis,
                 treatment_plan=treatment_plan))
        session.commit()
        # sending back updated data of the user
        patientdata = session.query(RheumatologyPatientInvestigation).filter_by(pid=pid).first()
        session.commit()
        patientschema = RheumatologyPatientInvestigationSchema()
        output = patientschema.dump(patientdata)
        return jsonify({'RheumatologyPatientInvestigation': output})

    return "get request made"

    # followup rheumatology api start here****


# shows all the followups of a single patient
@app.route("/allfollowuprheumadetails", methods=['GET', 'POST'])
def allfollowuprheumapatientdetails():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyFollowup).filter_by(pid=pid).all()
        session.commit()
        print(RheumatologyPat)
        RheumatologyFSchema = RheumatologyFollowupSchema(many=True)
        output = RheumatologyFSchema.dump(RheumatologyPat)
        print(output)
        return jsonify({'RheumatologyFollowup': output})
    except:
        return "Wrong Pid"


# get  single followup detail for showing single follow detials in edit/view
@app.route("/singlefollowuprheumadetails", methods=['GET', 'POST'])
def followuprheumapatientdetails():
    try:
        id = request.args['id']
    except:
        return "no id sent"
    try:
        RheumatologyPat = session.query(RheumatologyFollowup).filter_by(id=id).first()
        session.commit()
        print(RheumatologyPat)
        RheumatologyFSchema = RheumatologyFollowupSchema(many=True)
        output = RheumatologyFSchema.dump(RheumatologyPat)
        print(output)
        return jsonify({'RheumatologyFollowup': output})
    except:
        return "Wrong Pid"


# single followup delete from set of followup of same pid

@app.route("/followuprheumadelete", methods=['GET', 'POST'])
def followuprheumapatientdelete():
    global name
    try:
        id = request.args['id']
    except:
        return "no pid sent"
    try:
        RheumatologyPat = session.query(RheumatologyFollowup).filter_by(id=id).first()
        if RheumatologyPat:
            session.delete(RheumatologyPat)
            session.commit()
            return "Success"
        return "invalid id"
    except:
        return "Fail"


@app.route("/addfollowuprheuma", methods=['GET', 'POST'])
def addfollowuprheumapatient():
    if request.method == 'POST':
        pid = request.json.get("pid")
        code = request.json.get("code", False)
        pname = request.json.get("pname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        firstvisit = request.json.get("firstvisit", False)
        lastvisit = request.json.get("lastvisit", False)
        timelapsed = request.json.get("timelapsed", False)
        bp = request.json.get("bp", False)
        wt = request.json.get("wt", False)
        followup = request.json.get("followup", False)
        course_result = request.json.get("course_result", False)
        improved_percent = request.json.get("improved_percent", False)
        deteriorate_percent = request.json.get("deteriorate_percent", False)
        history = request.json.get("history", False)
        diagnosis = request.json.get("diagnosis", False)
        addnewdiagnosis = request.json.get("addnewdiagnosis", False)
        anorexia = request.json.get("anorexia", False)
        anorexia_duration = request.json.get("anorexia_duration", False)
        anorexia_severity = request.json.get("anorexia_severity", False)
        anorexia_cause = request.json.get("anorexia_cause", False)
        nausea = request.json.get("nausea", False)
        nausea_duration = request.json.get("nausea_duration", False)
        nausea_severity = request.json.get("nausea_severity", False)
        nausea_cause = request.json.get("nausea_cause", False)
        vomiting = request.json.get("vomiting", False)
        vomiting_duration = request.json.get("vomiting_duration", False)
        vomiting_severity = request.json.get("vomiting_severity", False)
        vomiting_cause = request.json.get("vomiting_cause", False)
        acidity = request.json.get("acidity", False)
        acidity_duration = request.json.get("acidity_duration", False)
        acidity_severity = request.json.get("acidity_severity", False)
        acidity_cause = request.json.get("acidity_cause", False)
        pain_abdomen = request.json.get("pain_abdomen", False)
        pain_duration = request.json.get("pain_duration", False)
        pain_severity = request.json.get("pain_severity", False)
        pain_cause = request.json.get("pain_cause", False)
        diarrhoea = request.json.get("diarrhoea", False)
        diarrhoea_duration = request.json.get("diarrhoea_duration", False)
        diarrhoea_severity = request.json.get("diarrhoea_severity", False)
        diarrhoea_cause = request.json.get("diarrhoea_cause", False)
        oral_ulcers = request.json.get("oral_ulcers", False)
        ulcers_duration = request.json.get("ulcers_duration", False)
        ulcers_severity = request.json.get("ulcers_severity", False)
        ulcers_cause = request.json.get("ulcers_cause", False)
        constipation = request.json.get("constipation", False)
        constipation_duration = request.json.get("constipation_duration", False)
        constipation_severity = request.json.get("constipation_severity", False)
        constipation_cause = request.json.get("constipation_cause", False)
        skinrash = request.json.get("skinrash", False)
        skinrash_duration = request.json.get("skinrash_duration", False)
        skinrash_severity = request.json.get("skinrash_severity", False)
        skinrash_cause = request.json.get("skinrash_cause", False)
        hairfall = request.json.get("hairfall", False)
        hairfall_duration = request.json.get("hairfall_duration", False)
        hairfall_severity = request.json.get("hairfall_severity", False)
        hairfall_cause = request.json.get("hairfall_cause", False)
        othertoxics = request.json.get("othertoxics", False)
        othertoxics_duration = request.json.get("othertoxics_duration", False)
        othertoxics_severity = request.json.get("othertoxics_severity", False)
        othertoxics_cause = request.json.get("othertoxics_cause", False)

        hcq = request.json.get("hcq", False)
        mtx = request.json.get("mtx", False)
        ssz = request.json.get("ssz", False)
        lef = request.json.get("lef", False)
        azt = request.json.get("azt", False)
        mmf = request.json.get("mmf", False)
        pred = request.json.get("pred", False)
        nsaid = request.json.get("nsaid", False)
        esr = request.json.get("esr", False)
        hb = request.json.get("hb", False)
        tlc = request.json.get("tlc", False)
        dlc = request.json.get("dlc", False)
        platelet = request.json.get("platelet", False)
        urine = request.json.get("urine", False)
        bun = request.json.get("bun", False)
        cr = request.json.get("cr", False)
        sgot = request.json.get("sgot", False)
        sgpt = request.json.get("sgpt", False)
        alb = request.json.get("alb", False)
        glob = request.json.get("glob", False)
        ua = request.json.get("ua", False)
        bsl = request.json.get("bsl", False)
        rf = request.json.get("rf", False)
        crp = request.json.get("crp", False)
        bili = request.json.get("bili", False)
        other = request.json.get("other", False)
        xray = request.json.get("xray", False)
        treatment_plan = request.json.get("treatment_plan", False)
        tm_t1 = request.json.get("tm_t1", False)
        tm_s1 = request.json.get("tm_s1", False)
        tm_t2 = request.json.get("tm_t2", False)
        tm_s2 = request.json.get("tm_s2", False)
        scl_t1 = request.json.get("scl_t1", False)
        scl_s1 = request.json.get("scl_s1", False)
        scl_t2 = request.json.get("scl_t2", False)
        scl_s2 = request.json.get("scl_s2", False)
        acl_t1 = request.json.get("acl_t1", False)
        acl_s1 = request.json.get("acl_s1", False)
        acl_t2 = request.json.get("acl_t2", False)
        acl_s2 = request.json.get("acl_s2", False)
        sh_t1 = request.json.get("sh_t1", False)
        sh_s1 = request.json.get("sh_s1", False)
        sh_t2 = request.json.get("sh_t2", False)
        sh_s2 = request.json.get("sh_s2", False)
        elbow_t1 = request.json.get("elbow_t1", False)
        elbow_s1 = request.json.get("elbow_s1", False)
        elbow_t2 = request.json.get("elbow_t2", False)
        elbow_s2 = request.json.get("elbow_s2", False)
        infru_t1 = request.json.get("infru_t1", False)
        infru_s1 = request.json.get("infru_s1", False)
        infru_t2 = request.json.get("infru_t2", False)
        infru_s2 = request.json.get("infru_s2", False)
        cmc1_t1 = request.json.get("cmc1_t1", False)
        cmc1_s1 = request.json.get("cmc1_s1", False)
        cmc1_t2 = request.json.get("cmc1_t2", False)
        cmc1_s2 = request.json.get("cmc1_s2", False)
        wrist_t1 = request.json.get("wrist_t1", False)
        wrist_s1 = request.json.get("wrist_s1", False)
        wrist_t2 = request.json.get("wrist_t2", False)
        wrist_s2 = request.json.get("wrist_s2", False)
        dip2_t1 = request.json.get("dip2_t1", False)
        dip2_s1 = request.json.get("dip2_s1", False)
        dip2_t2 = request.json.get("dip2_t2", False)
        dip2_s2 = request.json.get("dip2_s2", False)
        dip3_t1 = request.json.get("dip3_t1", False)
        dip3_s1 = request.json.get("dip3_s1", False)
        dip3_t2 = request.json.get("dip3_t2", False)
        dip3_s2 = request.json.get("dip3_s2", False)
        dip4_t1 = request.json.get("dip4_t1", False)
        dip4_s1 = request.json.get("dip4_s1", False)
        dip4_t2 = request.json.get("dip4_t2", False)
        dip4_s2 = request.json.get("dip4_s2", False)
        dip5_t1 = request.json.get("dip5_t1", False)
        dip5_s1 = request.json.get("dip5_s1", False)
        dip5_t2 = request.json.get("dip5_t2", False)
        dip5_s2 = request.json.get("dip5_s2", False)
        r1ip1_t1 = request.json.get("r1ip1_t1", False)
        r1ip1_s1 = request.json.get("r1ip1_s1", False)
        r1ip1_t2 = request.json.get("r1ip1_t2", False)
        r1ip1_s2 = request.json.get("r1ip1_s2", False)
        r1ip2_t1 = request.json.get("r1ip2_t1", False)
        r1ip2_s1 = request.json.get("r1ip2_s1", False)
        r1ip2_t2 = request.json.get("r1ip2_t2", False)
        r1ip2_s2 = request.json.get("r1ip2_s2", False)
        pip3_t1 = request.json.get("pip3_t1", False)
        pip3_s1 = request.json.get("pip3_s1", False)
        pip3_t2 = request.json.get("pip3_t2", False)
        pip3_s2 = request.json.get("pip3_s2", False)
        pip4_t1 = request.json.get("pip4_t1", False)
        pip4_s1 = request.json.get("pip4_s1", False)
        pip4_t2 = request.json.get("pip4_t2", False)
        pip4_s2 = request.json.get("pip4_s2", False)
        pip5_t1 = request.json.get("pip5_t1", False)
        pip5_s1 = request.json.get("pip5_s1", False)
        pip5_t2 = request.json.get("pip5_t2", False)
        pip5_s2 = request.json.get("pip5_s2", False)
        mcp1_t1 = request.json.get("mcp1_t1", False)
        mcp1_s1 = request.json.get("mcp1_s1", False)
        mcp1_t2 = request.json.get("mcp1_t2", False)
        mcp1_s2 = request.json.get("mcp1_s2", False)
        mcp2_t1 = request.json.get("mcp2_t1", False)
        mcp2_s1 = request.json.get("mcp2_s1", False)
        mcp2_t2 = request.json.get("mcp2_t2", False)
        mcp2_s2 = request.json.get("mcp2_s2", False)

        mcp3_t1 = request.json.get("mcp3_t1", False)
        mcp3_s1 = request.json.get("mcp3_s1", False)
        mcp3_t2 = request.json.get("mcp3_t2", False)
        mcp3_s2 = request.json.get("mcp3_s2", False)
        mcp4_t1 = request.json.get("mcp4_t1", False)
        mcp4_s1 = request.json.get("mcp4_s1", False)
        mcp4_t2 = request.json.get("mcp4_t2", False)
        mcp4_s2 = request.json.get("mcp4_s2", False)
        mcp5_t1 = request.json.get("mcp5_t1", False)
        mcp5_s1 = request.json.get("mcp5_s1", False)
        mcp5_t2 = request.json.get("mcp5_t2", False)
        mcp5_s2 = request.json.get("mcp5_s2", False)
        hip_t1 = request.json.get("hip_t1", False)
        hip_s1 = request.json.get("hip_s1", False)
        hip_t2 = request.json.get("hip_t2", False)
        hip_s2 = request.json.get("hip_s2", False)
        knee_t1 = request.json.get("knee_t1", False)
        knee_s1 = request.json.get("knee_s1", False)
        knee_t2 = request.json.get("knee_t2", False)
        knee_s2 = request.json.get("knee_s2", False)
        a_tt_t1 = request.json.get("a_tt_t1", False)
        a_tt_s1 = request.json.get("a_tt_s1", False)
        a_tt_t2 = request.json.get("a_tt_t2", False)
        a_tt_s2 = request.json.get("a_tt_s2", False)
        a_tc_t1 = request.json.get("a_tc_t1", False)
        a_tc_s1 = request.json.get("a_tc_s1", False)
        a_tc_t2 = request.json.get("a_tc_t2", False)
        a_tc_s2 = request.json.get("a_tc_s2", False)
        mtl_t1 = request.json.get("mtl_t1", False)
        mtl_s1 = request.json.get("mtl_s1", False)
        mtl_t2 = request.json.get("mtl_t2", False)
        mtl_s2 = request.json.get("mtl_s2", False)
        mtp1_t1 = request.json.get("mtp1_t1", False)
        mtp1_s1 = request.json.get("mtp1_s1", False)
        mtp1_t2 = request.json.get("mtp1_t2", False)
        mtp1_s2 = request.json.get("mtp1_s2", False)
        mtp2_t1 = request.json.get("mtp2_t1", False)
        mtp2_s1 = request.json.get("mtp2_s1", False)
        mtp2_t2 = request.json.get("mtp2_t2", False)
        mtp2_s2 = request.json.get("mtp2_s2", False)
        mtp3_t1 = request.json.get("mtp3_t1", False)
        mtp3_s1 = request.json.get("mtp3_s1", False)
        mtp3_t2 = request.json.get("mtp3_t2", False)
        mtp3_s2 = request.json.get("mtp3_s2", False)
        mtp4_t1 = request.json.get("mtp4_t1", False)
        mtp4_s1 = request.json.get("mtp4_s1", False)
        mtp4_t2 = request.json.get("mtp4_t2", False)
        mtp4_s2 = request.json.get("mtp4_s2", False)
        mtp5_t1 = request.json.get("mtp5_t1", False)
        mtp5_s1 = request.json.get("mtp5_s1", False)
        mtp5_t2 = request.json.get("mtp5_t2", False)
        mtp5_s2 = request.json.get("mtp5_s2", False)
        r2ip1_t1 = request.json.get("r2ip1_t1", False)
        r2ip1_s1 = request.json.get("r2ip1_s1", False)
        r2ip1_t2 = request.json.get("r2ip1_t2", False)
        r2ip1_s2 = request.json.get("r2ip1_s2", False)
        r2ip2_t1 = request.json.get("r2ip2_t1", False)
        r2ip2_s1 = request.json.get("r2ip2_s1", False)
        r2ip2_t2 = request.json.get("r2ip2_t2", False)
        r2ip2_s2 = request.json.get("r2ip2_s2", False)
        ip3_t1 = request.json.get("ip3_t1", False)
        ip3_s1 = request.json.get("ip3_s1", False)
        ip3_t2 = request.json.get("ip3_t2", False)
        ip3_s2 = request.json.get("ip3_s2", False)
        ip4_t1 = request.json.get("ip4_t1", False)
        ip4_s1 = request.json.get("ip4_s1", False)
        ip4_t2 = request.json.get("ip4_t2", False)
        ip4_s2 = request.json.get("ip4_s2", False)
        ip5_t1 = request.json.get("ip5_t1", False)
        ip5_s1 = request.json.get("ip5_s1", False)
        ip5_t2 = request.json.get("ip5_t2", False)
        ip5_s2 = request.json.get("ip5_s2", False)
        s1_t1 = request.json.get("s1_t1", False)
        s1_s1 = request.json.get("s1_s1", False)
        s1_t2 = request.json.get("s1_t2", False)
        s1_s2 = request.json.get("s1_s2", False)

        cervical_anky = request.json.get("cervical_anky", False)
        cervical_flex = request.json.get("cervical_flex", False)
        cervical_ext = request.json.get("cervical_ext", False)
        cervical_rtrot = request.json.get("cervical_rtrot", False)
        cervical_ltrot = request.json.get("cervical_ltrot", False)
        cervical_rtfl = request.json.get("cervical_rtfl", False)
        cervical_ltfl = request.json.get("cervical_ltfl", False)
        cervical_pt = request.json.get("cervical_pt", False)

        thorasic_anky = request.json.get("thorasic_anky", False)
        thorasic_flex = request.json.get("thorasic_flex", False)
        thorasic_ext = request.json.get("thorasic_ext", False)
        thorasic_rtrot = request.json.get("thorasic_rtrot", False)
        thorasic_ltrot = request.json.get("thorasic_ltrot", False)
        thorasic_rtfl = request.json.get("thorasic_rtfl", False)
        thorasic_ltfl = request.json.get("thorasic_ltfl", False)
        thorasic_pt = request.json.get("thorasic_pt", False)
        lumbar_anky = request.json.get("lumbar_anky", False)
        lumbar_flex = request.json.get("lumbar_flex", False)
        lumbar_ext = request.json.get("lumbar_ext", False)
        lumbar_rtrot = request.json.get("lumbar_rtrot", False)
        lumbar_ltrot = request.json.get("lumbar_ltrot", False)
        lumbar_rtfl = request.json.get("lumbar_rtfl", False)
        lumbar_ltfl = request.json.get("lumbar_ltfl", False)
        lumbar_pt = request.json.get("lumbar_pt", False)
        opt_rt = request.json.get("opt_rt", False)
        opt_lt = request.json.get("opt_lt", False)
        lcer_rt = request.json.get("lcer_rt", False)
        lcer_lt = request.json.get("lcer_lt", False)
        trpz_rt = request.json.get("trpz_rt", False)
        trpz_lt = request.json.get("trpz_lt", False)
        scap_rt = request.json.get("scap_rt", False)
        scap_lt = request.json.get("scap_lt", False)
        zcst_rt = request.json.get("zcst_rt", False)
        zcst_lt = request.json.get("zcst_lt", False)
        epdl_rt = request.json.get("epdl_rt", False)
        epdl_lt = request.json.get("epdl_lt", False)
        glut_rt = request.json.get("glut_rt", False)
        glut_lt = request.json.get("glut_lt", False)
        trcr_rt = request.json.get("trcr_rt", False)
        trcr_lt = request.json.get("trcr_lt", False)
        knee_rt = request.json.get("knee_rt", False)
        knee_lt = request.json.get("knee_lt", False)
        ta_rt = request.json.get("ta_rt", False)
        ta_lt = request.json.get("ta_lt", False)
        calf_rt = request.json.get("calf_rt", False)
        calf_lt = request.json.get("calf_lt", False)
        sole_rt = request.json.get("sole_rt", False)
        sole_lt = request.json.get("sole_lt", False)
        rhand_fl = request.json.get("rhand_fl", False)
        rhand_ex = request.json.get("rhand_ex", False)
        rhand_ab = request.json.get("rhand_ab", False)
        rhand_add = request.json.get("rhand_add", False)
        rhand_slx = request.json.get("rhand_slx", False)
        lhand_fl = request.json.get("lhand_fl", False)
        lhand_ex = request.json.get("lhand_ex", False)
        lhand_ab = request.json.get("lhand_ab", False)
        lhand_add = request.json.get("lhand_add", False)
        lhand_slx = request.json.get("lhand_slx", False)
        rwrist_fl = request.json.get("rwrist_fl", False)
        rwrist_ex = request.json.get("rwrist_ex", False)
        rwrist_ab = request.json.get("rwrist_ab", False)
        rwrist_add = request.json.get("rwrist_add", False)
        rwrist_slx = request.json.get("rwrist_slx", False)
        lwrist_fl = request.json.get("lwrist_fl", False)
        lwrist_ex = request.json.get("lwrist_ex", False)
        lwrist_ab = request.json.get("lwrist_ab", False)
        lwrist_add = request.json.get("lwrist_add", False)
        lwrist_slx = request.json.get("lwrist_slx", False)
        relb_fl = request.json.get("relb_fl", False)
        relb_ex = request.json.get("relb_ex", False)
        relb_ab = request.json.get("relb_ab", False)
        relb_add = request.json.get("relb_add", False)
        relb_slx = request.json.get("relb_slx", False)

        lelb_fl = request.json.get("lelb_fl", False)
        lelb_ex = request.json.get("lelb_ex", False)
        lelb_ab = request.json.get("lelb_ab", False)
        lelb_add = request.json.get("lelb_add", False)
        lelb_slx = request.json.get("lelb_slx", False)

        shrt_fl = request.json.get("shrt_fl", False)
        shrt_ex = request.json.get("shrt_ex", False)
        shrt_ab = request.json.get("shrt_ab", False)
        shrt_add = request.json.get("shrt_add", False)
        shrt_slx = request.json.get("shrt_slx", False)
        shlt_fl = request.json.get("shlt_fl", False)
        shlt_ex = request.json.get("shlt_ex", False)
        shlt_ab = request.json.get("shlt_ab", False)
        shlt_add = request.json.get("shlt_add", False)
        shlt_slx = request.json.get("shlt_slx", False)
        kneert_fl = request.json.get("kneert_fl", False)
        kneert_ex = request.json.get("kneert_ex", False)
        kneert_ab = request.json.get("kneert_ab", False)
        kneert_add = request.json.get("kneert_add", False)
        kneert_slx = request.json.get("kneert_slx", False)
        kneelt_fl = request.json.get("kneelt_fl", False)
        kneelt_ex = request.json.get("kneelt_ex", False)
        kneelt_ab = request.json.get("kneelt_ab", False)
        kneelt_add = request.json.get("kneelt_add", False)
        kneelt_slx = request.json.get("kneelt_slx", False)
        footrt_fl = request.json.get("footrt_fl", False)
        footrt_ex = request.json.get("footrt_ex", False)
        footrt_ab = request.json.get("footrt_ab", False)
        footrt_add = request.json.get("footrt_add", False)
        footrt_slx = request.json.get("footrt_slx", False)
        footlt_fl = request.json.get("footlt_fl", False)
        footlt_ex = request.json.get("footlt_ex", False)
        footlt_ab = request.json.get("footlt_ab", False)
        footlt_add = request.json.get("footlt_add", False)
        footlt_slx = request.json.get("footlt_slx", False)
        thumb_rt = request.json.get("thumb_rt", False)
        thumb_lt = request.json.get("thumb_lt", False)
        finger_rt = request.json.get("finger_rt", False)
        finger_lt = request.json.get("finger_lt", False)
        palm_rt = request.json.get("palm_rt", False)
        palm_lt = request.json.get("palm_lt", False)
        elbow_rt = request.json.get("elbow_rt", False)
        elbow_lt = request.json.get("elbow_lt", False)
        hy_knee_rt = request.json.get("hy_knee_rt", False)
        hy_knee_lt = request.json.get("hy_knee_lt", False)
        ankle_rt = request.json.get("ankle_rt", False)
        ankle_lt = request.json.get("ankle_lt", False)
        other_rt = request.json.get("other_rt", False)
        other_lt = request.json.get("other_lt", False)
        spine_rt = request.json.get("spine_rt", False)
        spine_lt = request.json.get("spine_lt", False)

        can_u_dress_urself = request.json.get("can_u_dress_urself", False)
        can_u_wash_ur_hair = request.json.get("can_u_wash_ur_hair", False)
        can_u_comb_ur_hair = request.json.get("can_u_comb_ur_hair", False)
        dressing_score = request.json.get("dressing_score", False)

        can_u_stand_from_chair = request.json.get("can_u_stand_from_chair", False)
        can_u_get_inout_from_bed = request.json.get("can_u_get_inout_from_bed", False)
        can_u_sit_grossteg_onfloor = request.json.get("can_u_sit_grossteg_onfloor", False)
        arising_score = request.json.get("arising_score", False)

        can_u_cut_vegetables = request.json.get("can_u_cut_vegetables", False)
        can_u_lift_glass = request.json.get("can_u_lift_glass", False)
        can_u_break_roti_from_1hand = request.json.get("can_u_break_roti_from_1hand", False)
        eating_score = request.json.get("eating_score", False)

        can_u_walk = request.json.get("can_u_walk", False)
        can_u_climb_5steps = request.json.get("can_u_climb_5steps", False)
        walking_score = request.json.get("walking_score", False)

        can_u_take_bath = request.json.get("can_u_take_bath", False)
        can_u_wash_dry_urbody = request.json.get("can_u_wash_dry_urbody", False)
        can_u_get_onoff_toilet = request.json.get("can_u_get_onoff_toilet", False)
        hygiene_score = request.json.get("hygiene_score", False)

        can_u_weigh_2kg = request.json.get("can_u_weigh_2kg", False)
        can_u_bend_and_pickcloths = request.json.get("can_u_bend_and_pickcloths", False)
        reaching_score = request.json.get("reaching_score", False)

        can_u_open_bottle = request.json.get("can_u_open_bottle", False)
        can_u_turntaps_onoff = request.json.get("can_u_turntaps_onoff", False)
        can_u_open_latches = request.json.get("can_u_open_latches", False)
        grip_score = request.json.get("grip_score", False)

        can_u_work_office_house = request.json.get("can_u_work_office_house", False)
        can_u_run_errands = request.json.get("can_u_run_errands", False)
        can_u_get_inout_of_bus = request.json.get("can_u_get_inout_of_bus", False)
        activities_score = request.json.get("activities_score", False)

        patient_assessment_pain = request.json.get("patient_assessment_pain", False)
        grip_strength_rt = request.json.get("grip_strength_rt", False)
        grip_strength_hg = request.json.get("grip_strength_hg", False)
        grip_strength_lt = request.json.get("grip_strength_lt", False)
        early_mrng_stiffness = request.json.get("early_mrng_stiffness", False)
        sleep = request.json.get("sleep", False)
        general_health_assessment = request.json.get("general_health_assessment", False)
        user_id = request.json.get("user_id", False)

        try:
            dbadd = RheumatologyFollowup(lelb_ab=lelb_ab, lelb_ex=lelb_ex, lelb_fl=lelb_fl, lelb_add=lelb_add,
                                         lelb_slx=lelb_slx, pid=pid, code=code, pname=pname, age=age, sex=sex,
                                         address=address, phone=phone, firstvisit=firstvisit, lastvisit=lastvisit,
                                         timelapsed=timelapsed, bp=bp, wt=wt, followup=followup,
                                         course_result=course_result, improved_percent=improved_percent,
                                         deteriorate_percent=deteriorate_percent, history=history, diagnosis=diagnosis,
                                         addnewdiagnosis=addnewdiagnosis, anorexia=anorexia,
                                         anorexia_duration=anorexia_duration, anorexia_severity=anorexia_severity,
                                         anorexia_cause=anorexia_cause, nausea=nausea, nausea_duration=nausea_duration,
                                         nausea_severity=nausea_severity, nausea_cause=nausea_cause, vomiting=vomiting,
                                         vomiting_duration=vomiting_duration, vomiting_severity=vomiting_severity,
                                         vomiting_cause=vomiting_cause, acidity=acidity, acidity_cause=acidity_cause,
                                         acidity_duration=acidity_duration, acidity_severity=acidity_severity,
                                         pain_abdomen=pain_abdomen, pain_duration=pain_duration,
                                         pain_severity=pain_severity, pain_cause=pain_cause, diarrhoea=diarrhoea,
                                         diarrhoea_duration=diarrhoea_duration, diarrhoea_severity=diarrhoea_severity,
                                         diarrhoea_cause=diarrhoea_cause, oral_ulcers=oral_ulcers,
                                         ulcers_duration=ulcers_duration, ulcers_severity=ulcers_severity,
                                         ulcers_cause=ulcers_cause, constipation=constipation,
                                         constipation_duration=constipation_duration,
                                         constipation_severity=constipation_severity,
                                         constipation_cause=constipation_cause, skinrash=skinrash,
                                         skinrash_duration=skinrash_duration, skinrash_severity=skinrash_severity,
                                         skinrash_cause=skinrash_cause, hairfall=hairfall,
                                         hairfall_duration=hairfall_duration, hairfall_severity=hairfall_severity,
                                         hairfall_cause=hairfall_cause, othertoxics=othertoxics,
                                         othertoxics_duration=othertoxics_duration,
                                         othertoxics_severity=othertoxics_severity, othertoxics_cause=othertoxics_cause,
                                         hcq=hcq, mtx=mtx, ssz=ssz, lef=lef, azt=azt, mmf=mmf, pred=pred, nsaid=nsaid,
                                         esr=esr, hb=hb, tlc=tlc, dlc=dlc, platelet=platelet, urine=urine, bun=bun,
                                         cr=cr, sgot=sgot, sgpt=sgpt, alb=alb, glob=glob, ua=ua, bsl=bsl, rf=rf,
                                         crp=crp, bili=bili, other=other, xray=xray, treatment_plan=treatment_plan,
                                         tm_t1=tm_t1, tm_s1=tm_s1, tm_t2=tm_t2, tm_s2=tm_s2, scl_t1=scl_t1,
                                         scl_s1=scl_s1, scl_t2=scl_t2, scl_s2=scl_s2, acl_t1=acl_t1, acl_s1=acl_s1,
                                         acl_t2=acl_t2, acl_s2=acl_s2, sh_t1=sh_t1, sh_s1=sh_s1, sh_t2=sh_t2,
                                         sh_s2=sh_s2, elbow_t1=elbow_t1, elbow_s1=elbow_s1, elbow_t2=elbow_t2,
                                         elbow_s2=elbow_s2, infru_t1=infru_t1, infru_s1=infru_s1, infru_t2=infru_t2,
                                         infru_s2=infru_s2, cmc1_t1=cmc1_t1, cmc1_s1=cmc1_s1, cmc1_t2=cmc1_t2,
                                         cmc1_s2=cmc1_s2, wrist_t1=wrist_t1, wrist_s1=wrist_s1, wrist_t2=wrist_t2,
                                         wrist_s2=wrist_s2, dip2_t1=dip2_t1, dip2_s1=dip2_s1, dip2_t2=dip2_t2,
                                         dip2_s2=dip2_s2, dip3_t1=dip3_t1, dip3_s1=dip3_s1, dip3_t2=dip3_t2,
                                         dip3_s2=dip3_s2, dip4_t1=dip4_t1, dip4_s1=dip4_s1, dip4_t2=dip4_t2,
                                         dip4_s2=dip4_s2, dip5_t1=dip5_t1, dip5_s1=dip5_s1, dip5_t2=dip5_t2,
                                         dip5_s2=dip5_s2, r1ip1_t1=r1ip1_t1, r1ip1_s1=r1ip1_s1, r1ip1_t2=r1ip1_t2,
                                         r1ip1_s2=r1ip1_s2, r1ip2_t1=r1ip2_t1, r1ip2_s1=r1ip2_s1, r1ip2_t2=r1ip2_t2,
                                         r1ip2_s2=r1ip2_s2, pip3_t1=pip3_t1, pip3_s1=pip3_s1, pip3_t2=pip3_t2,
                                         pip3_s2=pip3_s2, pip4_t1=pip4_t1, pip4_s1=pip4_s1, pip4_t2=pip4_t2,
                                         pip4_s2=pip4_s2, pip5_t1=pip5_t1, pip5_s1=pip5_s1, pip5_t2=pip5_t2,
                                         pip5_s2=pip5_s2, mcp1_t1=mcp1_t1, mcp1_s1=mcp1_s1, mcp1_t2=mcp1_t2,
                                         mcp1_s2=mcp1_s2, mcp2_t1=mcp2_t1, mcp2_s1=mcp2_s1, mcp2_t2=mcp2_t2,
                                         mcp2_s2=mcp2_s2, mcp3_t1=mcp3_t1, mcp3_s1=mcp3_s1, mcp3_t2=mcp3_t2,
                                         mcp3_s2=mcp3_s2, mcp4_t1=mcp4_t1, mcp4_s1=mcp4_s1, mcp4_t2=mcp4_t2,
                                         mcp4_s2=mcp4_s2, mcp5_t1=mcp5_t1, mcp5_s1=mcp5_s1, mcp5_t2=mcp5_t2,
                                         mcp5_s2=mcp5_s2, hip_t1=hip_t1, hip_s1=hip_s1, hip_t2=hip_t2, hip_s2=hip_s2,
                                         knee_t1=knee_t1, knee_s1=knee_s1, knee_t2=knee_t2, knee_s2=knee_s2,
                                         a_tt_t1=a_tt_t1, a_tt_s1=a_tt_s1, a_tt_t2=a_tt_t2, a_tt_s2=a_tt_s2,
                                         a_tc_t1=a_tc_t1, a_tc_s1=a_tc_s1, a_tc_t2=a_tc_t2, a_tc_s2=a_tc_s2,
                                         mtl_t1=mtl_t1, mtl_s1=mtl_s1, mtl_t2=mtl_t2, mtl_s2=mtl_s2, mtp1_t1=mtp1_t1,
                                         mtp1_s1=mtp1_s1, mtp1_t2=mtp1_t2, mtp1_s2=mtp1_s2, mtp2_t1=mtp2_t1,
                                         mtp2_s1=mtp2_s1, mtp2_t2=mtp2_t2, mtp2_s2=mtp2_s2, mtp3_t1=mtp3_t1,
                                         mtp3_s1=mtp3_s1, mtp3_t2=mtp3_t2, mtp3_s2=mtp3_s2, mtp4_t1=mtp4_t1,
                                         mtp4_s1=mtp4_s1, mtp4_t2=mtp4_t2, mtp4_s2=mtp4_s2, mtp5_t1=mtp5_t1,
                                         mtp5_s1=mtp5_s1, mtp5_t2=mtp5_t2, mtp5_s2=mtp5_s2, r2ip1_t1=r2ip1_t1,
                                         r2ip1_s1=r2ip1_s1, r2ip1_t2=r2ip1_t2, r2ip1_s2=r2ip1_s2, r2ip2_t1=r2ip2_t1,
                                         r2ip2_s1=r2ip2_s1, r2ip2_t2=r2ip2_t2, r2ip2_s2=r2ip2_s2, ip3_t1=ip3_t1,
                                         ip3_s1=ip3_s1, ip3_t2=ip3_t2, ip3_s2=ip3_s2, ip4_t1=ip4_t1, ip4_s1=ip4_s1,
                                         ip4_t2=ip4_t2, ip4_s2=ip4_s2, ip5_t1=ip5_t1, ip5_s1=ip5_s1, ip5_t2=ip5_t2,
                                         ip5_s2=ip5_s2, s1_t1=s1_t1, s1_s1=s1_s1, s1_t2=s1_t2, s1_s2=s1_s2,
                                         cervical_anky=cervical_anky, cervical_flex=cervical_flex,
                                         cervical_ext=cervical_ext, cervical_rtrot=cervical_rtrot,
                                         cervical_ltrot=cervical_ltrot, cervical_rtfl=cervical_rtfl,
                                         cervical_ltfl=cervical_ltfl, cervical_pt=cervical_pt,
                                         thorasic_anky=thorasic_anky, thorasic_flex=thorasic_flex,
                                         thorasic_ext=thorasic_ext, thorasic_rtrot=thorasic_rtrot,
                                         thorasic_ltrot=thorasic_ltrot, thorasic_rtfl=thorasic_rtfl,
                                         thorasic_ltfl=thorasic_ltfl, thorasic_pt=thorasic_pt, lumbar_anky=lumbar_anky,
                                         lumbar_flex=lumbar_flex, lumbar_ext=lumbar_ext, lumbar_rtrot=lumbar_rtrot,
                                         lumbar_ltrot=lumbar_ltrot, lumbar_rtfl=lumbar_rtfl, lumbar_ltfl=lumbar_ltfl,
                                         lumbar_pt=lumbar_pt, opt_rt=opt_rt, opt_lt=opt_lt, lcer_rt=lcer_rt,
                                         lcer_lt=lcer_lt, trpz_rt=trpz_rt, trpz_lt=trpz_lt, scap_rt=scap_rt,
                                         scap_lt=scap_lt, zcst_rt=zcst_rt, zcst_lt=zcst_lt, epdl_rt=epdl_rt,
                                         epdl_lt=epdl_lt, glut_rt=glut_rt, glut_lt=glut_lt, trcr_rt=trcr_rt,
                                         trcr_lt=trcr_lt, knee_rt=knee_rt, knee_lt=knee_lt, ta_rt=ta_rt, ta_lt=ta_lt,
                                         calf_rt=calf_rt, calf_lt=calf_lt, sole_rt=sole_rt, sole_lt=sole_lt,
                                         rhand_fl=rhand_fl, rhand_ex=rhand_ex, rhand_ab=rhand_ab, rhand_add=rhand_add,
                                         rhand_slx=rhand_slx, lhand_fl=lhand_fl, lhand_ex=lhand_ex, lhand_ab=lhand_ab,
                                         lhand_add=lhand_add, lhand_slx=lhand_slx, rwrist_fl=rwrist_fl,
                                         rwrist_ex=rwrist_ex, rwrist_ab=rwrist_ab, rwrist_add=rwrist_add,
                                         rwrist_slx=rwrist_slx, lwrist_fl=lwrist_fl, lwrist_ex=lwrist_ex,
                                         lwrist_ab=lwrist_ab, lwrist_add=lwrist_add, lwrist_slx=lwrist_slx,
                                         relb_fl=relb_fl, relb_ex=relb_ex, relb_ab=relb_ab, relb_add=relb_add,
                                         relb_slx=relb_slx, shrt_fl=shrt_fl, shrt_ex=shrt_ex, shrt_ab=shrt_ab,
                                         shrt_add=shrt_add, shrt_slx=shrt_slx, shlt_fl=shlt_fl, shlt_ex=shlt_ex,
                                         shlt_ab=shlt_ab, shlt_add=shlt_add, shlt_slx=shlt_slx, kneert_fl=kneert_fl,
                                         kneert_ex=kneert_ex, kneert_ab=kneert_ab, kneert_add=kneert_add,
                                         kneert_slx=kneert_slx, kneelt_fl=kneelt_fl, kneelt_ex=kneelt_ex,
                                         kneelt_ab=kneelt_ab, kneelt_add=kneelt_add, kneelt_slx=kneelt_slx,
                                         footrt_fl=footrt_fl, footrt_ex=footrt_ex, footrt_ab=footrt_ab,
                                         footrt_add=footrt_add, footrt_slx=footrt_slx, footlt_fl=footlt_fl,
                                         footlt_ex=footlt_ex, footlt_ab=footlt_ab, footlt_add=footlt_add,
                                         footlt_slx=footlt_slx, thumb_rt=thumb_rt, thumb_lt=thumb_lt,
                                         finger_rt=finger_rt, finger_lt=finger_lt, palm_rt=palm_rt, palm_lt=palm_lt,
                                         elbow_rt=elbow_rt, elbow_lt=elbow_lt, hy_knee_rt=hy_knee_rt,
                                         hy_knee_lt=hy_knee_lt, ankle_rt=ankle_rt, ankle_lt=ankle_lt, other_rt=other_rt,
                                         other_lt=other_lt, spine_rt=spine_rt, spine_lt=spine_lt,
                                         can_u_dress_urself=can_u_dress_urself, can_u_wash_ur_hair=can_u_wash_ur_hair,
                                         can_u_comb_ur_hair=can_u_comb_ur_hair, dressing_score=dressing_score,
                                         can_u_stand_from_chair=can_u_stand_from_chair,
                                         can_u_get_inout_from_bed=can_u_get_inout_from_bed,
                                         can_u_sit_grossteg_onfloor=can_u_sit_grossteg_onfloor,
                                         arising_score=arising_score, can_u_cut_vegetables=can_u_cut_vegetables,
                                         can_u_lift_glass=can_u_lift_glass,
                                         can_u_break_roti_from_1hand=can_u_break_roti_from_1hand,
                                         eating_score=eating_score, can_u_walk=can_u_walk,
                                         can_u_climb_5steps=can_u_climb_5steps, walking_score=walking_score,
                                         can_u_take_bath=can_u_take_bath, can_u_wash_dry_urbody=can_u_wash_dry_urbody,
                                         can_u_get_onoff_toilet=can_u_get_onoff_toilet, hygiene_score=hygiene_score,
                                         can_u_weigh_2kg=can_u_weigh_2kg,
                                         can_u_bend_and_pickcloths=can_u_bend_and_pickcloths,
                                         reaching_score=reaching_score, can_u_open_bottle=can_u_open_bottle,
                                         can_u_turntaps_onoff=can_u_turntaps_onoff,
                                         can_u_open_latches=can_u_open_latches, grip_score=grip_score,
                                         can_u_work_office_house=can_u_work_office_house,
                                         can_u_run_errands=can_u_run_errands,
                                         can_u_get_inout_of_bus=can_u_get_inout_of_bus,
                                         activities_score=activities_score,
                                         patient_assessment_pain=patient_assessment_pain,
                                         grip_strength_rt=grip_strength_rt, grip_strength_hg=grip_strength_hg,
                                         grip_strength_lt=grip_strength_lt, early_mrng_stiffness=early_mrng_stiffness,
                                         sleep=sleep, general_health_assessment=general_health_assessment,
                                         user_id=user_id)
            session.add(dbadd)
            session.commit()
            return "Success"
        except IntegrityError:
            return "Same PID given"
    return "get request made"


@app.route("/updatefollowuprheuma", methods=['GET', 'POST'])
def updatefollowuprheuma():
    if request.method == 'POST':
        # as multiple followup for single patient id is necessary
        id = request.json.get("id")
        pid = request.json.get("pid", False)
        code = request.json.get("code", False)
        date = request.json.get("date")
        pname = request.json.get("pname", False)
        age = request.json.get("age", False)
        sex = request.json.get("sex", False)
        address = request.json.get("address", False)
        phone = request.json.get("phone", False)
        firstvisit = request.json.get("firstvisit", False)
        lastvisit = request.json.get("lastvisit", False)
        timelapsed = request.json.get("timelapsed", False)
        bp = request.json.get("bp", False)
        wt = request.json.get("wt", False)
        followup = request.json.get("followup", False)
        course_result = request.json.get("course_result", False)
        improved_percent = request.json.get("improved_percent", False)
        deteriorate_percent = request.json.get("deteriorate_percent", False)
        history = request.json.get("history", False)
        diagnosis = request.json.get("diagnosis", False)
        addnewdiagnosis = request.json.get("addnewdiagnosis", False)
        anorexia = request.json.get("anorexia", False)
        anorexia_duration = request.json.get("anorexia_duration", False)
        anorexia_severity = request.json.get("anorexia_severity", False)
        anorexia_cause = request.json.get("anorexia_cause", False)
        nausea = request.json.get("nausea", False)
        nausea_duration = request.json.get("nausea_duration", False)
        nausea_severity = request.json.get("nausea_severity", False)
        nausea_cause = request.json.get("nausea_cause", False)
        vomiting = request.json.get("vomiting", False)
        vomiting_duration = request.json.get("vomiting_duration", False)
        vomiting_severity = request.json.get("vomiting_severity", False)
        vomiting_cause = request.json.get("vomiting_cause", False)
        acidity = request.json.get("acidity", False)
        acidity_duration = request.json.get("acidity_duration", False)
        acidity_severity = request.json.get("acidity_severity", False)
        acidity_cause = request.json.get("acidity_cause", False)
        pain_abdomen = request.json.get("pain_abdomen", False)
        pain_duration = request.json.get("pain_duration", False)
        pain_severity = request.json.get("pain_severity", False)
        pain_cause = request.json.get("pain_cause", False)
        diarrhoea = request.json.get("diarrhoea", False)
        diarrhoea_duration = request.json.get("diarrhoea_duration", False)
        diarrhoea_severity = request.json.get("diarrhoea_severity", False)
        diarrhoea_cause = request.json.get("diarrhoea_cause", False)
        oral_ulcers = request.json.get("oral_ulcers", False)
        ulcers_duration = request.json.get("ulcers_duration", False)
        ulcers_severity = request.json.get("ulcers_severity", False)
        ulcers_cause = request.json.get("ulcers_cause", False)
        constipation = request.json.get("constipation", False)
        constipation_duration = request.json.get("constipation_duration", False)
        constipation_severity = request.json.get("constipation_severity", False)
        constipation_cause = request.json.get("constipation_cause", False)
        skinrash = request.json.get("skinrash", False)
        skinrash_duration = request.json.get("skinrash_duration", False)
        skinrash_severity = request.json.get("skinrash_severity", False)
        skinrash_cause = request.json.get("skinrash_cause", False)
        hairfall = request.json.get("hairfall", False)
        hairfall_duration = request.json.get("hairfall_duration", False)
        hairfall_severity = request.json.get("hairfall_severity", False)
        hairfall_cause = request.json.get("hairfall_cause", False)
        othertoxics = request.json.get("othertoxics", False)
        othertoxics_duration = request.json.get("othertoxics_duration", False)
        othertoxics_severity = request.json.get("othertoxics_severity", False)
        othertoxics_cause = request.json.get("othertoxics_cause", False)

        hcq = request.json.get("hcq", False)
        mtx = request.json.get("mtx", False)
        ssz = request.json.get("ssz", False)
        lef = request.json.get("lef", False)
        azt = request.json.get("azt", False)
        mmf = request.json.get("mmf", False)
        pred = request.json.get("pred", False)
        nsaid = request.json.get("nsaid", False)
        esr = request.json.get("esr", False)
        hb = request.json.get("hb", False)
        tlc = request.json.get("tlc", False)
        dlc = request.json.get("dlc", False)
        platelet = request.json.get("platelet", False)
        urine = request.json.get("urine", False)
        bun = request.json.get("bun", False)
        cr = request.json.get("cr", False)
        sgot = request.json.get("sgot", False)
        sgpt = request.json.get("sgpt", False)
        alb = request.json.get("alb", False)
        glob = request.json.get("glob", False)
        ua = request.json.get("ua", False)
        bsl = request.json.get("bsl", False)
        rf = request.json.get("rf", False)
        crp = request.json.get("crp", False)
        bili = request.json.get("bili", False)
        other = request.json.get("other", False)
        xray = request.json.get("xray", False)
        treatment_plan = request.json.get("treatment_plan", False)
        tm_t1 = request.json.get("tm_t1", False)
        tm_s1 = request.json.get("tm_s1", False)
        tm_t2 = request.json.get("tm_t2", False)
        tm_s2 = request.json.get("tm_s2", False)
        scl_t1 = request.json.get("scl_t1", False)
        scl_s1 = request.json.get("scl_s1", False)
        scl_t2 = request.json.get("scl_t2", False)
        scl_s2 = request.json.get("scl_s2", False)
        acl_t1 = request.json.get("acl_t1", False)
        acl_s1 = request.json.get("acl_s1", False)
        acl_t2 = request.json.get("acl_t2", False)
        acl_s2 = request.json.get("acl_s2", False)
        sh_t1 = request.json.get("sh_t1", False)
        sh_s1 = request.json.get("sh_s1", False)
        sh_t2 = request.json.get("sh_t2", False)
        sh_s2 = request.json.get("sh_s2", False)
        elbow_t1 = request.json.get("elbow_t1", False)
        elbow_s1 = request.json.get("elbow_s1", False)
        elbow_t2 = request.json.get("elbow_t2", False)
        elbow_s2 = request.json.get("elbow_s2", False)
        infru_t1 = request.json.get("infru_t1", False)
        infru_s1 = request.json.get("infru_s1", False)
        infru_t2 = request.json.get("infru_t2", False)
        infru_s2 = request.json.get("infru_s2", False)
        cmc1_t1 = request.json.get("cmc1_t1", False)
        cmc1_s1 = request.json.get("cmc1_s1", False)
        cmc1_t2 = request.json.get("cmc1_t2", False)
        cmc1_s2 = request.json.get("cmc1_s2", False)
        wrist_t1 = request.json.get("wrist_t1", False)
        wrist_s1 = request.json.get("wrist_s1", False)
        wrist_t2 = request.json.get("wrist_t2", False)
        wrist_s2 = request.json.get("wrist_s2", False)
        dip2_t1 = request.json.get("dip2_t1", False)
        dip2_s1 = request.json.get("dip2_s1", False)
        dip2_t2 = request.json.get("dip2_t2", False)
        dip2_s2 = request.json.get("dip2_s2", False)
        dip3_t1 = request.json.get("dip3_t1", False)
        dip3_s1 = request.json.get("dip3_s1", False)
        dip3_t2 = request.json.get("dip3_t2", False)
        dip3_s2 = request.json.get("dip3_s2", False)
        dip4_t1 = request.json.get("dip4_t1", False)
        dip4_s1 = request.json.get("dip4_s1", False)
        dip4_t2 = request.json.get("dip4_t2", False)
        dip4_s2 = request.json.get("dip4_s2", False)
        dip5_t1 = request.json.get("dip5_t1", False)
        dip5_s1 = request.json.get("dip5_s1", False)
        dip5_t2 = request.json.get("dip5_t2", False)
        dip5_s2 = request.json.get("dip5_s2", False)
        r1ip1_t1 = request.json.get("r1ip1_t1", False)
        r1ip1_s1 = request.json.get("r1ip1_s1", False)
        r1ip1_t2 = request.json.get("r1ip1_t2", False)
        r1ip1_s2 = request.json.get("r1ip1_s2", False)
        r1ip2_t1 = request.json.get("r1ip2_t1", False)
        r1ip2_s1 = request.json.get("r1ip2_s1", False)
        r1ip2_t2 = request.json.get("r1ip2_t2", False)
        r1ip2_s2 = request.json.get("r1ip2_s2", False)
        pip3_t1 = request.json.get("pip3_t1", False)
        pip3_s1 = request.json.get("pip3_s1", False)
        pip3_t2 = request.json.get("pip3_t2", False)
        pip3_s2 = request.json.get("pip3_s2", False)
        pip4_t1 = request.json.get("pip4_t1", False)
        pip4_s1 = request.json.get("pip4_s1", False)
        pip4_t2 = request.json.get("pip4_t2", False)
        pip4_s2 = request.json.get("pip4_s2", False)
        pip5_t1 = request.json.get("pip5_t1", False)
        pip5_s1 = request.json.get("pip5_s1", False)
        pip5_t2 = request.json.get("pip5_t2", False)
        pip5_s2 = request.json.get("pip5_s2", False)
        mcp1_t1 = request.json.get("mcp1_t1", False)
        mcp1_s1 = request.json.get("mcp1_s1", False)
        mcp1_t2 = request.json.get("mcp1_t2", False)
        mcp1_s2 = request.json.get("mcp1_s2", False)
        mcp2_t1 = request.json.get("mcp2_t1", False)
        mcp2_s1 = request.json.get("mcp2_s1", False)
        mcp2_t2 = request.json.get("mcp2_t2", False)
        mcp2_s2 = request.json.get("mcp2_s2", False)

        mcp3_t1 = request.json.get("mcp3_t1", False)
        mcp3_s1 = request.json.get("mcp3_s1", False)
        mcp3_t2 = request.json.get("mcp3_t2", False)
        mcp3_s2 = request.json.get("mcp3_s2", False)
        mcp4_t1 = request.json.get("mcp4_t1", False)
        mcp4_s1 = request.json.get("mcp4_s1", False)
        mcp4_t2 = request.json.get("mcp4_t2", False)
        mcp4_s2 = request.json.get("mcp4_s2", False)
        mcp5_t1 = request.json.get("mcp5_t1", False)
        mcp5_s1 = request.json.get("mcp5_s1", False)
        mcp5_t2 = request.json.get("mcp5_t2", False)
        mcp5_s2 = request.json.get("mcp5_s2", False)
        hip_t1 = request.json.get("hip_t1", False)
        hip_s1 = request.json.get("hip_s1", False)
        hip_t2 = request.json.get("hip_t2", False)
        hip_s2 = request.json.get("hip_s2", False)
        knee_t1 = request.json.get("knee_t1", False)
        knee_s1 = request.json.get("knee_s1", False)
        knee_t2 = request.json.get("knee_t2", False)
        knee_s2 = request.json.get("knee_s2", False)
        a_tt_t1 = request.json.get("a_tt_t1", False)
        a_tt_s1 = request.json.get("a_tt_s1", False)
        a_tt_t2 = request.json.get("a_tt_t2", False)
        a_tt_s2 = request.json.get("a_tt_s2", False)
        a_tc_t1 = request.json.get("a_tc_t1", False)
        a_tc_s1 = request.json.get("a_tc_s1", False)
        a_tc_t2 = request.json.get("a_tc_t2", False)
        a_tc_s2 = request.json.get("a_tc_s2", False)
        mtl_t1 = request.json.get("mtl_t1", False)
        mtl_s1 = request.json.get("mtl_s1", False)
        mtl_t2 = request.json.get("mtl_t2", False)
        mtl_s2 = request.json.get("mtl_s2", False)
        mtp1_t1 = request.json.get("mtp1_t1", False)
        mtp1_s1 = request.json.get("mtp1_s1", False)
        mtp1_t2 = request.json.get("mtp1_t2", False)
        mtp1_s2 = request.json.get("mtp1_s2", False)
        mtp2_t1 = request.json.get("mtp2_t1", False)
        mtp2_s1 = request.json.get("mtp2_s1", False)
        mtp2_t2 = request.json.get("mtp2_t2", False)
        mtp2_s2 = request.json.get("mtp2_s2", False)
        mtp3_t1 = request.json.get("mtp3_t1", False)
        mtp3_s1 = request.json.get("mtp3_s1", False)
        mtp3_t2 = request.json.get("mtp3_t2", False)
        mtp3_s2 = request.json.get("mtp3_s2", False)
        mtp4_t1 = request.json.get("mtp4_t1", False)
        mtp4_s1 = request.json.get("mtp4_s1", False)
        mtp4_t2 = request.json.get("mtp4_t2", False)
        mtp4_s2 = request.json.get("mtp4_s2", False)
        mtp5_t1 = request.json.get("mtp5_t1", False)
        mtp5_s1 = request.json.get("mtp5_s1", False)
        mtp5_t2 = request.json.get("mtp5_t2", False)
        mtp5_s2 = request.json.get("mtp5_s2", False)
        r2ip1_t1 = request.json.get("r2ip1_t1", False)
        r2ip1_s1 = request.json.get("r2ip1_s1", False)
        r2ip1_t2 = request.json.get("r2ip1_t2", False)
        r2ip1_s2 = request.json.get("r2ip1_s2", False)
        r2ip2_t1 = request.json.get("r2ip2_t1", False)
        r2ip2_s1 = request.json.get("r2ip2_s1", False)
        r2ip2_t2 = request.json.get("r2ip2_t2", False)
        r2ip2_s2 = request.json.get("r2ip2_s2", False)
        ip3_t1 = request.json.get("ip3_t1", False)
        ip3_s1 = request.json.get("ip3_s1", False)
        ip3_t2 = request.json.get("ip3_t2", False)
        ip3_s2 = request.json.get("ip3_s2", False)
        ip4_t1 = request.json.get("ip4_t1", False)
        ip4_s1 = request.json.get("ip4_s1", False)
        ip4_t2 = request.json.get("ip4_t2", False)
        ip4_s2 = request.json.get("ip4_s2", False)
        ip5_t1 = request.json.get("ip5_t1", False)
        ip5_s1 = request.json.get("ip5_s1", False)
        ip5_t2 = request.json.get("ip5_t2", False)
        ip5_s2 = request.json.get("ip5_s2", False)
        s1_t1 = request.json.get("s1_t1", False)
        s1_s1 = request.json.get("s1_s1", False)
        s1_t2 = request.json.get("s1_t2", False)
        s1_s2 = request.json.get("s1_s2", False)

        cervical_anky = request.json.get("cervical_anky", False)
        cervical_flex = request.json.get("cervical_flex", False)
        cervical_ext = request.json.get("cervical_ext", False)
        cervical_rtrot = request.json.get("cervical_rtrot", False)
        cervical_ltrot = request.json.get("cervical_ltrot", False)
        cervical_rtfl = request.json.get("cervical_rtfl", False)
        cervical_ltfl = request.json.get("cervical_ltfl", False)
        cervical_pt = request.json.get("cervical_pt", False)

        thorasic_anky = request.json.get("thorasic_anky", False)
        thorasic_flex = request.json.get("thorasic_flex", False)
        thorasic_ext = request.json.get("thorasic_ext", False)
        thorasic_rtrot = request.json.get("thorasic_rtrot", False)
        thorasic_ltrot = request.json.get("thorasic_ltrot", False)
        thorasic_rtfl = request.json.get("thorasic_rtfl", False)
        thorasic_ltfl = request.json.get("thorasic_ltfl", False)
        thorasic_pt = request.json.get("thorasic_pt", False)
        lumbar_anky = request.json.get("lumbar_anky", False)
        lumbar_flex = request.json.get("lumbar_flex", False)
        lumbar_ext = request.json.get("lumbar_ext", False)
        lumbar_rtrot = request.json.get("lumbar_rtrot", False)
        lumbar_ltrot = request.json.get("lumbar_ltrot", False)
        lumbar_rtfl = request.json.get("lumbar_rtfl", False)
        lumbar_ltfl = request.json.get("lumbar_ltfl", False)
        lumbar_pt = request.json.get("lumbar_pt", False)
        opt_rt = request.json.get("opt_rt", False)
        opt_lt = request.json.get("opt_lt", False)
        lcer_rt = request.json.get("lcer_rt", False)
        lcer_lt = request.json.get("lcer_lt", False)
        trpz_rt = request.json.get("trpz_rt", False)
        trpz_lt = request.json.get("trpz_lt", False)
        scap_rt = request.json.get("scap_rt", False)
        scap_lt = request.json.get("scap_lt", False)
        zcst_rt = request.json.get("zcst_rt", False)
        zcst_lt = request.json.get("zcst_lt", False)
        epdl_rt = request.json.get("epdl_rt", False)
        epdl_lt = request.json.get("epdl_lt", False)
        glut_rt = request.json.get("glut_rt", False)
        glut_lt = request.json.get("glut_lt", False)
        trcr_rt = request.json.get("trcr_rt", False)
        trcr_lt = request.json.get("trcr_lt", False)
        knee_rt = request.json.get("knee_rt", False)
        knee_lt = request.json.get("knee_lt", False)
        ta_rt = request.json.get("ta_rt", False)
        ta_lt = request.json.get("ta_lt", False)
        calf_rt = request.json.get("calf_rt", False)
        calf_lt = request.json.get("calf_lt", False)
        sole_rt = request.json.get("sole_rt", False)
        sole_lt = request.json.get("sole_lt", False)
        rhand_fl = request.json.get("rhand_fl", False)
        rhand_ex = request.json.get("rhand_ex", False)
        rhand_ab = request.json.get("rhand_ab", False)
        rhand_add = request.json.get("rhand_add", False)
        rhand_slx = request.json.get("rhand_slx", False)
        lhand_fl = request.json.get("lhand_fl", False)
        lhand_ex = request.json.get("lhand_ex", False)
        lhand_ab = request.json.get("lhand_ab", False)
        lhand_add = request.json.get("lhand_add", False)
        lhand_slx = request.json.get("lhand_slx", False)
        rwrist_fl = request.json.get("rwrist_fl", False)
        rwrist_ex = request.json.get("rwrist_ex", False)
        rwrist_ab = request.json.get("rwrist_ab", False)
        rwrist_add = request.json.get("rwrist_add", False)
        rwrist_slx = request.json.get("rwrist_slx", False)
        lwrist_fl = request.json.get("lwrist_fl", False)
        lwrist_ex = request.json.get("lwrist_ex", False)
        lwrist_ab = request.json.get("lwrist_ab", False)
        lwrist_add = request.json.get("lwrist_add", False)
        lwrist_slx = request.json.get("lwrist_slx", False)
        relb_fl = request.json.get("relb_fl", False)
        relb_ex = request.json.get("relb_ex", False)
        relb_ab = request.json.get("relb_ab", False)
        relb_add = request.json.get("relb_add", False)
        relb_slx = request.json.get("relb_slx", False)

        lelb_fl = request.json.get("lelb_fl", False)
        lelb_ex = request.json.get("lelb_ex", False)
        lelb_ab = request.json.get("lelb_ab", False)
        lelb_add = request.json.get("lelb_add", False)
        lelb_slx = request.json.get("lelb_slx", False)

        shrt_fl = request.json.get("shrt_fl", False)
        shrt_ex = request.json.get("shrt_ex", False)
        shrt_ab = request.json.get("shrt_ab", False)
        shrt_add = request.json.get("shrt_add", False)
        shrt_slx = request.json.get("shrt_slx", False)
        shlt_fl = request.json.get("shlt_fl", False)
        shlt_ex = request.json.get("shlt_ex", False)
        shlt_ab = request.json.get("shlt_ab", False)
        shlt_add = request.json.get("shlt_add", False)
        shlt_slx = request.json.get("shlt_slx", False)
        kneert_fl = request.json.get("kneert_fl", False)
        kneert_ex = request.json.get("kneert_ex", False)
        kneert_ab = request.json.get("kneert_ab", False)
        kneert_add = request.json.get("kneert_add", False)
        kneert_slx = request.json.get("kneert_slx", False)
        kneelt_fl = request.json.get("kneelt_fl", False)
        kneelt_ex = request.json.get("kneelt_ex", False)
        kneelt_ab = request.json.get("kneelt_ab", False)
        kneelt_add = request.json.get("kneelt_add", False)
        kneelt_slx = request.json.get("kneelt_slx", False)
        footrt_fl = request.json.get("footrt_fl", False)
        footrt_ex = request.json.get("footrt_ex", False)
        footrt_ab = request.json.get("footrt_ab", False)
        footrt_add = request.json.get("footrt_add", False)
        footrt_slx = request.json.get("footrt_slx", False)
        footlt_fl = request.json.get("footlt_fl", False)
        footlt_ex = request.json.get("footlt_ex", False)
        footlt_ab = request.json.get("footlt_ab", False)
        footlt_add = request.json.get("footlt_add", False)
        footlt_slx = request.json.get("footlt_slx", False)
        thumb_rt = request.json.get("thumb_rt", False)
        thumb_lt = request.json.get("thumb_lt", False)
        finger_rt = request.json.get("finger_rt", False)
        finger_lt = request.json.get("finger_lt", False)
        palm_rt = request.json.get("palm_rt", False)
        palm_lt = request.json.get("palm_lt", False)
        elbow_rt = request.json.get("elbow_rt", False)
        elbow_lt = request.json.get("elbow_lt", False)
        hy_knee_rt = request.json.get("hy_knee_rt", False)
        hy_knee_lt = request.json.get("hy_knee_lt", False)
        ankle_rt = request.json.get("ankle_rt", False)
        ankle_lt = request.json.get("ankle_lt", False)
        other_rt = request.json.get("other_rt", False)
        other_lt = request.json.get("other_lt", False)
        spine_rt = request.json.get("spine_rt", False)
        spine_lt = request.json.get("spine_lt", False)

        can_u_dress_urself = request.json.get("can_u_dress_urself", False)
        can_u_wash_ur_hair = request.json.get("can_u_wash_ur_hair", False)
        can_u_comb_ur_hair = request.json.get("can_u_comb_ur_hair", False)
        dressing_score = request.json.get("dressing_score", False)

        can_u_stand_from_chair = request.json.get("can_u_stand_from_chair", False)
        can_u_get_inout_from_bed = request.json.get("can_u_get_inout_from_bed", False)
        can_u_sit_grossteg_onfloor = request.json.get("can_u_sit_grossteg_onfloor", False)
        arising_score = request.json.get("arising_score", False)

        can_u_cut_vegetables = request.json.get("can_u_cut_vegetables", False)
        can_u_lift_glass = request.json.get("can_u_lift_glass", False)
        can_u_break_roti_from_1hand = request.json.get("can_u_break_roti_from_1hand", False)
        eating_score = request.json.get("eating_score", False)

        can_u_walk = request.json.get("can_u_walk", False)
        can_u_climb_5steps = request.json.get("can_u_climb_5steps", False)
        walking_score = request.json.get("walking_score", False)

        can_u_take_bath = request.json.get("can_u_take_bath", False)
        can_u_wash_dry_urbody = request.json.get("can_u_wash_dry_urbody", False)
        can_u_get_onoff_toilet = request.json.get("can_u_get_onoff_toilet", False)
        hygiene_score = request.json.get("hygiene_score", False)

        can_u_weigh_2kg = request.json.get("can_u_weigh_2kg", False)
        can_u_bend_and_pickcloths = request.json.get("can_u_bend_and_pickcloths", False)
        reaching_score = request.json.get("reaching_score", False)

        can_u_open_bottle = request.json.get("can_u_open_bottle", False)
        can_u_turntaps_onoff = request.json.get("can_u_turntaps_onoff", False)
        can_u_open_latches = request.json.get("can_u_open_latches", False)
        grip_score = request.json.get("grip_score", False)

        can_u_work_office_house = request.json.get("can_u_work_office_house", False)
        can_u_run_errands = request.json.get("can_u_run_errands", False)
        can_u_get_inout_of_bus = request.json.get("can_u_get_inout_of_bus", False)
        activities_score = request.json.get("activities_score", False)

        patient_assessment_pain = request.json.get("patient_assessment_pain", False)
        grip_strength_rt = request.json.get("grip_strength_rt", False)
        grip_strength_hg = request.json.get("grip_strength_hg", False)
        grip_strength_lt = request.json.get("grip_strength_lt", False)
        early_mrng_stiffness = request.json.get("early_mrng_stiffness", False)
        sleep = request.json.get("sleep", False)
        general_health_assessment = request.json.get("general_health_assessment", False)
        user_id = request.json.get("user_id", False)
        try:
            dbedit = session.query(RheumatologyFollowup).filter_by(id=id).update(
                dict(lelb_ab=lelb_ab, lelb_ex=lelb_ex, lelb_fl=lelb_fl, lelb_add=lelb_add, lelb_slx=lelb_slx, pid=pid,
                     code=code, date=date, pname=pname, age=age, sex=sex, address=address, phone=phone,
                     firstvisit=firstvisit, lastvisit=lastvisit, timelapsed=timelapsed, bp=bp, wt=wt, followup=followup,
                     course_result=course_result, improved_percent=improved_percent,
                     deteriorate_percent=deteriorate_percent, history=history, diagnosis=diagnosis,
                     addnewdiagnosis=addnewdiagnosis, anorexia=anorexia, anorexia_duration=anorexia_duration,
                     anorexia_severity=anorexia_severity, anorexia_cause=anorexia_cause, nausea=nausea,
                     nausea_duration=nausea_duration, nausea_severity=nausea_severity, nausea_cause=nausea_cause,
                     vomiting=vomiting, vomiting_duration=vomiting_duration, vomiting_severity=vomiting_severity,
                     vomiting_cause=vomiting_cause, acidity=acidity, acidity_cause=acidity_cause,
                     acidity_duration=acidity_duration, acidity_severity=acidity_severity, pain_abdomen=pain_abdomen,
                     pain_duration=pain_duration, pain_severity=pain_severity, pain_cause=pain_cause,
                     diarrhoea=diarrhoea, diarrhoea_duration=diarrhoea_duration, diarrhoea_severity=diarrhoea_severity,
                     diarrhoea_cause=diarrhoea_cause, oral_ulcers=oral_ulcers, ulcers_duration=ulcers_duration,
                     ulcers_severity=ulcers_severity, ulcers_cause=ulcers_cause, constipation=constipation,
                     constipation_duration=constipation_duration, constipation_severity=constipation_severity,
                     constipation_cause=constipation_cause, skinrash=skinrash, skinrash_duration=skinrash_duration,
                     skinrash_severity=skinrash_severity, skinrash_cause=skinrash_cause, hairfall=hairfall,
                     hairfall_duration=hairfall_duration, hairfall_severity=hairfall_severity,
                     hairfall_cause=hairfall_cause, othertoxics=othertoxics, othertoxics_duration=othertoxics_duration,
                     othertoxics_severity=othertoxics_severity, othertoxics_cause=othertoxics_cause, hcq=hcq, mtx=mtx,
                     ssz=ssz, lef=lef, azt=azt, mmf=mmf, pred=pred, nsaid=nsaid, esr=esr, hb=hb, tlc=tlc, dlc=dlc,
                     platelet=platelet, urine=urine, bun=bun, cr=cr, sgot=sgot, sgpt=sgpt, alb=alb, glob=glob, ua=ua,
                     bsl=bsl, rf=rf, crp=crp, bili=bili, other=other, xray=xray, treatment_plan=treatment_plan,
                     tm_t1=tm_t1, tm_s1=tm_s1, tm_t2=tm_t2, tm_s2=tm_s2, scl_t1=scl_t1, scl_s1=scl_s1, scl_t2=scl_t2,
                     scl_s2=scl_s2, acl_t1=acl_t1, acl_s1=acl_s1, acl_t2=acl_t2, acl_s2=acl_s2, sh_t1=sh_t1,
                     sh_s1=sh_s1, sh_t2=sh_t2, sh_s2=sh_s2, elbow_t1=elbow_t1, elbow_s1=elbow_s1, elbow_t2=elbow_t2,
                     elbow_s2=elbow_s2, infru_t1=infru_t1, infru_s1=infru_s1, infru_t2=infru_t2, infru_s2=infru_s2,
                     cmc1_t1=cmc1_t1, cmc1_s1=cmc1_s1, cmc1_t2=cmc1_t2, cmc1_s2=cmc1_s2, wrist_t1=wrist_t1,
                     wrist_s1=wrist_s1, wrist_t2=wrist_t2, wrist_s2=wrist_s2, dip2_t1=dip2_t1, dip2_s1=dip2_s1,
                     dip2_t2=dip2_t2, dip2_s2=dip2_s2, dip3_t1=dip3_t1, dip3_s1=dip3_s1, dip3_t2=dip3_t2,
                     dip3_s2=dip3_s2, dip4_t1=dip4_t1, dip4_s1=dip4_s1, dip4_t2=dip4_t2, dip4_s2=dip4_s2,
                     dip5_t1=dip5_t1, dip5_s1=dip5_s1, dip5_t2=dip5_t2, dip5_s2=dip5_s2, r1ip1_t1=r1ip1_t1,
                     r1ip1_s1=r1ip1_s1, r1ip1_t2=r1ip1_t2, r1ip1_s2=r1ip1_s2, r1ip2_t1=r1ip2_t1, r1ip2_s1=r1ip2_s1,
                     r1ip2_t2=r1ip2_t2, r1ip2_s2=r1ip2_s2, pip3_t1=pip3_t1, pip3_s1=pip3_s1, pip3_t2=pip3_t2,
                     pip3_s2=pip3_s2, pip4_t1=pip4_t1, pip4_s1=pip4_s1, pip4_t2=pip4_t2, pip4_s2=pip4_s2,
                     pip5_t1=pip5_t1, pip5_s1=pip5_s1, pip5_t2=pip5_t2, pip5_s2=pip5_s2, mcp1_t1=mcp1_t1,
                     mcp1_s1=mcp1_s1, mcp1_t2=mcp1_t2, mcp1_s2=mcp1_s2, mcp2_t1=mcp2_t1, mcp2_s1=mcp2_s1,
                     mcp2_t2=mcp2_t2, mcp2_s2=mcp2_s2, mcp3_t1=mcp3_t1, mcp3_s1=mcp3_s1, mcp3_t2=mcp3_t2,
                     mcp3_s2=mcp3_s2, mcp4_t1=mcp4_t1, mcp4_s1=mcp4_s1, mcp4_t2=mcp4_t2, mcp4_s2=mcp4_s2,
                     mcp5_t1=mcp5_t1, mcp5_s1=mcp5_s1, mcp5_t2=mcp5_t2, mcp5_s2=mcp5_s2, hip_t1=hip_t1, hip_s1=hip_s1,
                     hip_t2=hip_t2, hip_s2=hip_s2, knee_t1=knee_t1, knee_s1=knee_s1, knee_t2=knee_t2, knee_s2=knee_s2,
                     a_tt_t1=a_tt_t1, a_tt_s1=a_tt_s1, a_tt_t2=a_tt_t2, a_tt_s2=a_tt_s2, a_tc_t1=a_tc_t1,
                     a_tc_s1=a_tc_s1, a_tc_t2=a_tc_t2, a_tc_s2=a_tc_s2, mtl_t1=mtl_t1, mtl_s1=mtl_s1, mtl_t2=mtl_t2,
                     mtl_s2=mtl_s2, mtp1_t1=mtp1_t1, mtp1_s1=mtp1_s1, mtp1_t2=mtp1_t2, mtp1_s2=mtp1_s2, mtp2_t1=mtp2_t1,
                     mtp2_s1=mtp2_s1, mtp2_t2=mtp2_t2, mtp2_s2=mtp2_s2, mtp3_t1=mtp3_t1, mtp3_s1=mtp3_s1,
                     mtp3_t2=mtp3_t2, mtp3_s2=mtp3_s2, mtp4_t1=mtp4_t1, mtp4_s1=mtp4_s1, mtp4_t2=mtp4_t2,
                     mtp4_s2=mtp4_s2, mtp5_t1=mtp5_t1, mtp5_s1=mtp5_s1, mtp5_t2=mtp5_t2, mtp5_s2=mtp5_s2,
                     r2ip1_t1=r2ip1_t1, r2ip1_s1=r2ip1_s1, r2ip1_t2=r2ip1_t2, r2ip1_s2=r2ip1_s2, r2ip2_t1=r2ip2_t1,
                     r2ip2_s1=r2ip2_s1, r2ip2_t2=r2ip2_t2, r2ip2_s2=r2ip2_s2, ip3_t1=ip3_t1, ip3_s1=ip3_s1,
                     ip3_t2=ip3_t2, ip3_s2=ip3_s2, ip4_t1=ip4_t1, ip4_s1=ip4_s1, ip4_t2=ip4_t2, ip4_s2=ip4_s2,
                     ip5_t1=ip5_t1, ip5_s1=ip5_s1, ip5_t2=ip5_t2, ip5_s2=ip5_s2, s1_t1=s1_t1, s1_s1=s1_s1, s1_t2=s1_t2,
                     s1_s2=s1_s2, cervical_anky=cervical_anky, cervical_flex=cervical_flex, cervical_ext=cervical_ext,
                     cervical_rtrot=cervical_rtrot, cervical_ltrot=cervical_ltrot, cervical_rtfl=cervical_rtfl,
                     cervical_ltfl=cervical_ltfl, cervical_pt=cervical_pt, thorasic_anky=thorasic_anky,
                     thorasic_flex=thorasic_flex, thorasic_ext=thorasic_ext, thorasic_rtrot=thorasic_rtrot,
                     thorasic_ltrot=thorasic_ltrot, thorasic_rtfl=thorasic_rtfl, thorasic_ltfl=thorasic_ltfl,
                     thorasic_pt=thorasic_pt, lumbar_anky=lumbar_anky, lumbar_flex=lumbar_flex, lumbar_ext=lumbar_ext,
                     lumbar_rtrot=lumbar_rtrot, lumbar_ltrot=lumbar_ltrot, lumbar_rtfl=lumbar_rtfl,
                     lumbar_ltfl=lumbar_ltfl, lumbar_pt=lumbar_pt, opt_rt=opt_rt, opt_lt=opt_lt, lcer_rt=lcer_rt,
                     lcer_lt=lcer_lt, trpz_rt=trpz_rt, trpz_lt=trpz_lt, scap_rt=scap_rt, scap_lt=scap_lt,
                     zcst_rt=zcst_rt, zcst_lt=zcst_lt, epdl_rt=epdl_rt, epdl_lt=epdl_lt, glut_rt=glut_rt,
                     glut_lt=glut_lt, trcr_rt=trcr_rt, trcr_lt=trcr_lt, knee_rt=knee_rt, knee_lt=knee_lt, ta_rt=ta_rt,
                     ta_lt=ta_lt, calf_rt=calf_rt, calf_lt=calf_lt, sole_rt=sole_rt, sole_lt=sole_lt, rhand_fl=rhand_fl,
                     rhand_ex=rhand_ex, rhand_ab=rhand_ab, rhand_add=rhand_add, rhand_slx=rhand_slx, lhand_fl=lhand_fl,
                     lhand_ex=lhand_ex, lhand_ab=lhand_ab, lhand_add=lhand_add, lhand_slx=lhand_slx,
                     rwrist_fl=rwrist_fl, rwrist_ex=rwrist_ex, rwrist_ab=rwrist_ab, rwrist_add=rwrist_add,
                     rwrist_slx=rwrist_slx, lwrist_fl=lwrist_fl, lwrist_ex=lwrist_ex, lwrist_ab=lwrist_ab,
                     lwrist_add=lwrist_add, lwrist_slx=lwrist_slx, relb_fl=relb_fl, relb_ex=relb_ex, relb_ab=relb_ab,
                     relb_add=relb_add, relb_slx=relb_slx, shrt_fl=shrt_fl, shrt_ex=shrt_ex, shrt_ab=shrt_ab,
                     shrt_add=shrt_add, shrt_slx=shrt_slx, shlt_fl=shlt_fl, shlt_ex=shlt_ex, shlt_ab=shlt_ab,
                     shlt_add=shlt_add, shlt_slx=shlt_slx, kneert_fl=kneert_fl, kneert_ex=kneert_ex,
                     kneert_ab=kneert_ab, kneert_add=kneert_add, kneert_slx=kneert_slx, kneelt_fl=kneelt_fl,
                     kneelt_ex=kneelt_ex, kneelt_ab=kneelt_ab, kneelt_add=kneelt_add, kneelt_slx=kneelt_slx,
                     footrt_fl=footrt_fl, footrt_ex=footrt_ex, footrt_ab=footrt_ab, footrt_add=footrt_add,
                     footrt_slx=footrt_slx, footlt_fl=footlt_fl, footlt_ex=footlt_ex, footlt_ab=footlt_ab,
                     footlt_add=footlt_add, footlt_slx=footlt_slx, thumb_rt=thumb_rt, thumb_lt=thumb_lt,
                     finger_rt=finger_rt, finger_lt=finger_lt, palm_rt=palm_rt, palm_lt=palm_lt, elbow_rt=elbow_rt,
                     elbow_lt=elbow_lt, hy_knee_rt=hy_knee_rt, hy_knee_lt=hy_knee_lt, ankle_rt=ankle_rt,
                     ankle_lt=ankle_lt, other_rt=other_rt, other_lt=other_lt, spine_rt=spine_rt, spine_lt=spine_lt,
                     can_u_dress_urself=can_u_dress_urself, can_u_wash_ur_hair=can_u_wash_ur_hair,
                     can_u_comb_ur_hair=can_u_comb_ur_hair, dressing_score=dressing_score,
                     can_u_stand_from_chair=can_u_stand_from_chair, can_u_get_inout_from_bed=can_u_get_inout_from_bed,
                     can_u_sit_grossteg_onfloor=can_u_sit_grossteg_onfloor, arising_score=arising_score,
                     can_u_cut_vegetables=can_u_cut_vegetables, can_u_lift_glass=can_u_lift_glass,
                     can_u_break_roti_from_1hand=can_u_break_roti_from_1hand, eating_score=eating_score,
                     can_u_walk=can_u_walk, can_u_climb_5steps=can_u_climb_5steps, walking_score=walking_score,
                     can_u_take_bath=can_u_take_bath, can_u_wash_dry_urbody=can_u_wash_dry_urbody,
                     can_u_get_onoff_toilet=can_u_get_onoff_toilet, hygiene_score=hygiene_score,
                     can_u_weigh_2kg=can_u_weigh_2kg, can_u_bend_and_pickcloths=can_u_bend_and_pickcloths,
                     reaching_score=reaching_score, can_u_open_bottle=can_u_open_bottle,
                     can_u_turntaps_onoff=can_u_turntaps_onoff, can_u_open_latches=can_u_open_latches,
                     grip_score=grip_score, can_u_work_office_house=can_u_work_office_house,
                     can_u_run_errands=can_u_run_errands, can_u_get_inout_of_bus=can_u_get_inout_of_bus,
                     activities_score=activities_score, patient_assessment_pain=patient_assessment_pain,
                     grip_strength_rt=grip_strength_rt, grip_strength_hg=grip_strength_hg,
                     grip_strength_lt=grip_strength_lt, early_mrng_stiffness=early_mrng_stiffness, sleep=sleep,
                     general_health_assessment=general_health_assessment, user_id=user_id))
            session.commit()
            # sending back updated data of the user
            patientdata = session.query(RheumatologyFollowup).filter_by(id=id).first()
            session.commit()
            patientschema = RheumatologyFollowupSchema()
            output = patientschema.dump(patientdata)
            return jsonify({'RheumatologyFollowup': output})
        except:
            return "Fail"
    return "get request made"


# Attachments for rheuma , rheuma followup , breast cancer

# Add attachments
# rheumatology upload import uuid

#inputs are pid and filename
@app.route("/rfiles", methods=["POST"])
def post_file():
    """Upload a file."""
    random_filename = str(uuid.uuid4())
    shortname = str(random_filename)[19:]
    filenameend = request.args.get("filename")
    # file = request.files['file']
    # frontend_filename = secure_filename(file.filename)
    filename = shortname + "_Rheuma_"+ filenameend

    pid = request.args.get("pid")
    if pid:

        if "/" in filename:
            # Return 400 BAD REQUEST
            abort(400, "no subdirectories allowed")
        try:
            dbadd = RAttachments(pid=pid, attachment=filename)
            session.add(dbadd)
            session.commit()

            with open(os.path.join(UPLOAD_DIRECTORY, filename), "wb") as fp:
                fp.write(request.data)
            # file.save(os.path.join(UPLOAD_DIRECTORY, filename))
            # Return 201 CREATED
            return "Success", 201
        except:
            return "pid doesnot exist"
    else:
        return "send pid in params"


# rheumatology followup upload
@app.route("/rffiles", methods=["POST"])
def post_rfile():
    """Upload a file."""
    random_filename = str(uuid.uuid4())
    shortname = str(random_filename)[19:]
    filenameend = request.args.get("filename")
    # file = request.files['file']
    # frontend_filename = secure_filename(file.filename)
    filename = shortname + "_Rheuma_" + filenameend

    pid = request.args.get("pid")
    if pid:

        if "/" in filename:
            # Return 400 BAD REQUEST
            abort(400, "no subdirectories allowed")

        try:
            dbadd = RFollowAttachments(pid=pid, attachment=filename)
            session.add(dbadd)
            session.commit()

            with open(os.path.join(UPLOAD_DIRECTORY, filename), "wb") as fp:
                fp.write(request.data)
            # file.save(os.path.join(UPLOAD_DIRECTORY, filename))

            # Return 201 CREATED
            return "Success", 201
        except:
            return "pid doesnot exist"
    else:
        return "send pid in params"


# Breast upload
@app.route("/bfiles", methods=["POST"])
def post_bfile():
    """Upload a file."""
    random_filename = str(uuid.uuid4())
    shortname = str(random_filename)[19:]

    filenameend = request.args.get("filename")
    # file = request.files['file']
    # frontend_filename = secure_filename(file.filename)
    filename = shortname + "_Rheuma_" + filenameend

    pid = request.args.get("pid")
    if pid:

        if "/" in filename:
            # Return 400 BAD REQUEST
            abort(400, "no subdirectories allowed")
        try:
            dbadd = BAttachments(pid=pid, attachment=filename)
            session.add(dbadd)
            session.commit()

            with open(os.path.join(UPLOAD_DIRECTORY, filename), "wb") as fp:
                fp.write(request.data)
            # file.save(os.path.join(UPLOAD_DIRECTORY, filename))

            # Return 201 CREATED
            return "Success", 201
        except:
            return "pid doesnot exist"
    else:
        return "send pid in params"


# __________Download Attachments_______
# rheuma
# get reuest for download
@app.route("/files/<path:path>")
def get_file(path):
    # filename is path
    # ex for filename 12_breast ,45_rheuma , 8_rheumafollow
    """Download a file."""
    return send_from_directory(UPLOAD_DIRECTORY, path, as_attachment=True)


# _____List of Attachments___
# rheuma

@app.route("/rattachmentslist", methods=['GET'])
def rattachmentslist():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    # print(pid)
    attachments = session.query(RAttachments).filter_by(pid=pid).all()
    session.commit()
    for abc in attachments:
        print(abc.attachment)
    user_schema = Rheumatologyattachmentschema(many=True)
    output = user_schema.dump(attachments)
    return jsonify({'Rattachments': output})


# rheuma followup

@app.route("/rfattachmentslist", methods=['GET'])
def rfattachmentslist():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    # print(pid)
    attachments = session.query(RFollowAttachments).filter_by(pid=pid).all()
    session.commit()
    for abc in attachments:
        print(abc.attachment)
    user_schema = RheumatologyFollowattachmentschema(many=True)
    output = user_schema.dump(attachments)
    return jsonify({'RFattachments': output})


# BReast cancer

@app.route("/battachmentslist", methods=['GET'])
def battachmentslist():
    try:
        pid = request.args['pid']
    except:
        return "no pid sent"
    # print(pid)
    attachments = session.query(BAttachments).filter_by(pid=pid).all()
    session.commit()
    for abc in attachments:
        print(abc.attachment)
    user_schema = Breastattachmentschema(many=True)
    output = user_schema.dump(attachments)
    return jsonify({'Battachments': output})


if __name__ == "__main__":
    app.run(host="localhost", port=5000, debug="true")
